//
//  calculateAvarageDifferenceForExpenses.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 09/09/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import XCTest
@testable import TeamExpenses

class calculateAvarageDifferenceForExpenses: XCTestCase {
    var event: Event?
    var result = Set<SplitModel>()
    var allPerson = Set<Person>()
    var splitModel = SplitModel()
    var personInDeal = Set<Person>()
    
    var person_A: Person?
    var person_B: Person?
    var person_C: Person?
    var person_D: Person?
    
    let managedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(ExpenseManager.sharedInstance.managedObjectContext)
    
    override func setUp() {
        super.setUp()
        
        self.event = ExpenseManager.sharedInstance.createNewEvent(self.managedObjectContext)
        
        self.result = Set<SplitModel>()
        self.allPerson = Set<Person>()
        self.splitModel = SplitModel()
        self.personInDeal = Set<Person>()
        
        // Creating event persons
        self.person_A = ExpenseManager.sharedInstance.createNewPerson(self.managedObjectContext)
        person_A?.name = "person_A"
        
        self.person_B = ExpenseManager.sharedInstance.createNewPerson(self.managedObjectContext)
        person_B?.name = "person_B"
        
        self.person_C = ExpenseManager.sharedInstance.createNewPerson(self.managedObjectContext)
        person_C?.name = "person_C"
        
        self.person_D = ExpenseManager.sharedInstance.createNewPerson(self.managedObjectContext)
        person_D?.name = "person_D"
        
        self.allPerson.insert(self.person_A!)
        self.allPerson.insert(self.person_B!)
        self.allPerson.insert(self.person_C!)
        self.allPerson.insert(self.person_D!)
        
        self.personInDeal.insert(self.person_A!)
        self.personInDeal.insert(self.person_B!)
        self.personInDeal.insert(self.person_C!)
        self.personInDeal.insert(self.person_D!)
    }
    
    override func tearDown() {
        ExpenseManager.sharedInstance.dropDataBase(nil)
        
        super.tearDown()
    }
    
    func testExpensesAreEmpty() {
        let result = [SplitModel]()
        
        XCTAssertEqual(result, BillManager.sharedInstance.calculateAvarageDifferenceForExpenses(self.event?.objectID, managedObjectContext: self.managedObjectContext))
    }
    
    func testSplitExpensesEquallyWithOneExpense() {
        self.event?.addPersons(self.allPerson)
        
        let expense_A1 = ExpenseManager.sharedInstance.createNewExpense(self.managedObjectContext)
        expense_A1?.title = "expense_A1"
        
        expense_A1?.addPersons(inDeal: self.personInDeal)
        self.event?.addExpensesObject(expense_A1!)
        
        expense_A1?.price = NSDecimalNumber(value: 100.0)
        expense_A1?.event = event
        expense_A1?.creator = self.person_A
        
        do {
            try ExpenseManager.sharedInstance.saveContext(self.managedObjectContext)
        }
        catch {
            XCTAssert(false)
        }
        
        self.splitModel.personID = self.person_A?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = (expense_A1?.price?.doubleValue)!
        self.splitModel.differenceFromAvarage = -75.0
        self.result.insert(splitModel)
        
        self.splitModel = SplitModel()
        self.splitModel.personID = self.person_B?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 0.0
        self.splitModel.differenceFromAvarage = 25.0
        self.result.insert(splitModel)
        
        self.splitModel = SplitModel()
        self.splitModel.personID = self.person_C?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 0.0
        self.splitModel.differenceFromAvarage = 25.0
        self.result.insert(self.splitModel)
        
        self.splitModel = SplitModel()
        self.splitModel.personID = self.person_D?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 0.0
        self.splitModel.differenceFromAvarage = 25.0
        self.result.insert(self.splitModel)
        
        let items = BillManager.sharedInstance.calculateAvarageDifferenceForExpenses(self.event?.objectID, managedObjectContext: self.managedObjectContext)
        let set = Set(items)
        
        XCTAssertEqual(self.result, set)
    }
    
    func testSplitExpensesEquallyWithMultipleExpensesSameUser() {
        self.event?.addPersons(self.allPerson)
        
        // Create expense
        let expense_A1 = ExpenseManager.sharedInstance.createNewExpense(self.managedObjectContext)
        expense_A1?.title = "expense_A1"
        
        // Set people in deal
        expense_A1?.addPersons(inDeal: self.personInDeal)
        
        // Configure inverse reference
        self.person_A?.addExpensePersons(inDealObject: expense_A1!)
        self.person_B?.addExpensePersons(inDealObject: expense_A1!)
        self.person_C?.addExpensePersons(inDealObject: expense_A1!)
        self.person_D?.addExpensePersons(inDealObject: expense_A1!)
        expense_A1?.event = self.event
        
        // Add expense to the event
        self.event?.addExpensesObject(expense_A1!)
        
        expense_A1?.price = NSDecimalNumber(value: 100.0)
        expense_A1?.event = self.event
        expense_A1?.creator = self.person_A
        
        // Create expense
        let expense_A2 = ExpenseManager.sharedInstance.createNewExpense(self.managedObjectContext)
        expense_A2?.title = "expense_A2"
        
        // Set people in deal
        expense_A2?.addPersons(inDeal: self.personInDeal)
        
        // Configure inverse reference
        self.person_A?.addExpensePersons(inDealObject: expense_A2!)
        self.person_B?.addExpensePersons(inDealObject: expense_A2!)
        self.person_C?.addExpensePersons(inDealObject: expense_A2!)
        self.person_D?.addExpensePersons(inDealObject: expense_A2!)
        expense_A2?.event = self.event
        
        // Add expense to the event
        self.event?.addExpensesObject(expense_A2!)
        
        expense_A2?.price = NSDecimalNumber(value: 300.0)
        expense_A2?.event = self.event
        expense_A2?.creator = self.person_A
        
        do {
            try ExpenseManager.sharedInstance.saveContext(self.managedObjectContext)
        }
        catch {
            XCTAssert(false)
        }
        
        // Creating result data
        self.splitModel.personID = self.person_A?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 400.0 // expense_1 + expense_2
        self.splitModel.differenceFromAvarage = -300.0
        self.result.insert(self.splitModel)
        
        self.splitModel = SplitModel()
        self.splitModel.personID = self.person_B?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 0.0
        self.splitModel.differenceFromAvarage = 100.0
        self.result.insert(self.splitModel)
        
        self.splitModel = SplitModel()
        self.splitModel.personID = self.person_C?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 0.0
        self.splitModel.differenceFromAvarage = 100.0
        self.result.insert(self.splitModel)
        
        self.splitModel = SplitModel()
        self.splitModel.personID = self.person_D?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 0.0
        self.splitModel.differenceFromAvarage = 100.0
        self.result.insert(self.splitModel)
        
        let items = BillManager.sharedInstance.calculateAvarageDifferenceForExpenses(self.event?.objectID, managedObjectContext: self.managedObjectContext)
        let set = Set(items)
        
        XCTAssertEqual(self.result, set)
    }
    
    func testSplitExpensesEquallyWithMultipleExpensesDifferentUser() {
        self.event?.addPersons(self.allPerson)
        
        // Create expense
        let expense_A1 = ExpenseManager.sharedInstance.createNewExpense(self.managedObjectContext)
        expense_A1?.title = "expense_A1"
        
        // Set people in deal
        expense_A1?.addPersons(inDeal: self.personInDeal)
        
        // Configure inverse reference
        self.person_A?.addExpensePersons(inDealObject: expense_A1!)
        self.person_B?.addExpensePersons(inDealObject: expense_A1!)
        self.person_C?.addExpensePersons(inDealObject: expense_A1!)
        self.person_D?.addExpensePersons(inDealObject: expense_A1!)
        expense_A1?.event = self.event
        
        // Add expense to the event
        self.event?.addExpensesObject(expense_A1!)
        
        expense_A1?.price = NSDecimalNumber(value: 100.0)
        expense_A1?.event = self.event
        expense_A1?.creator = self.person_A
        
        // Create expense
        let expense_B1 = ExpenseManager.sharedInstance.createNewExpense(self.managedObjectContext)
        expense_B1?.title = "expense_B1"
        
        // Set people in deal
        expense_B1?.addPersons(inDeal: self.personInDeal)
        
        // Configure inverse reference
        self.person_A?.addExpensePersons(inDealObject: expense_B1!)
        self.person_B?.addExpensePersons(inDealObject: expense_B1!)
        self.person_C?.addExpensePersons(inDealObject: expense_B1!)
        self.person_D?.addExpensePersons(inDealObject: expense_B1!)
        expense_B1?.event = self.event
        
        // Add expense to the event
        self.event?.addExpensesObject(expense_B1!)
        
        expense_B1?.price = NSDecimalNumber(value: 300.0)
        expense_B1?.event = self.event
        expense_B1?.creator = self.person_B
        
        do {
            try ExpenseManager.sharedInstance.saveContext(self.managedObjectContext)
        }
        catch {
            XCTAssert(false)
        }
        
        // Creating result data
        self.splitModel.personID = self.person_A?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 100.0 // expense_A1
        self.splitModel.differenceFromAvarage = 0.0
        self.result.insert(self.splitModel)
        
        self.splitModel = SplitModel()
        self.splitModel.personID = self.person_B?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 300.0 // expense_B1
        self.splitModel.differenceFromAvarage = -200.0
        self.result.insert(self.splitModel)
        
        self.splitModel = SplitModel()
        self.splitModel.personID = self.person_C?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 0.0
        self.splitModel.differenceFromAvarage = 100.0
        self.result.insert(self.splitModel)
        
        self.splitModel = SplitModel()
        self.splitModel.personID = self.person_D?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 0.0
        self.splitModel.differenceFromAvarage = 100.0
        self.result.insert(self.splitModel)
        
        let items = BillManager.sharedInstance.calculateAvarageDifferenceForExpenses(self.event?.objectID, managedObjectContext: self.managedObjectContext)
        let set = Set(items)
        
        XCTAssertEqual(self.result, set)
    }
    
    func testCreatorExcludedSingleExpense() {
        self.event?.addPersons(self.allPerson)
        
        // Create expense
        let expense_A1 = ExpenseManager.sharedInstance.createNewExpense(self.managedObjectContext)
        expense_A1?.title = "expense_A1"
        
        // Set people in deal
        self.personInDeal.remove(self.person_A!)
        expense_A1?.addPersons(inDeal: self.personInDeal)
        
        // Configure inverse reference
        self.person_B?.addExpensePersons(inDealObject: expense_A1!)
        self.person_C?.addExpensePersons(inDealObject: expense_A1!)
        self.person_D?.addExpensePersons(inDealObject: expense_A1!)
        expense_A1?.event = self.event
        
        // Add expense to the event
        self.event?.addExpensesObject(expense_A1!)
        
        expense_A1?.price = NSDecimalNumber(value: 300.0)
        expense_A1?.event = self.event
        expense_A1?.creator = self.person_A
        
        do {
            try ExpenseManager.sharedInstance.saveContext(self.managedObjectContext)
        }
        catch {
            XCTAssert(false)
        }
        
        // Creating result data
        self.splitModel.personID = self.person_A?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 300.0 // expense_1
        self.splitModel.differenceFromAvarage = -300.0
        self.result.insert(self.splitModel)
        
        self.splitModel = SplitModel()
        self.splitModel.personID = self.person_B?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 0.0
        self.splitModel.differenceFromAvarage = 100.0
        self.result.insert(self.splitModel)
        
        self.splitModel = SplitModel()
        self.splitModel.personID = self.person_C?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 0.0
        self.splitModel.differenceFromAvarage = 100.0
        self.result.insert(self.splitModel)
        
        self.splitModel = SplitModel()
        self.splitModel.personID = self.person_D?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 0.0
        self.splitModel.differenceFromAvarage = 100.0
        self.result.insert(self.splitModel)
        
        let items = BillManager.sharedInstance.calculateAvarageDifferenceForExpenses(self.event?.objectID, managedObjectContext: self.managedObjectContext)
        let set = Set(items)
        
        XCTAssertEqual(self.result, set)
    }
    
    func testCreatorExcludedMultipleExpenses() {
        self.event?.addPersons(self.allPerson)
        
        // Create expense
        let expense_A1 = ExpenseManager.sharedInstance.createNewExpense(self.managedObjectContext)
        expense_A1?.title = "expense_A1"
        
        // Set people in deal
        self.personInDeal.remove(self.person_A!)
        expense_A1?.addPersons(inDeal: self.personInDeal)
        
        // Configure inverse reference
        self.person_B?.addExpensePersons(inDealObject: expense_A1!)
        self.person_C?.addExpensePersons(inDealObject: expense_A1!)
        self.person_D?.addExpensePersons(inDealObject: expense_A1!)
        expense_A1?.event = self.event
        
        // Add expense to the event
        self.event?.addExpensesObject(expense_A1!)
        
        expense_A1?.price = NSDecimalNumber(value: 300.0)
        expense_A1?.event = self.event
        expense_A1?.creator = self.person_A
        
        // Create expense
        let expense_A2 = ExpenseManager.sharedInstance.createNewExpense(self.managedObjectContext)
        expense_A2?.title = "expense_A2"
        
        self.personInDeal.remove(self.person_B!)
        self.personInDeal.insert(self.person_A!)
        // Set people in deal
        expense_A2?.addPersons(inDeal: self.personInDeal)
        
        // Configure inverse reference
        self.person_A?.addExpensePersons(inDealObject: expense_A2!)
        self.person_C?.addExpensePersons(inDealObject: expense_A2!)
        self.person_D?.addExpensePersons(inDealObject: expense_A2!)
        expense_A2?.event = self.event
        
        // Add expense to the event
        self.event?.addExpensesObject(expense_A2!)
        
        expense_A2?.price = NSDecimalNumber(value: 90.0)
        expense_A2?.event = self.event
        expense_A2?.creator = self.person_B
        
        do {
            try ExpenseManager.sharedInstance.saveContext(self.managedObjectContext)
        }
        catch {
            XCTAssert(false)
        }
        
        // Creating result data
        self.splitModel.personID = self.person_A?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 300.0 // expense_1
        self.splitModel.differenceFromAvarage = -270.0
        self.result.insert(self.splitModel)
        
        self.splitModel = SplitModel()
        self.splitModel.personID = self.person_B?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 90.0 // expense_2
        self.splitModel.differenceFromAvarage = 10.0
        self.result.insert(self.splitModel)
        
        self.splitModel = SplitModel()
        self.splitModel.personID = self.person_C?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 0.0
        self.splitModel.differenceFromAvarage = 130.0
        self.result.insert(self.splitModel)
        
        self.splitModel = SplitModel()
        self.splitModel.personID = self.person_D?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 0.0
        self.splitModel.differenceFromAvarage = 130.0
        self.result.insert(self.splitModel)
        
        let items = BillManager.sharedInstance.calculateAvarageDifferenceForExpenses(self.event?.objectID, managedObjectContext: self.managedObjectContext)
        let set = Set(items)
        
        XCTAssertEqual(self.result, set)
    }
    
    func testCreatorPaidForHimself() {
        self.event?.addPersons(self.allPerson)
        
        // Create expense
        let expense_A1 = ExpenseManager.sharedInstance.createNewExpense(self.managedObjectContext)
        expense_A1?.title = "expense_A1"
        
        // Set people in deal
        self.personInDeal.remove(self.person_B!)
        self.personInDeal.remove(self.person_C!)
        self.personInDeal.remove(self.person_D!)
        expense_A1?.addPersons(inDeal: self.personInDeal)
        
        // Configure inverse reference
        self.person_A?.addExpensePersons(inDealObject: expense_A1!)
        expense_A1?.event = self.event
        
        // Add expense to the event
        self.event?.addExpensesObject(expense_A1!)
        
        expense_A1?.price = NSDecimalNumber(value: 100.0)
        expense_A1?.event = self.event
        expense_A1?.creator = self.person_A
        
        do {
            try ExpenseManager.sharedInstance.saveContext(self.managedObjectContext)
        }
        catch {
            XCTAssert(false)
        }
        
        let items = BillManager.sharedInstance.calculateAvarageDifferenceForExpenses(self.event?.objectID, managedObjectContext: self.managedObjectContext)
        let set = Set(items)
        
        XCTAssertEqual(self.result, set)
    }
    
    func testMixedExpenses() {
        self.event?.addPersons(self.allPerson)
        
        // Create expense
        let expense_A1 = ExpenseManager.sharedInstance.createNewExpense(self.managedObjectContext)
        expense_A1?.title = "expense_A1"
        
        // Set people in deal
        self.personInDeal.remove(self.person_B!)
        expense_A1?.addPersons(inDeal: self.personInDeal)
        
        // Configure inverse reference
        self.person_A?.addExpensePersons(inDealObject: expense_A1!)
        self.person_C?.addExpensePersons(inDealObject: expense_A1!)
        self.person_D?.addExpensePersons(inDealObject: expense_A1!)
        expense_A1?.event = self.event
        
        // Add expense to the event
        self.event?.addExpensesObject(expense_A1!)
        
        expense_A1?.price = NSDecimalNumber(value: 300.0)
        expense_A1?.event = self.event
        expense_A1?.creator = self.person_A
        
        // Create expense
        let expense_B1 = ExpenseManager.sharedInstance.createNewExpense(self.managedObjectContext)
        expense_B1?.title = "expense_B1"
        
        // Set people in deal
        self.personInDeal.insert(self.person_B!)
        expense_B1?.addPersons(inDeal: self.personInDeal)
        
        // Configure inverse reference
        self.person_A?.addExpensePersons(inDealObject: expense_B1!)
        self.person_B?.addExpensePersons(inDealObject: expense_B1!)
        self.person_C?.addExpensePersons(inDealObject: expense_B1!)
        self.person_D?.addExpensePersons(inDealObject: expense_B1!)
        expense_B1?.event = self.event
        
        // Add expense to the event
        self.event?.addExpensesObject(expense_B1!)
        
        expense_B1?.price = NSDecimalNumber(value: 100.0)
        expense_B1?.event = self.event
        expense_B1?.creator = self.person_B
        
        // Create expense
        let expense_B2 = ExpenseManager.sharedInstance.createNewExpense(self.managedObjectContext)
        expense_B2?.title = "expense_B2"
        
        // Set people in deal
        expense_B2?.addPersons(inDeal: self.personInDeal)
        
        // Configure inverse reference
        self.person_A?.addExpensePersons(inDealObject: expense_B2!)
        self.person_B?.addExpensePersons(inDealObject: expense_B2!)
        self.person_C?.addExpensePersons(inDealObject: expense_B2!)
        self.person_D?.addExpensePersons(inDealObject: expense_B2!)
        expense_B2?.event = self.event
        
        // Add expense to the event
        self.event?.addExpensesObject(expense_B2!)
        
        expense_B2?.price = NSDecimalNumber(value: 300.0)
        expense_B2?.event = self.event
        expense_B2?.creator = self.person_B
        
        // Create expense
        let expense_C1 = ExpenseManager.sharedInstance.createNewExpense(self.managedObjectContext)
        expense_C1?.title = "expense_C1"
        
        // Set people in deal
        expense_C1?.addPersons(inDeal: self.personInDeal)
        
        // Configure inverse reference
        self.person_A?.addExpensePersons(inDealObject: expense_C1!)
        self.person_B?.addExpensePersons(inDealObject: expense_C1!)
        self.person_C?.addExpensePersons(inDealObject: expense_C1!)
        self.person_D?.addExpensePersons(inDealObject: expense_C1!)
        expense_C1?.event = self.event
        
        // Add expense to the event
        self.event?.addExpensesObject(expense_C1!)
        
        expense_C1?.price = NSDecimalNumber(value: 160.0)
        expense_C1?.event = self.event
        expense_C1?.creator = self.person_C
        
        do {
            try ExpenseManager.sharedInstance.saveContext(self.managedObjectContext)
        }
        catch {
            XCTAssert(false)
        }
        
        // Creating result data
        self.splitModel.personID = self.person_A?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 300.0 // expense_A1
        self.splitModel.differenceFromAvarage = -60.0
        self.result.insert(self.splitModel)
        
        self.splitModel = SplitModel()
        self.splitModel.personID = self.person_B?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 400.0 // expense_B1 + expense_B2
        self.splitModel.differenceFromAvarage = -260.0
        self.result.insert(self.splitModel)
        
        self.splitModel = SplitModel()
        self.splitModel.personID = self.person_C?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 160.0
        self.splitModel.differenceFromAvarage = 80.0
        self.result.insert(self.splitModel)
        
        self.splitModel = SplitModel()
        self.splitModel.personID = self.person_D?.objectID
        self.splitModel.type = SplitModelType.userExpense
        self.splitModel.value = 0.0
        self.splitModel.differenceFromAvarage = 240.0
        self.result.insert(self.splitModel)
        
        let items = BillManager.sharedInstance.calculateAvarageDifferenceForExpenses(self.event?.objectID, managedObjectContext: self.managedObjectContext)
        let set = Set(items)
        
        XCTAssertEqual(self.result, set)
    }
}
