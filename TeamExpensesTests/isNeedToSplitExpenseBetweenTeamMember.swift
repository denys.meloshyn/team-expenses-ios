//
//  isNeedToSplitExpenseBetweenTeamMember.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 08/09/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import XCTest
@testable import TeamExpenses

class isNeedToSplitExpenseBetweenTeamMember: XCTestCase {
    func testCreatorNotTakePartInDeal() {
        let childrenManagedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(ExpenseManager.sharedInstance.managedObjectContext)
        let event = ExpenseManager.sharedInstance.createNewEvent(childrenManagedObjectContext)
        let expense = ExpenseManager.sharedInstance.createNewExpense(childrenManagedObjectContext)
        
        if let expense = expense {
            expense.event = event
            event?.addExpensesObject(expense)
        }
        
        var personInDeal = Set<Person>()
        var allPerson = Set<Person>()
        
        let creator = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        let personA = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        let personB = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        let personC = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        let personD = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        
        expense?.creator = creator
        
        if let person = creator {
            allPerson.insert(person)
        }
        
        if let person = personA {
            personInDeal.insert(person)
            allPerson.insert(person)
        }
        
        if let person = personB {
            personInDeal.insert(person)
            allPerson.insert(person)
        }
        
        if let person = personC {
            personInDeal.insert(person)
            allPerson.insert(person)
        }
        
        if let person = personD {
            personInDeal.insert(person)
            allPerson.insert(person)
        }
        
        expense?.addPersons(inDeal: personInDeal)
        event?.addPersons(allPerson)
        
        let numberOfPersonToSplit = BillManager.sharedInstance.numberOfPersonToSplit(for: expense)
        XCTAssertEqual(true, BillManager.sharedInstance.isNeedToSplitExpenseBetweenTeamMember(numberOfExpenseMembers: numberOfPersonToSplit, person: personC!, expense: expense!))
    }
    
    func testPersonInDeal() {
        let childrenManagedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(ExpenseManager.sharedInstance.managedObjectContext)
        let event = ExpenseManager.sharedInstance.createNewEvent(childrenManagedObjectContext)
        let expense = ExpenseManager.sharedInstance.createNewExpense(childrenManagedObjectContext)
        
        if let expense = expense {
            expense.event = event
            event?.addExpensesObject(expense)
        }
        
        var personInDeal = Set<Person>()
        var allPerson = Set<Person>()
        
        let creator = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        let personA = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        let personB = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        let personC = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        let personD = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        
        expense?.creator = creator
        
        if let person = creator {
            personInDeal.insert(person)
            allPerson.insert(person)
        }
        
        if let person = personA {
            personInDeal.insert(person)
            allPerson.insert(person)
        }
        
        if let person = personB {
            personInDeal.insert(person)
            allPerson.insert(person)
        }
        
        if let person = personC {
            personInDeal.insert(person)
            allPerson.insert(person)
        }
        
        if let person = personD {
            personInDeal.insert(person)
            allPerson.insert(person)
        }
        
        expense?.addPersons(inDeal: personInDeal)
        event?.addPersons(allPerson)
        
        let numberOfPersonToSplit = BillManager.sharedInstance.numberOfPersonToSplit(for: expense)
        XCTAssertEqual(true, BillManager.sharedInstance.isNeedToSplitExpenseBetweenTeamMember(numberOfExpenseMembers: numberOfPersonToSplit, person: personC!, expense: expense!))
    }
    
    func testPersonNotInDeal() {
        let childrenManagedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(ExpenseManager.sharedInstance.managedObjectContext)
        let event = ExpenseManager.sharedInstance.createNewEvent(childrenManagedObjectContext)
        let expense = ExpenseManager.sharedInstance.createNewExpense(childrenManagedObjectContext)
        
        if let expense = expense {
            expense.event = event
            event?.addExpensesObject(expense)
        }
        
        var personInDeal = Set<Person>()
        var allPerson = Set<Person>()
        
        let creator = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        let personA = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        let personB = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        let personC = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        let personD = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        
        expense?.creator = creator
        
        if let person = creator {
            personInDeal.insert(person)
            allPerson.insert(person)
        }
        
        if let person = personA {
            allPerson.insert(person)
        }
        
        if let person = personB {
            personInDeal.insert(person)
            allPerson.insert(person)
        }
        
        if let person = personC {
            personInDeal.insert(person)
            allPerson.insert(person)
        }
        
        if let person = personD {
            personInDeal.insert(person)
            allPerson.insert(person)
        }
        
        expense?.addPersons(inDeal: personInDeal)
        event?.addPersons(allPerson)
        
        let numberOfPersonToSplit = BillManager.sharedInstance.numberOfPersonToSplit(for: expense)
        XCTAssertEqual(false, BillManager.sharedInstance.isNeedToSplitExpenseBetweenTeamMember(numberOfExpenseMembers: numberOfPersonToSplit, person: personA!, expense: expense!))
    }
}
