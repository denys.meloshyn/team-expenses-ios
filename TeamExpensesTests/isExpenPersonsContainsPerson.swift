//
//  isExpenPersonsContainsPerson.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 08/09/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import XCTest
@testable import TeamExpenses

class isExpenPersonsContainsPerson: XCTestCase {
    func testIsExpenPersonsContainsPersonUserInList() {
        let childrenManagedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(ExpenseManager.sharedInstance.managedObjectContext)
        let expense = ExpenseManager.sharedInstance.createNewExpense(childrenManagedObjectContext)
        
        var persons = Set<Person>()
        
        for _ in 0..<10 {
            if let person = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext) {
                persons.insert(person)
            }
        }
        expense?.addPersons(inDeal: persons)
        
        let personFromList = persons.first
        
        XCTAssertEqual(true, BillManager.sharedInstance.isExpenPersonsContainsPerson(expense: expense, person: personFromList))
    }
    
    func testIsExpenPersonsContainsPersonUserNotInList() {
        let childrenManagedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(ExpenseManager.sharedInstance.managedObjectContext)
        let expense = ExpenseManager.sharedInstance.createNewExpense(childrenManagedObjectContext)
        
        var persons = Set<Person>()
        
        for _ in 0..<10 {
            if let person = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext) {
                persons.insert(person)
            }
        }
        expense?.addPersons(inDeal: persons)
        
        let personNotInList = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        
        XCTAssertEqual(false, BillManager.sharedInstance.isExpenPersonsContainsPerson(expense: expense, person: personNotInList))
    }
    
    func testIsExpenPersonsContainsWithEmptyList() {
        let childrenManagedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(ExpenseManager.sharedInstance.managedObjectContext)
        let expense = ExpenseManager.sharedInstance.createNewExpense(childrenManagedObjectContext)
        
        let personNotInList = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        
        XCTAssertEqual(false, BillManager.sharedInstance.isExpenPersonsContainsPerson(expense: expense, person: personNotInList))
    }
}
