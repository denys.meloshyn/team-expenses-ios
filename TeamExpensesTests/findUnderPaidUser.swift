//
//  findUnderPaidUser.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 08/09/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import XCTest
@testable import TeamExpenses

class findUnderPaidUser: XCTestCase {
    func testFindUnderPaidUserCorrectValue() {
        var model = SplitModel()
        var items = [SplitModel]()
        var correctModel = SplitModel()
        
        correctModel = SplitModel()
        correctModel.differenceFromAvarage = 100.0
        items.append(correctModel)
        
        model = SplitModel()
        model.differenceFromAvarage = -10.0
        items.append(model)
        
        model = SplitModel()
        model.differenceFromAvarage = -20.0
        items.append(correctModel)
        
        model = SplitModel()
        model.differenceFromAvarage = -0.1
        items.append(model)
        
        let result = BillManager.sharedInstance.findUnderPaidUser(items)
        XCTAssertEqual(correctModel.differenceFromAvarage, result?.differenceFromAvarage)
    }
    
    func testFindUnderPaidUserNotFoundValue() {
        var model = SplitModel()
        var items = [SplitModel]()
        
        model = SplitModel()
        model.differenceFromAvarage = -100.0
        items.append(model)
        
        model = SplitModel()
        model.differenceFromAvarage = 0.0
        items.append(model)
        
        model = SplitModel()
        model.differenceFromAvarage = -20.0
        items.append(model)
        
        model = SplitModel()
        model.differenceFromAvarage = -0.1
        items.append(model)
        
        let result = BillManager.sharedInstance.findUnderPaidUser(items)
        XCTAssertNil(result)
    }
    
    func testFindUnderPaidUserMoreThanOneCorrect() {
        var model = SplitModel()
        var items = [SplitModel]()
        
        model = SplitModel()
        model.differenceFromAvarage = 100.0
        items.append(model)
        
        model = SplitModel()
        model.differenceFromAvarage = 10.0
        items.append(model)
        
        model = SplitModel()
        model.differenceFromAvarage = 20.0
        items.append(model)
        
        model = SplitModel()
        model.differenceFromAvarage = 0.0
        items.append(model)
        
        let result = BillManager.sharedInstance.findUnderPaidUser(items)
        if let result = result {
            XCTAssertGreaterThan(result.differenceFromAvarage, 0.0)
        }
        else {
            XCTAssertNotNil(result)
        }
    }
    
    func testFindUnderPaidUserEmptyInputParameters() {
        let items = [SplitModel]()
        
        let result = BillManager.sharedInstance.findUnderPaidUser(items)
        XCTAssertNil(result)
    }
}
