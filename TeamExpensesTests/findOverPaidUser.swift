//
//  findOverPaidUser.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 08/09/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import XCTest
@testable import TeamExpenses

class findOverPaidUser: XCTestCase {
    func testFindOverPaidUserCorrectValue() {
        var model = SplitModel()
        var items = [SplitModel]()
        var correctModel = SplitModel()
        
        model = SplitModel()
        model.differenceFromAvarage = 100.0
        items.append(model)
        
        model = SplitModel()
        model.differenceFromAvarage = 0.0
        items.append(model)
        
        correctModel = SplitModel()
        correctModel.differenceFromAvarage = -20.0
        items.append(correctModel)
        
        model = SplitModel()
        model.differenceFromAvarage = 0.1
        items.append(model)
        
        let result = BillManager.sharedInstance.findOverPaidUser(items)
        XCTAssertEqual(correctModel.differenceFromAvarage, result?.differenceFromAvarage)
    }
    
    func testFindOverPaidUserNotFoundValue() {
        var model = SplitModel()
        var items = [SplitModel]()
        
        model = SplitModel()
        model.differenceFromAvarage = 100.0
        items.append(model)
        
        model = SplitModel()
        model.differenceFromAvarage = 0.0
        items.append(model)
        
        model = SplitModel()
        model.differenceFromAvarage = 20.0
        items.append(model)
        
        model = SplitModel()
        model.differenceFromAvarage = 0.1
        items.append(model)
        
        let result = BillManager.sharedInstance.findOverPaidUser(items)
        XCTAssertNil(result)
    }
    
    func testFindOverPaidUserMoreThanOneCorrect() {
        var model = SplitModel()
        var items = [SplitModel]()
        
        model = SplitModel()
        model.differenceFromAvarage = -100.0
        items.append(model)
        
        model = SplitModel()
        model.differenceFromAvarage = -10.0
        items.append(model)
        
        model = SplitModel()
        model.differenceFromAvarage = -20.0
        items.append(model)
        
        model = SplitModel()
        model.differenceFromAvarage = -0.0
        items.append(model)
        
        let result = BillManager.sharedInstance.findOverPaidUser(items)
        if let result = result {
            XCTAssertLessThan(result.differenceFromAvarage, 0.0)
        }
        else {
            XCTAssertNotNil(result)
        }
    }
    
    func testFindOverPaidUserEmptyInputParameters() {
        let items = [SplitModel]()
        
        let result = BillManager.sharedInstance.findOverPaidUser(items)
        XCTAssertNil(result)
    }
}
