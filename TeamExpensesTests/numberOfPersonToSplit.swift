//
//  numberOfPersonToSplit.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 08/09/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import XCTest
@testable import TeamExpenses

class numberOfPersonToSplit: XCTestCase {
    func testNumberOfPersonToSplitNoPersons() {
        let childrenManagedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(ExpenseManager.sharedInstance.managedObjectContext)
        let expense = ExpenseManager.sharedInstance.createNewExpense(childrenManagedObjectContext)
        
        XCTAssertEqual(0, BillManager.sharedInstance.numberOfPersonToSplit(for: expense))
    }
    
    func testNumberOfPersonToSplitOnlyWithCreator() {
        let childrenManagedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(ExpenseManager.sharedInstance.managedObjectContext)
        let expense = ExpenseManager.sharedInstance.createNewExpense(childrenManagedObjectContext)
        let person = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        
        expense?.creator = person
        if let person = person {
            expense?.addPersons(inDealObject: person)
        }
        
        XCTAssertEqual(0, BillManager.sharedInstance.numberOfPersonToSplit(for: expense))
    }
    
    func testNumberOfPersonToSplitEqual() {
        let childrenManagedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(ExpenseManager.sharedInstance.managedObjectContext)
        let expense = ExpenseManager.sharedInstance.createNewExpense(childrenManagedObjectContext)
        let creator = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        
        expense?.creator = creator
        if let creator = creator {
            expense?.addPersons(inDealObject: creator)
        }
        
        for _ in 0..<10 {
            if let person = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext) {
                expense?.addPersons(inDealObject: person)
            }
        }
        
        var result = 0
        if let personsInDeal = expense?.personsInDeal {
            result = personsInDeal.count
        }
        
        XCTAssertEqual(result, BillManager.sharedInstance.numberOfPersonToSplit(for: expense))
    }
    
    func testNumberOfPersonToSplitCreatorPayedForAll() {
        let childrenManagedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(ExpenseManager.sharedInstance.managedObjectContext)
        let expense = ExpenseManager.sharedInstance.createNewExpense(childrenManagedObjectContext)
        let creator = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext)
        
        expense?.creator = creator
        
        for _ in 0..<10 {
            if let person = ExpenseManager.sharedInstance.createNewPerson(childrenManagedObjectContext) {
                expense?.addPersons(inDealObject: person)
            }
        }
        
        var result = 0
        if let personsInDeal = expense?.personsInDeal {
            result = personsInDeal.count
        }
        
        XCTAssertEqual(result, BillManager.sharedInstance.numberOfPersonToSplit(for: expense))
    }
}
