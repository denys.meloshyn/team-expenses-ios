//
//  ExpenseEditInteractionTests.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 29/08/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import XCTest
@testable import TeamExpenses

class ExpenseEditInteractionTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitialConfigurationWithoutModel() {
        let interaction = ExpenseEditInteraction()
        interaction.initialConfiguration()
        
        XCTAssertNotNil(interaction.currentExpenseModel)
    }
    
    func testInitialConfigurationWithModel() {
        let managedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(ExpenseManager.sharedInstance.managedObjectContext)
        let model = ExpenseManager.sharedInstance.createNewExpense(managedObjectContext)
        
        let interaction = ExpenseEditInteraction()
        interaction.childrenManagedObjectContext = managedObjectContext
        interaction.currentExpenseModelID = model?.objectID
        interaction.initialConfiguration()
        
        XCTAssertEqual(model, interaction.currentExpenseModel)
    }
}
