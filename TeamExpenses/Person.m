//
//  Person+CoreDataClass.m
//  
//
//  Created by Denys.Meloshyn on 08/09/16.
//
//

#import "Person.h"
#import "Event.h"
#import "Expense.h"

#import "TeamExpenses-Swift.h"

@implementation Person

- (NSString *)userName
{
    if ([self.isCurrentUser boolValue]) {
        return [LocalisationManager me];
    }
    
    return self.name;
}

- (BOOL)isUserCurrent
{
    return [self.isCurrentUser boolValue];
}

@end
