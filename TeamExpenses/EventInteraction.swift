//
//  EventInteraction.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 23/08/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit
import CoreData

protocol EventInteractionDelegate: class {
    func createNewPerson()
    func createNewExpense()
    func showResultScreen(with event: Event?)
    func didChangeContent(for state:EventState)
    func willChangeContent(for state:EventState)
    func changed(at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?, for state:EventState)
}

class EventInteraction: NSObject, NSFetchedResultsControllerDelegate {
    private var personFetchController: NSFetchedResultsController <BaseModel>?
    private var expensesFetchedResultsController: NSFetchedResultsController <Expense>?
    private let managedObjectContext = ExpenseManager.sharedInstance.managedObjectContext
    
    var currentEvent: Event?
    var currentEventID: NSManagedObjectID? {
        didSet {
            if let currentEventID = self.currentEventID {
                self.currentEvent = self.managedObjectContext.object(with: currentEventID) as? Event
            }
            
            // Configure expense fetch controller
            self.expensesFetchedResultsController = ExpenseManager.sharedInstance.expensesFetchControllerForEvent(self.currentEventID, managedObjectContext: self.managedObjectContext)
            self.expensesFetchedResultsController?.delegate = self
            
            do {
                try self.expensesFetchedResultsController?.performFetch()
            }
            catch {
                AnalyticsManager.trackError("EventViewController error fetching expenses \(error)")
            }
            
            // Configure person fetch controller
            self.personFetchController = ExpenseManager.sharedInstance.eventPersonFetchController(self.currentEventID, managedObjectContext: self.managedObjectContext)
            self.personFetchController?.delegate = self;
            
            do {
                try self.personFetchController?.performFetch()
            }
            catch {
                AnalyticsManager.trackError("EventViewController error fetching persons \(error)")
            }
        }
    }
    weak var delegate: EventInteractionDelegate?
    
    // MARK: - NSFetchedResultsControllerDelegate methds
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        if controller === self.expensesFetchedResultsController {
            self.delegate?.willChangeContent(for: EventState.Expense)
        }
        else {
            self.delegate?.willChangeContent(for: EventState.Person)
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        if controller === self.expensesFetchedResultsController {
            self.delegate?.didChangeContent(for: EventState.Expense)
        }
        else {
            self.delegate?.didChangeContent(for: EventState.Person)
        }
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        if controller === self.expensesFetchedResultsController {
            self.delegate?.changed(at: indexPath, for: type, newIndexPath: newIndexPath, for: EventState.Expense)
        }
        else {
            self.delegate?.changed(at: indexPath, for: type, newIndexPath: newIndexPath, for: EventState.Person)
        }
    }
    
    // MARK: - Public methds
    
    func showResults() {
        AnalyticsManager.track(kEventScreen, action: kSplitAction, label: "")
        
        self.delegate?.showResultScreen(with: self.currentEvent)
    }
    
    func addNewExpense() {
        AnalyticsManager.track(kEventScreen, action: kCreateExpenseAction, label: "")
        
        self.delegate?.createNewExpense()
    }
    
    func addNewTeamMember() {
        AnalyticsManager.track(kEventScreen, action: kCreateTeamMemberAction, label: "")
        
        self.delegate?.createNewPerson()
    }
    
    func numberOfExpenses() -> Int {
        if let sections = self.expensesFetchedResultsController?.sections {
            if let sectionModel = sections.first {
                return sectionModel.numberOfObjects
            }
        }
        
        return 0
    }
    
    func expense(at indexPath: IndexPath) -> Expense? {
        let expense = self.expensesFetchedResultsController?.object(at: indexPath)
        
        return expense
    }
    
    func numberOfPersons() -> Int {
        if let sections = self.personFetchController?.sections {
            if let sectionModel = sections.first {
                return sectionModel.numberOfObjects
            }
        }
        
        return 0
    }
    
    func person(at indexPath: IndexPath) -> Person? {
        let person = self.personFetchController?.object(at: indexPath) as? Person
        
        return person
    }
    
    func trackOpenningExpenseDetailScreen() {
        AnalyticsManager.track(kEventScreen, action: kEditExpenseAction, label: "")
    }
    
    func trackOpenningContactDetailScreen() {
        AnalyticsManager.track(kEventScreen, action: kEditTeamMemberAction, label: "")
    }
    
    func trackCancelingOfDeletingExpense() {
        AnalyticsManager.track(kEventScreen, action: kCancelDeleteExpenseAction, label: "")
    }
    
    func deleteExpense(_ expense: Expense?) {
        AnalyticsManager.track(kEventScreen, action: kExpenseDeleteAction, label: "")
        
        if let expense = expense {
            self.managedObjectContext.delete(expense)
            
            do {
                try ExpenseManager.sharedInstance.saveContext(self.managedObjectContext)
            }
            catch {
                AnalyticsManager.trackError("EventViewController error saving context during delete \(error)")
            }
        }
    }
    
    func deletePerson(_ person: Person?) {
        AnalyticsManager.track(kEventScreen, action: kTeamMemberDeleteAction, label: "")
        
        if let currentEventID = self.currentEventID {
            if let person = person {
                ExpenseManager.sharedInstance.removeAllExpensesForPerson(currentEventID, personModelID: person.objectID, managedObjectContext: self.managedObjectContext)
                
                do {
                    try ExpenseManager.sharedInstance.saveContext(self.managedObjectContext)
                }
                catch {
                    AnalyticsManager.trackError("EventViewController error saving context during delete \(error)")
                }
            }
        }
    }
}
