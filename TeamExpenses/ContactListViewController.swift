//
//  ContactListViewController.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 29/10/15.
//  Copyright © 2015 Denys Meloshyn. All rights reserved.
//

import UIKit
import CoreData
import XCGLogger

enum ContactPageType: Int {
    case `default`
    case changeExpenseOwner
    case splitExpenseBetweenPeople
};

class ContactListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate {
    // MARK: - IBOutlet properties
    
    @IBOutlet private var tableView: UITableView?
    
    // MARK: - Public properties
    
    var currentEventID: NSManagedObjectID?
    var currentExpenseID: NSManagedObjectID?
    var contactPageType = ContactPageType.default
    var managedObjectContext: NSManagedObjectContext?
    
    // MARK: - Private properties
    
    private var currentEvent: Event?
    private var currentExpense: Expense?
    private var childrenManagedObjectContext: NSManagedObjectContext?
    private var fetchedResultsController: NSFetchedResultsController<BaseModel>?
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.childrenManagedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(self.managedObjectContext)
        
        // Get event model by ID
        if let currentEventID = self.currentEventID {
            self.currentEvent = self.childrenManagedObjectContext?.object(with: currentEventID) as? Event
        }
        
        // Get expense model by ID
        if let currentExpenseID = self.currentExpenseID {
            self.currentExpense = self.childrenManagedObjectContext?.object(with: currentExpenseID) as? Expense
        }
        
        // Configure fetchedResultsController
        if let childrenManagedObjectContext = self.childrenManagedObjectContext {
            self.fetchedResultsController = ExpenseManager.sharedInstance.eventPersonFetchController(self.currentEventID, managedObjectContext: childrenManagedObjectContext)
            self.fetchedResultsController?.delegate = self
        }
        
        do {
            try self.fetchedResultsController?.performFetch()
        }
        catch let error as NSError {
            AnalyticsManager.trackError("ContactListViewController: couldn't fetch team mebers for event \(self.currentEvent.debugDescription) \n \(error)")
        }
        
        let addBarButtonItem = UIBarButtonItem(image: UIImage(named: "account_plus"), style: UIBarButtonItemStyle.plain, target: self, action: #selector(ContactListViewController.addNewTeamMember))
        let saveBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.save, target: self, action: #selector(ContactListViewController.save))
        
        self.navigationItem.rightBarButtonItems = [addBarButtonItem, saveBarButtonItem]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tableView?.reloadData()
        
        AnalyticsManager.trackScreenVisible(name: kContactListScreen)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ContactListEditSegue" {
            if let viewController = segue.destination as? ContactEditViewController {
                viewController.managedObjectContext = self.childrenManagedObjectContext
                viewController.currentEventID = self.currentEventID
                viewController.currentExpenseID = self.currentExpenseID
                
                if let indexPath = self.tableView?.indexPathForSelectedRow {
                    self.tableView?.deselectRow(at: indexPath, animated: false)
                }
            }
        }
    }
    
    // MARK: - NSFetchedResultsControllerDelegate methds
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        XCGLogger.default.info("ContactListViewController: upadte contact list \(controller.managedObjectContext)")
        self.tableView?.reloadData()
    }
    
    // MARK: - UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sections = self.fetchedResultsController?.sections {
            return sections[ section ].numberOfObjects
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ContactCell") {
            let teamMember = self.fetchedResultsController?.object(at: indexPath)

            self.configureCell(cell, teamMember: teamMember as? Person)
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.fetchedResultsController?.object(at: indexPath) as? Person
        
        if self.contactPageType == ContactPageType.changeExpenseOwner {
            // Change expense creator
            AnalyticsManager.track(kContactListScreen, action: kChangeExpenseOwnerAction, label: "")
            
            if let person = model {
                self.currentExpense?.creator = person
            }
            
            ExpenseManager.sharedInstance.saveManagedObjectContextWithPerformBlock(self.childrenManagedObjectContext, completionBlock: { [weak self] (response, error) -> (Void) in
                DispatchQueue.main.async(execute: { () -> Void in
                    self?.closeViewController()
                })
            })
        }
        else if self.contactPageType == ContactPageType.default {
            self.performSegue(withIdentifier: "ContactListEditSegue", sender: nil)
        }
        else if self.contactPageType == ContactPageType.splitExpenseBetweenPeople {
            if let splitBetweenPersons = self.currentExpense?.personsInDeal {
                var findExpensePerson: Person?
                
                if let selectedPerson = model {
                    for expensePerson in splitBetweenPersons {
                        if expensePerson == selectedPerson {
                            findExpensePerson = expensePerson
                            continue
                        }
                    }
                }
                
                if let findExpensePerson = findExpensePerson {
                    if let splitBetweenPersons = self.currentExpense?.personsInDeal, splitBetweenPersons.count > 1 {
                        // Configure inverse reference
                        if let currentExpenseModel = self.currentExpense {
                            findExpensePerson.removeExpensePersons(inDealObject: currentExpenseModel)
                        }
                        
                        self.currentExpense?.removePersons(inDealObject: findExpensePerson)
                    }
                }
                else {
                    if let findExpensePerson = model {
                        // Configure inverse reference
                        if let currentExpenseModel = self.currentExpense {
                            findExpensePerson.addExpensePersons(inDealObject: currentExpenseModel)
                        }
                        
                        self.currentExpense?.addPersons(inDealObject: findExpensePerson)
                    }
                }
            }
            
            self.tableView?.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        }
    }
    
    // MARK: - Private methods
    
    func addNewTeamMember() {
        AnalyticsManager.track(kContactListScreen, action: kCreateTeamMemberAction, label: "")
        
        self.performSegue(withIdentifier: "ContactListEditSegue", sender: nil)
    }
    
    private func configureCell(_ cell: UITableViewCell?, teamMember: Person?) {
        cell?.tintColor = Constatnts.defaultActiveColor()
        
        cell?.textLabel?.text = NameFormatter.userName(teamMember, replace: LocalisationManager.expenseSplittedWithMe())
        
        cell?.accessoryType = UITableViewCellAccessoryType.none
        cell?.selectionStyle = UITableViewCellSelectionStyle.default
        
        if self.contactPageType == ContactPageType.changeExpenseOwner {
            if let userID = teamMember?.objectID {
                if let creatorID = self.currentExpense?.creator?.objectID , creatorID == userID {
                    cell?.accessoryType = UITableViewCellAccessoryType.checkmark
                    cell?.selectionStyle = UITableViewCellSelectionStyle.none
                }
            }
        }
        else if self.contactPageType == ContactPageType.splitExpenseBetweenPeople {
            if let splitBetweenPersons = self.currentExpense?.personsInDeal {
                var findExpensePerson: Person?
                
                if let selectedPerson = teamMember {
                    for expensePerson in splitBetweenPersons {
                        if expensePerson == selectedPerson {
                            findExpensePerson = expensePerson
                            continue
                        }
                    }
                }
                
                if let _ = findExpensePerson {
                    cell?.accessoryType = UITableViewCellAccessoryType.checkmark
                }
                else {
                    cell?.accessoryType = UITableViewCellAccessoryType.none
                }
            }
        }
    }
    
    // MARK: - Public methods
    
    func closeViewController() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func save() {
        ExpenseManager.sharedInstance.saveManagedObjectContextWithPerformBlock(self.childrenManagedObjectContext, completionBlock: { [weak self] (response, error) -> (Void) in
            DispatchQueue.main.async(execute: { () -> Void in
                self?.closeViewController()
            })
        })
    }
}
