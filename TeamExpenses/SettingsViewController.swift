//
//  SettingsViewController.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 27/01/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    @IBOutlet private var loginLogoutButton: UIButton?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserInfo.sharedInstance.isUserLoggedIn() {
            self.loginLogoutButton?.setTitle("Logout", for: UIControlState())
            self.loginLogoutButton?.setTitle("Logout", for: UIControlState.highlighted)
        }
        else {
            self.loginLogoutButton?.setTitle("Login", for: UIControlState())
            self.loginLogoutButton?.setTitle("Login", for: UIControlState.highlighted)
        }
    }
    
    @IBAction func loginLogoutAction() {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let loginPage = storyBoard.instantiateViewController(withIdentifier: "LoginViewController")
        
        if UserInfo.sharedInstance.isUserLoggedIn() {
            ExpenseManager.sharedInstance.dropDataBase({ (response, error) -> (Void) in
                let appDelegate = UIApplication.shared.delegate as? AppDelegate
                appDelegate?.window?.rootViewController = loginPage
            })
        }
        else {
            self.navigationController?.pushViewController(loginPage, animated: true)
        }
    }
}
