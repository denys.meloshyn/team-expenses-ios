//
//  Expense+CoreDataProperties.h
//  
//
//  Created by Denys.Meloshyn on 08/09/16.
//
//

#import "Expense.h"


NS_ASSUME_NONNULL_BEGIN

@interface Expense (CoreDataProperties)

+ (NSFetchRequest<Expense *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *creationDate;
@property (nullable, nonatomic, copy) NSDecimalNumber *price;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, retain) Person *creator;
@property (nullable, nonatomic, retain) Event *event;
@property (nullable, nonatomic, retain) NSSet<Person *> *personsInDeal;

@end

@interface Expense (CoreDataGeneratedAccessors)

- (void)addPersonsInDealObject:(Person *)value;
- (void)removePersonsInDealObject:(Person *)value;
- (void)addPersonsInDeal:(NSSet<Person *> *)values;
- (void)removePersonsInDeal:(NSSet<Person *> *)values;

@end

NS_ASSUME_NONNULL_END
