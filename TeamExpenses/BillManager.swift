//
//  BillManager.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 21/01/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import CoreData
import XCGLogger
import Foundation
import Crashlytics

class BillManager: NSObject {
    class var sharedInstance: BillManager {
        struct Static {
            static let instance: BillManager = BillManager()
        }
        
        return Static.instance
    }
    
    // MARK: - Private methods
    
    internal func findOverPaidUser(_ items: [SplitModel]) -> SplitModel? {
        let result = items.filter { (model) -> Bool in
            model.differenceFromAvarage < 0.0
        }
        
        return result.first
    }
    
    internal func findUnderPaidUser(_ items: [SplitModel]) -> SplitModel? {
        let result = items.filter { (model) -> Bool in
            model.differenceFromAvarage > 0.0
        }
        
        return result.first
    }
    
    internal func findSplitModelForPerson(_ items: [SplitModel], personID: NSManagedObjectID) -> SplitModel? {
        let items = items.filter { (model) -> Bool in
            model.personID == personID
        }
        
        return items.first
    }
    
    internal func isExpenPersonsContainsPerson(expense: Expense?, person: Person?) -> Bool {
        if let expense = expense {
            if let person = person {
                if let personsInDeal = expense.personsInDeal {
                    return personsInDeal.contains(person)
                }
            }
        }
        
        return false
    }
    
    internal func numberOfPersonToSplit(for expense: Expense?) -> Int {
        if let personsInDeal = expense?.personsInDeal {
            // There are only one person in the split list
            if personsInDeal.count == 1 {
                if self.isExpenPersonsContainsPerson(expense: expense, person: expense?.creator) {
                    // The single person in the list is a creator of the expense
                    return 0
                }
            }
            
            // Include all persons
            return personsInDeal.count
        }
        
        return 0
    }
    
    internal func isNeedToSplitExpenseBetweenTeamMember(numberOfExpenseMembers: Int, person: Person, expense: Expense) -> Bool {
        // Do we need to include this expense to person?
        var isNeedToSeparate = false
        
        if let persons = expense.event?.persons {
            if numberOfExpenseMembers != persons.count {
                // Expense is splitted between group of people
                // We need to understand if we need to include it to person or no
                
                if self.isExpenPersonsContainsPerson(expense: expense, person: person) {
                    isNeedToSeparate = true
                }
                else {
                    if let expenseCreator = expense.creator {
                        // Person is the creator of this expense
                        if person.objectID == expenseCreator.objectID {
                            isNeedToSeparate = true
                        }
                    }
                }
            }
            else {
                // Number of split person is equal to number of all team members: split between all
                isNeedToSeparate = true
            }
        }
        
        return isNeedToSeparate
    }
    
    // MARK: - Public methods
    
    func splitExpenses(_ eventID: NSManagedObjectID?, managedObjectContext: NSManagedObjectContext) -> [SplitModel] {
        let items = self.calculateAvarageDifferenceForExpenses(eventID, managedObjectContext: managedObjectContext)
        
        let result = self.splitExpenseBetweenUsers(items)
        
        return result
    }
    
    func splitExpenseBetweenUsers(_ userBills: [SplitModel]) -> [SplitModel] {
        var result = [SplitModel]()
        var splitModel = SplitModel()
        
        // If there are only one team member in event (or no one) who paid, there are nothing to split
        if userBills.count <= 1 {
            return result
        }
        
        while let overPaidModel = self.findOverPaidUser(userBills) {
            if let underPaidModel = self.findUnderPaidUser(userBills) {
                splitModel = SplitModel()
                splitModel.type = SplitModelType.split
                splitModel.overPaidPersonID = overPaidModel.personID
                splitModel.underPaidPersonID = underPaidModel.personID
                
                if fabs(overPaidModel.differenceFromAvarage) > underPaidModel.differenceFromAvarage {
                    splitModel.value = underPaidModel.differenceFromAvarage
                    overPaidModel.differenceFromAvarage += underPaidModel.differenceFromAvarage
                    underPaidModel.differenceFromAvarage = 0.0
                }
                else {
                    splitModel.value = fabs(overPaidModel.differenceFromAvarage)
                    underPaidModel.differenceFromAvarage += overPaidModel.differenceFromAvarage
                    overPaidModel.differenceFromAvarage = 0.0
                }
                
                result.append(splitModel)
            }
            else {
                overPaidModel.differenceFromAvarage = 0.0
            }
        }
        
        return result
    }
    
    func calculateAvarageDifferenceForExpenses(_ eventID: NSManagedObjectID?, managedObjectContext: NSManagedObjectContext) -> [SplitModel] {
        var userBills = [SplitModel]()
        
        let fetchedResultsControllerPerson = ExpenseManager.sharedInstance.eventPersonFetchController(eventID, managedObjectContext: managedObjectContext)
        let fetchedResultsControllerExpenses = ExpenseManager.sharedInstance.expensesFetchControllerForEvent(eventID, managedObjectContext: managedObjectContext)
        
        do {
            try fetchedResultsControllerPerson.performFetch()
            try fetchedResultsControllerExpenses.performFetch()
        }
        catch {
            XCGLogger.default.error("SplitExpensesViewController: couldn't fetch team mebers\n \(error)")
        }
        
        // Get each expense model
        if let sectionExpenses = fetchedResultsControllerExpenses.sections {
            for sectionExpense in 0..<sectionExpenses.count {
                let sectionExpenseInfo = sectionExpenses[sectionExpense]
                
                for rowExpense in 0..<sectionExpenseInfo.numberOfObjects {
                    let indexPathExpense = IndexPath(row: rowExpense, section: sectionExpense)
                    let expense = fetchedResultsControllerExpenses.object(at: indexPathExpense)
                    
                    XCGLogger.default.info("\(expense.title ?? ""), \(expense.price ?? 0)\n")
                    
                    // Use this value to store number of people with whom we need to split this expense
                    let numberOfExpenseMembers = self.numberOfPersonToSplit(for: expense)
                    
                    if numberOfExpenseMembers == 0 {
                        XCGLogger.default.info("Skip expense")
                        continue
                    }
                    
                    // Get each person model
                    if let sectionPersons = fetchedResultsControllerPerson.sections {
                        for sectionPerson in 0..<sectionPersons.count {
                            let sectionInfoPerson = sectionPersons[sectionPerson]
                            
                            for rowPerson in 0..<sectionInfoPerson.numberOfObjects {
                                let indexPathPerson = IndexPath(row: rowPerson, section: sectionPerson)
                                if let person = fetchedResultsControllerPerson.object(at: indexPathPerson) as? Person {
                                    let isNeedToSeparate = self.isNeedToSplitExpenseBetweenTeamMember(numberOfExpenseMembers: numberOfExpenseMembers, person: person, expense: expense)
                                    
                                    if !isNeedToSeparate {
                                        continue
                                    }
                                    
                                    var splitModel = self.findSplitModelForPerson(userBills, personID: person.objectID)
                                    
                                    if splitModel == nil {
                                        splitModel = SplitModel()
                                        
                                        if let splitModel = splitModel {
                                            splitModel.personID = person.objectID
                                            splitModel.type = SplitModelType.userExpense
                                            
                                            userBills.append(splitModel)
                                        }
                                    }
                                    
                                    if let price = expense.price?.doubleValue {
                                        let avarage = price / Double(numberOfExpenseMembers)
                                        
                                        if let creator = expense.creator {
                                            // Is expence was paid by current user?
                                            if creator.objectID == person.objectID {
                                                splitModel?.value += price
                                                
                                                if self.isExpenPersonsContainsPerson(expense: expense, person: creator) {
                                                    // Decrease creator debt
                                                    splitModel?.differenceFromAvarage += (avarage - price)
                                                }
                                                else {
                                                    // Creator just paid for other team members
                                                    splitModel?.differenceFromAvarage -= price
                                                }
                                            }
                                            else {
                                                // Increase user debt
                                                splitModel?.differenceFromAvarage += avarage
                                            }
                                        }
                                    }
                                    
//                                    DDLogWrapper.logInfo("\(userBills)\n")
                                }
                            }
                        }
                    }
                    
//                    DDLogWrapper.logInfo("-------")
                }
            }
        }
        
        for userBill in userBills {
            userBill.differenceFromAvarage = PriceFormatter.roundDecimalForTwoPlacesToDouble(userBill.differenceFromAvarage)
        }
        
//        DDLogWrapper.logInfo("\(userBills)")
        
        return userBills
    }
}
