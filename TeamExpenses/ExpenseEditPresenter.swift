//
//  ExpenseEditPresenter.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 25/08/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit

protocol ExpenseEditPresenterDelegate: class {
    func hideKeyboard()
    func priceIsValid()
    func showTitle(_ title: String?)
    func showPrice(_ price: String?)
    func presentInterfaceForNewExpense()
    func pageSaveState(is enabled: Bool)
    func showPersons(inDeal value: String?)
    func showKeyboard(with height: CGFloat)
    func presentInterfaceForExistedExpense()
    func activeTextField() -> ExpenseEditField
    func showPriceErrorMessage(_ message: String)
    func presentCreationDateField(_ date: String?)
    func showExpenseCreatorButton(with title: String?)
    func dismissKeyboard(for textField: ExpenseEditField)
    func activateTextField(_ textField: ExpenseEditField)
    func updateTextBorder(for textField: ExpenseEditField)
}

enum ExpenseEditField {
    case Price
    case Title
    case CreationDate
    case None
}

class ExpenseEditPresenter: NSObject, ExpenseEditInteractionDelegate, UITextFieldDelegate {
    var router = ExpenseEditRouter()
    var interaction = ExpenseEditInteraction()
    weak var viewController: UIViewController?
    weak var delegate: ExpenseEditPresenterDelegate?
    
    func initialConfiguration() {
        self.router.viewController = self.viewController
        self.interaction.delegate = self
        self.interaction.initialConfiguration()
        
        if self.interaction.currentExpenseModelID == nil {
            self.delegate?.presentInterfaceForNewExpense()
            self.delegate?.pageSaveState(is: false)
        }
        else {
            self.delegate?.presentInterfaceForExistedExpense()
            
            if let price = self.interaction.currentExpenseModel?.price , price.doubleValue > 0.0 {
                self.delegate?.showPrice(PriceFormatter.roundDecimalForTwoPlacesToString(Double(price)))
            }
            self.creationDateChanged(self.interaction.currentExpenseModel?.creationDate)
            
            self.delegate?.showTitle(self.interaction.currentExpenseModel?.title)
        }
        
        self.delegate?.showExpenseCreatorButton(with: NameFormatter.userName(self.interaction.currentExpenseModel?.creator, replace: LocalisationManager.expenseSplittedWithMe()))
        self.delegate?.showPersons(inDeal: NameFormatter.allPeopleInDealNames(self.interaction.currentExpenseModel, self.interaction.currentEventModel))
    }
    
    // MARK: - UITextFieldDelegate methods
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.nextKeyboard()
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let activeTextField = self.delegate?.activeTextField() {
            self.delegate?.updateTextBorder(for: activeTextField)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let activeTextField = self.delegate?.activeTextField() {
            self.delegate?.updateTextBorder(for: activeTextField)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let activeTextField = self.delegate?.activeTextField(), activeTextField == ExpenseEditField.CreationDate {
            return false
        }
        
        return true
    }
    
    // MARK: - Public methods
    
    func viewWillAppear() {
        self.configureNotifications()
        
        self.interaction.trackOpenningScreen()
        
        self.delegate?.showExpenseCreatorButton(with: NameFormatter.userName(self.interaction.currentExpenseModel?.creator, replace: LocalisationManager.expenseSplittedWithMe()))
        self.delegate?.showPersons(inDeal: NameFormatter.allPeopleInDealNames(self.interaction.currentExpenseModel, self.interaction.currentEventModel, LocalisationManager.splitExpenseBetweenAll(), splitWithMe: LocalisationManager.expenseSplittedWithMe()))
    }
    
    func viewWillDisappear() {
        self.removeNotifications()
    }
    
    func doneKeyboard() {
        if let textField = self.delegate?.activeTextField() {
            self.delegate?.dismissKeyboard(for: textField)
        }
    }
    
    func nextKeyboard() {
        if let textField = self.delegate?.activeTextField() {
            if textField == ExpenseEditField.Title {
                self.delegate?.activateTextField(ExpenseEditField.CreationDate)
            }
            else if textField == ExpenseEditField.Price {
                self.delegate?.activateTextField(ExpenseEditField.Title)
            }
        }
    }
    
    func updateCreationDate(sender: UIDatePicker) {
        self.creationDateChanged(sender.date)
    }
    
    func expenseCreationDate() -> Date? {
        return self.interaction.currentExpenseModel?.creationDate
    }
    
    func changeExpenseCreator() {
        self.router.presentExpenseCreatorPage(eventID: self.interaction.currentEventModelID, expenseID: self.interaction.currentExpenseModel?.objectID, managedObjectContext: self.interaction.childrenManagedObjectContext)
    }
    
    func splitExpenseBetweenTeamMembersAction(sender: UIButton) {
        self.router.presentSplitExpenseBetweenPeopleScreen(eventID: self.interaction.currentEventModelID, expenseID: self.interaction.currentExpenseModel?.objectID, managedObjectContext: self.interaction.childrenManagedObjectContext)
    }
    
    func priceChangedAction(sender: UITextField) {
        if let text = sender.text?.trimmingCharacters(in: NSCharacterSet.whitespaces) {
            if text.characters.count > 0 {
                let error = self.isPriceValid(price: text)
                
                if error == ExpenseEditError.none {
                    if let price = PriceFormatter.roundStringDecimalForTwoPlacesToNumber(text) {
                        self.interaction.currentExpenseModel?.price = NSDecimalNumber(value: price.doubleValue)
                    }
                    
                    self.delegate?.priceIsValid()
                    self.delegate?.pageSaveState(is: true)
                }
                else {
                    switch error {
                    case ExpenseEditError.priceIsNegative:
                        self.delegate?.showPriceErrorMessage(LocalisationManager.priceIsNegative())
                    case ExpenseEditError.priceTooBig:
                        self.delegate?.showPriceErrorMessage(LocalisationManager.priceTooBig())
                    case ExpenseEditError.priceNotValid:
                         self.delegate?.showPriceErrorMessage(LocalisationManager.wrongPriceFormat())
                    default:
                        break
                    }
                    
                    self.delegate?.pageSaveState(is: false)
                }
            }
            else {
                self.delegate?.priceIsValid()
                self.delegate?.pageSaveState(is: false)
            }
        }
    }
    
    // MARK: - Private methods
    
    private func configureNotifications() {
        NotificationCenter.default.addObserver(self, selector:#selector(ExpenseEditPresenter.keyboardWasShown(notofication:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(ExpenseEditPresenter.keyboardWillBeHidden), name:NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    private func removeNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc private func keyboardWasShown(notofication: NSNotification) {
        if let info = notofication.userInfo {
            if let kbSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                self.delegate?.showKeyboard(with: kbSize.height)
            }
        }
    }
    
    @objc private func keyboardWillBeHidden() {
        self.delegate?.hideKeyboard()
    }
    
    private func isPriceValid(price: String) -> ExpenseEditError {
        let formatter = PriceFormatter.roundDecimalForTwoPlacesFormatter()
        let value = formatter.number(from: price)
        
        if let value = value {
            if value.doubleValue > maxPrice {
                return ExpenseEditError.priceTooBig
            }
            
            if value.doubleValue < 0.0 {
                return ExpenseEditError.priceIsNegative
            }
            
            return ExpenseEditError.none
        }
        
        return ExpenseEditError.priceNotValid
    }
    
    // MARK: - ExpenseEditInteractionDelegate methods
    
    func contentSaved() {
        self.router.dismissPage()
    }
    
    func titleValueEdited(_ sender: UITextField?) {
        self.interaction.currentExpenseModel?.title = sender?.text
    }
    
    func creationDateChanged(_ date: Date?) {
        self.interaction.currentExpenseModel?.creationDate = date
        
        if let creationDate = date {
            self.delegate?.presentCreationDateField(DateIsoFormatter.creationDateFormatter().string(from: creationDate))
        }
    }
}
