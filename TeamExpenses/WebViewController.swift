//
//  WebViewController.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 02/05/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController, WKNavigationDelegate {
    private var webView: WKWebView?
    
    var path = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let preferences = WKPreferences()
        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences
        
        self.webView = WKWebView(frame: self.view.frame, configuration: configuration)
        
        if let webView = self.webView {
            webView.navigationDelegate = self
            self.view.addSubview(webView)
        }
        
        if let path = Bundle.main.path(forResource: self.path, ofType:nil) {
            do {
                let license = try NSString(contentsOfFile: path, encoding: String.Encoding.utf8.rawValue)
                
                let result = "<html><body>\(license)</body></html>"
                _ = self.webView?.loadHTMLString(result, baseURL: nil)
            }
            catch {
                
            }
        }
    }
    
    private func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: (WKNavigationActionPolicy) -> Void) {
        if navigationAction.navigationType == WKNavigationType.linkActivated {
            if let url = navigationAction.request.url {
                let shared = UIApplication.shared
                
                if shared.canOpenURL(url) {
                    shared.openURL(url)
                }
            }
            
            decisionHandler(WKNavigationActionPolicy.cancel)
        }
        
        decisionHandler(WKNavigationActionPolicy.allow)
    }
}
