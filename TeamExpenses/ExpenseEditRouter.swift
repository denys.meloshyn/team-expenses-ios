//
//  ExpenseEditRouter.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 26/08/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit
import CoreData

class ExpenseEditRouter: NSObject {
    weak var viewController: UIViewController?
    
    private func presentContactScreen(eventID: NSManagedObjectID?, expenseID: NSManagedObjectID?, managedObjectContext: NSManagedObjectContext, page type: ContactPageType) {
        if let viewController = self.viewController?.storyboard?.instantiateViewController(withIdentifier: "ContactListViewController") as? ContactListViewController {
            viewController.currentEventID = eventID
            viewController.currentExpenseID = expenseID
            viewController.managedObjectContext = managedObjectContext
            viewController.contactPageType = type
            
            self.viewController?.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func dismissPage() {
        DispatchQueue.main.async(execute: { () -> Void in
            _ = self.viewController?.navigationController?.popViewController(animated: true)
        })
    }
    
    func presentExpenseCreatorPage(eventID: NSManagedObjectID?, expenseID: NSManagedObjectID?, managedObjectContext: NSManagedObjectContext) {
        self.presentContactScreen(eventID: eventID, expenseID: expenseID, managedObjectContext: managedObjectContext, page: ContactPageType.changeExpenseOwner)
    }
    
    func presentSplitExpenseBetweenPeopleScreen(eventID: NSManagedObjectID?, expenseID: NSManagedObjectID?, managedObjectContext: NSManagedObjectContext) {
        self.presentContactScreen(eventID: eventID, expenseID: expenseID, managedObjectContext: managedObjectContext, page: ContactPageType.splitExpenseBetweenPeople)
    }
}
