//
//  SplitExpensesViewController.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 19/01/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit
import Darwin

class SplitExpensesViewController: UIViewController {
    @IBOutlet private var splitExpensesView: SplitExpensesView?
    var currentEventID: NSManagedObjectID?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.splitExpensesView?.presenter.interaction.currentEventID = self.currentEventID
        self.splitExpensesView?.initialConfiguration()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AnalyticsManager.trackScreenVisible(name: kSplitExpenseScreen)
    }
}
