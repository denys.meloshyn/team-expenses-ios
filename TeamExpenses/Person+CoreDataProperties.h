//
//  Person+CoreDataProperties.h
//  
//
//  Created by Denys.Meloshyn on 08/09/16.
//
//

#import "Person.h"


NS_ASSUME_NONNULL_BEGIN

@interface Person (CoreDataProperties)

+ (NSFetchRequest<Person *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *isCurrentUser;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, retain) NSSet<Event *> *event;
@property (nullable, nonatomic, retain) NSSet<Expense *> *expenseCreator;
@property (nullable, nonatomic, retain) NSSet<Expense *> *expensePersonsInDeal;

@end

@interface Person (CoreDataGeneratedAccessors)

- (void)addEventObject:(Event *)value;
- (void)removeEventObject:(Event *)value;
- (void)addEvent:(NSSet<Event *> *)values;
- (void)removeEvent:(NSSet<Event *> *)values;

- (void)addExpenseCreatorObject:(Expense *)value;
- (void)removeExpenseCreatorObject:(Expense *)value;
- (void)addExpenseCreator:(NSSet<Expense *> *)values;
- (void)removeExpenseCreator:(NSSet<Expense *> *)values;

- (void)addExpensePersonsInDealObject:(Expense *)value;
- (void)removeExpensePersonsInDealObject:(Expense *)value;
- (void)addExpensePersonsInDeal:(NSSet<Expense *> *)values;
- (void)removeExpensePersonsInDeal:(NSSet<Expense *> *)values;

@end

NS_ASSUME_NONNULL_END
