//
//  EventView.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 23/08/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit

class EventView: NSObject, EventPresenterDelegate {
    @IBOutlet private var translucentView: UIView?
    @IBOutlet private var emptyExpanseView: UIView?
    @IBOutlet private var expenseTableView: UITableView?
    @IBOutlet private var teamMemberTableView: UITableView?
    @IBOutlet private var segmentedControl: UISegmentedControl?
    @IBOutlet private var visualEffectView: UIVisualEffectView?
    @IBOutlet private weak var viewController: UIViewController?
    
    let presenter = EventPresenter()
    
    func initialConfiguration() {
        self.presenter.delegate = self
        self.presenter.viewController = self.viewController
        self.presenter.initialConfiguration()
    }
    
    func layoutSubViews() {
        self.configureInsets(for: self.expenseTableView)
        self.configureInsets(for: self.teamMemberTableView)
    }
    
    // MARK: - EventPresenterDelegate methods
    
    func showPageTitle(_ title: String?) {
        self.viewController?.navigationItem.title = title
    }
    
    func applySwitcherBackgroundColor(_ color: UIColor) {
        self.translucentView?.backgroundColor = color
    }
    
    func applyPageBackgroundColor(_ color: UIColor) {
        self.viewController?.view.backgroundColor = color
    }
    
    func configureSwitcher(with selectedState: EventState) {
        self.segmentedControl?.addTarget(self.presenter, action: #selector(EventPresenter.segmentValueChangedAction(_:)), for: UIControlEvents.valueChanged)
        
        if selectedState == EventState.Expense {
            self.segmentedControl?.selectedSegmentIndex = 0
        }
        else {
            self.segmentedControl?.selectedSegmentIndex = 1
        }
    }
    
    func configureExpenseList(with cellName: String) {
        self.expenseTableView?.estimatedRowHeight = 44.0
        self.expenseTableView?.rowHeight = UITableViewAutomaticDimension
        
        // Add expense cell
        let nib = UINib(nibName: cellName, bundle:nil)
        self.expenseTableView?.register(nib, forCellReuseIdentifier: cellName)
        
        self.expenseTableView?.delegate = self.presenter
        self.expenseTableView?.dataSource = self.presenter
    }
    
    func configurePeopleList(with cellName: String) {
        self.teamMemberTableView?.estimatedRowHeight = 44.0
        self.teamMemberTableView?.rowHeight = UITableViewAutomaticDimension
        
        self.teamMemberTableView?.register(TeamTableViewCell.self, forCellReuseIdentifier: cellName)
        
        self.teamMemberTableView?.delegate = self.presenter
        self.teamMemberTableView?.dataSource = self.presenter
    }
    
    func state() -> EventState {
        if let index = self.segmentedControl?.selectedSegmentIndex {
            if index == 0 {
                return EventState.Expense
            }
        }
        
        return EventState.Person
    }
    
    func state(for table: UITableView?) -> EventState {
        if table === self.expenseTableView {
            return EventState.Expense
        }
        
        return EventState.Person
    }
    
    func createExpenseCell(name cellIdentifier:String, and model: Expense?) -> UITableViewCell? {
        let cell = self.expenseTableView?.dequeueReusableCell(withIdentifier: cellIdentifier) as? ExpenseTableViewCell
        cell?.configureWithModel(model)
        
        return cell
    }
    
    func createPersonCell(name cellIdentifier: String, and model: Person?) -> UITableViewCell? {
        let cell = self.teamMemberTableView?.dequeueReusableCell(withIdentifier: cellIdentifier) as? TeamTableViewCell
        cell?.configureWithModel(model)
        
        return cell
    }
    
    func showEmptyMessage() {
        self.emptyExpanseView?.isHidden = false
        
        self.expenseTableView?.isHidden = true
        self.teamMemberTableView?.isHidden = true
    }
    
    func showPersonList() {
        self.emptyExpanseView?.isHidden = true
        
        self.expenseTableView?.isHidden = true
        self.teamMemberTableView?.isHidden = false
    }
    
    func showExpenseList() {
        self.emptyExpanseView?.isHidden = true
        
        self.expenseTableView?.isHidden = false
        self.teamMemberTableView?.isHidden = true
    }
    
    func showSplitButtonAndNewExpense(_ splitIcon: String) {
        // Configure navigation bar items
        let resultBarButtonItem = UIBarButtonItem(image: UIImage(named: splitIcon), style: UIBarButtonItemStyle.plain, target: self.presenter.interaction, action: #selector(EventInteraction.showResults))
        let addExpenseBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add, target: self.presenter.interaction, action: #selector(EventInteraction.addNewExpense))
        
        var items = [UIBarButtonItem]()
        items.append(addExpenseBarButtonItem)
        items.append(resultBarButtonItem)
        
        self.viewController?.navigationItem.rightBarButtonItems = items
    }
    
    func showSplitButton(_ splitIcon: String, and createUser: String) {
        // Configure navigation bar items
        let resultBarButtonItem = UIBarButtonItem(image: UIImage(named: splitIcon), style: UIBarButtonItemStyle.plain, target: self.presenter.interaction, action: #selector(EventInteraction.showResults))
        let addExpenseBarButtonItem = UIBarButtonItem(image: UIImage(named: createUser), style: UIBarButtonItemStyle.plain, target: self.presenter.interaction, action: #selector(EventInteraction.addNewTeamMember))
        
        var items = [UIBarButtonItem]()
        items.append(addExpenseBarButtonItem)
        items.append(resultBarButtonItem)
        
        self.viewController?.navigationItem.rightBarButtonItems = items
    }
    
    func createRowAction(title: String?, handler: @escaping (UITableViewRowAction, IndexPath) -> Void) -> UITableViewRowAction {
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.destructive, title: title, handler: handler)
        
        return deleteAction
    }
    
    func deselectPersonCell(at indexPath: IndexPath) {
        self.teamMemberTableView?.deselectRow(at: indexPath, animated: false)
    }
    
    func deselectExpenseCell(at indexPath: IndexPath) {
        self.expenseTableView?.deselectRow(at: indexPath, animated: false)
    }
    
    func listDidChange(for state: EventState) {
        if state == EventState.Expense {
            self.tableViewDidChange(self.expenseTableView)
        }
        else {
            self.tableViewDidChange(self.teamMemberTableView)
        }
    }
    
    func listWillChange(for state: EventState) {
        if state == EventState.Expense {
            self.tableViewWillChange(self.expenseTableView)
        }
        else {
            self.tableViewWillChange(self.teamMemberTableView)
        }
    }
    
    func listChanged(at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?, for state: EventState) {
        if state == EventState.Expense {
            self.tableViewChanged(self.expenseTableView, at: indexPath, for: type, newIndexPath: newIndexPath)
        }
        else {
            self.tableViewChanged(self.teamMemberTableView, at: indexPath, for: type, newIndexPath: newIndexPath)
        }
    }
    
    func showDeleteExpenseDialog(with title: String, message: String, expense: Expense?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let deleteAction = UIAlertAction(title: LocalisationManager.delete(), style: UIAlertActionStyle.destructive, handler: { (alertAction: UIAlertAction) in
            self.presenter.interaction.deleteExpense(expense)
        })
        
        let cancelAction = UIAlertAction(title: LocalisationManager.cancel(), style: UIAlertActionStyle.cancel) { (_) in
            self.presenter.interaction.trackCancelingOfDeletingExpense()
            self.expenseTableView?.isEditing = false
        }
        
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        
        self.viewController?.present(alertController, animated: true, completion: nil)
        alertController.view.tintColor = Constatnts.defaultActiveColor()
    }
    
    func showDeletePersonDialog(with title: String, message: String, person: Person?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let deleteAction = UIAlertAction(title: LocalisationManager.delete(), style: UIAlertActionStyle.destructive, handler: { (alertAction: UIAlertAction) in
            self.presenter.interaction.deletePerson(person)
        })
        
        let cancelAction = UIAlertAction(title: LocalisationManager.cancel(), style: UIAlertActionStyle.cancel) { (_) in
            AnalyticsManager.track(kEventScreen, action: kCancelDeleteTeamMemberAction, label: "")
            self.teamMemberTableView?.isEditing = false
        }
        
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        
        self.viewController?.present(alertController, animated: true, completion: nil)
        alertController.view.tintColor = Constatnts.defaultActiveColor()
    }
    
    func refreshExpensesList() {
        self.expenseTableView?.reloadData()
    }
    
    // MARK: - Private methods
    
    private func configureInsets(for table: UITableView?) {
        if var contentInset = table?.contentInset {
            if let length = self.viewController?.topLayoutGuide.length {
                contentInset.top = length
                
                if let visualEffectView = self.visualEffectView {
                    contentInset.top += visualEffectView.frame.height
                }
                
                table?.contentInset = contentInset
                table?.contentOffset = CGPoint(x: 0.0, y: -contentInset.top)
                table?.scrollIndicatorInsets = contentInset
            }
        }
    }
    
    private func tableViewWillChange(_ tableView: UITableView?) {
        tableView?.beginUpdates()
    }
    
    private func tableViewDidChange(_ tableView: UITableView?) {
        tableView?.endUpdates()
    }
    
    private func tableViewChanged(_ tableView: UITableView?, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case NSFetchedResultsChangeType.insert:
            if let newIndexPath = newIndexPath {
                tableView?.insertRows(at: [newIndexPath], with: UITableViewRowAnimation.automatic)
            }
            
        case NSFetchedResultsChangeType.delete:
            if let indexPath = indexPath {
                tableView?.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            }
            
        case NSFetchedResultsChangeType.update:
            if let indexPath = indexPath {
                tableView?.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            }
            
        case NSFetchedResultsChangeType.move:
            if let indexPath = indexPath {
                tableView?.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            }
            
            if let newIndexPath = newIndexPath {
                tableView?.insertRows(at: [newIndexPath], with: UITableViewRowAnimation.automatic)
            }
        }
    }
}
