//
//  EventListInteraction.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 17/08/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit
import CoreData

protocol EventListInteractionDelegate: class {
    func didChangeContent()
    func willChangeContent()
    func showCreateNewEvenDialog()
    func changed(at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?)
}

class EventListInteraction: NSObject, NSFetchedResultsControllerDelegate {
    private let fetchedResultsController = ExpenseManager.sharedInstance.eventFetchController(ExpenseManager.sharedInstance.managedObjectContext)
    
    var delegate: EventListInteractionDelegate?
    
    func initialConfiguration() {
        do {
            try self.fetchedResultsController.performFetch()
            self.fetchedResultsController.delegate = self
        }
        catch {
            AnalyticsManager.trackError("EventListViewController error fetching events in viewDidLoad \(error)")
        }
    }
    
    // MARK: - NSFetchedResultsControllerDelegate methds
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.delegate?.willChangeContent()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.delegate?.didChangeContent()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        self.delegate?.changed(at: indexPath, for: type, newIndexPath: newIndexPath)
    }
    
    // MARK: - Public methods
    
    func eventModel(for indexPath: IndexPath) -> Event {
        let event = self.fetchedResultsController.object(at: indexPath)
        
        return event
    }
    
    func numberOfRowsInSection() -> Int {
        if let sections = self.fetchedResultsController.sections {
            if let sectionModel = sections.first {
                return sectionModel.numberOfObjects
            }
        }
        
        return 0
    }
    
    func createNewEvent() -> Event? {
        if let event = ExpenseManager.sharedInstance.createNewEvent(ExpenseManager.sharedInstance.managedObjectContext) {
            if let currentUser = ExpenseManager.sharedInstance.currentUser(ExpenseManager.sharedInstance.managedObjectContext) {
                // Configure inverse reference
                currentUser.addEventObject(event)
                
                event.creationDate = Date()
                event.addPersonsObject(currentUser)
            }
            
            return event
        }
        
        return nil
    }
    
    func saveEvent() {
        do {
            try ExpenseManager.sharedInstance.saveContext(ExpenseManager.sharedInstance.managedObjectContext)
        }
        catch {
            AnalyticsManager.trackError("EventListViewController error saving context \(error)")
        }
    }
    
    func delete(event model: Event) {
        ExpenseManager.sharedInstance.managedObjectContext.delete(model)
        
        self.saveEvent()
    }
    
    func cancelCreatingNewEvent() {
        AnalyticsManager.track(kEventListScreen, action: kCancelAction, label: kCreateEventAction)
    }
    
    func trackEditingOfEvent() {
        AnalyticsManager.track(kEventListScreen, action: kEditAction, label: "")
    }
    
    func trackCancelingEditingOfEvent() {
        AnalyticsManager.track(kEventListScreen, action: kCancelAction, label: kEditAction)
    }
    
    func trackDeleteEditingOfEvent() {
        AnalyticsManager.track(kEventListScreen, action: kDeleteAction, label: "")
    }
    
    func trackSwitchingToDetailPage() {
        AnalyticsManager.track(kEventListScreen, action: kOpenEventScreenAction, label: "")
    }
    
    @IBAction func addEvent() {
        AnalyticsManager.track(kEventListScreen, action: kCreateEventAction, label: "")
        self.delegate?.showCreateNewEvenDialog()
    }
}
