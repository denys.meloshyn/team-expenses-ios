//
//  WriteEmailTableViewCell.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 03/05/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit

class WriteEmailTableViewCell: UITableViewCell {
    @IBOutlet private var emailLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.emailLabel?.text = supportEmail
        
        let selectedView = UIView()
        selectedView.backgroundColor = Constatnts.defaultActiveColor()
        self.selectedBackgroundView = selectedView
    }
}
