//
//  Constants.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 15/12/15.
//  Copyright © 2015 Denys Meloshyn. All rights reserved.
//

import Foundation

let kUser = "user"
let kResult = "result"
let kUserID = "userID"
let kTimeStamp = "timeStamp"
let kUserToken = "userToken"
let kTokenExpired = "token_expired"
let kTokenNotValid = "token_not_valid"

let supportEmail = "pitch-in@mail.com"
let defaultBackgroundHEXColor = "#F7F7F7"
let defaultActiveColorHEXColor = "#ec407a"
let defaultApperanceColorHEXColor = "#4db6ac"

let kTeamMemberCell = "TeamTableViewCell"
let kExpenseTableViewCell = "ExpenseTableViewCell"

let maxPrice = 10000000.0

enum ExpenseEditError {
    case priceNotValid
    case priceIsNegative
    case priceTooBig
    case none
}

class Constatnts: NSObject
{
    class var sharedInstance: Constatnts
    {
        struct Static {
            static let instance: Constatnts = Constatnts()
        }
        
        return Static.instance
    }
    
    class func defaultApperanceColor() -> UIColor {
        return UIColor(rgba: defaultApperanceColorHEXColor)
    }
    
    class func defaultPriceColor() -> UIColor {
        return UIColor(rgba: "#8E8E93")
    }
    
    class func defaultErrorColor() -> UIColor {
        return UIColor(rgba: "#E53935")
    }
    
    class func defaultActiveColor() -> UIColor {
        return UIColor(rgba: defaultActiveColorHEXColor)
    }
    
    class func defaultBackgroundColor() -> UIColor {
        return UIColor(rgba: defaultBackgroundHEXColor)
    }
    
    class func defaultTextBorderColor() -> UIColor {
        return UIColor(rgba: "#E0E0E0")
    }
    
    class func defaultDescriptionTextColor() -> UIColor {
        return UIColor.darkGray
    }
}
