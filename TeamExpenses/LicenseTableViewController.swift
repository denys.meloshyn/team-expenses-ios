//
//  LicenseTableViewController.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 02/05/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit

let kPath = "path"
let kTitle = "title"

class LicenseTableViewController: UITableViewController {
    var items = [Dictionary<String, String>]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundColor = Constatnts.defaultBackgroundColor()
        
        if let path = Bundle.main.path(forResource: "Licenses", ofType: "plist") {
            if let items = NSArray(contentsOfFile: path) as? [Dictionary<String, String>] {
                self.items = items
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AnalyticsManager.trackScreenVisible(name: kLicenseScreen)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LicenseSegue" {
            if let viewController = segue.destination as? WebViewController {
                if let indexPath = self.tableView.indexPathForSelectedRow {
                    let dict = self.items[(indexPath as NSIndexPath).row]
                    if let path = dict[kPath] {
                        viewController.path = path
                    }
                    
                    viewController.title = dict[kTitle]
                    self.tableView.deselectRow(at: indexPath, animated: false)
                }
            }
        }
    }
    
    // MARK: - UITableViewDataSource methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LicenseCell", for: indexPath)
        
        let selectedView = UIView()
        selectedView.backgroundColor = Constatnts.defaultActiveColor()
        cell.selectedBackgroundView = selectedView
        
        let dict = self.items[(indexPath as NSIndexPath).row]
        cell.textLabel?.text = dict[kTitle]

        return cell
    }
    
    // MARK: - UITableViewDelegate methods
    
}
