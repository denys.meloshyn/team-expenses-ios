//
//  Person+CoreDataClass.h
//  
//
//  Created by Denys.Meloshyn on 08/09/16.
//
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

@class Event, Expense;

NS_ASSUME_NONNULL_BEGIN

@interface Person : BaseModel

- (NSString *)userName;
- (BOOL)isUserCurrent;

@end

NS_ASSUME_NONNULL_END

#import "Person+CoreDataProperties.h"
