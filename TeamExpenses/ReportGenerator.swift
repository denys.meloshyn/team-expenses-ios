//
//  ReportGenerator.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 08/09/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit
import XCGLogger

class ReportGenerator: NSObject {
    class var sharedInstance: ReportGenerator {
        struct Static {
            static let instance: ReportGenerator = ReportGenerator()
        }
        
        return Static.instance
    }
    
    private func generateSummaryReport(_ fetchedResultsControllerTeamMember: NSFetchedResultsController<BaseModel>, fetchedResultsControllerExpenses: NSFetchedResultsController<Expense>, splitItems: [SplitModel]?, event: Event?) -> String {
        var html = "<html>\n"
        html += "\t<head>\n"
        html += "\t\t<meta charset=\"UTF-8\">\n"
        html += "\t\t<style>\n"
        html += "\t\t\ttable { border-collapse: collapse; }\n"
        html += "\t\t\tth { padding: 8px; border: 1px solid black;}\n"
        html += "\t\t\ttd { padding: 8px; border: 1px solid #ddd; }\n"
        html += "\t\t\tcaption { background-color: \(defaultApperanceColorHEXColor); color: white; padding: 8px; }\n"
        html += "\t\t</style>\n"
        html += "\t</head>\n\n"
        
        html += "\t<body>\n"
        html += "\t\t<body bgcolor=\"\(defaultBackgroundHEXColor)\">\n"
        
        if let title = event?.title {
            html += "\t\t<table width=\"100%%\" border=\"1\" style=\"border-collapse: collapse;\">\n"
            html += "\t\t\t<tr bgcolor=\"\(defaultApperanceColorHEXColor)\" align=\"center\">\n"
            html += "\t\t\t\t<td><font color=white>\(title)</font></td>\n"
            html += "\t\t\t</tr>\n"
            html += "\t\t</table>\n\n"
        }
        
        html += "\t\t<table width=\"100%%\" border=\"1\" style=\"border-collapse: collapse;\">\n"
        html += "\t\t\t<caption style=\"text-align:left\">\(LocalisationManager.summary())</caption>\n"
        html += "\t\t\t<tr>\n"
        html += "\t\t\t\t<th></th>\n"
        
        if let sections = fetchedResultsControllerTeamMember.sections {
            for sectionIndex in 0..<sections.count {
                let sectionInfo = sections[sectionIndex]
                
                for row in 0..<sectionInfo.numberOfObjects {
                    let indexPath = IndexPath(row: row, section: sectionIndex)
                    
                    if let person = fetchedResultsControllerTeamMember.object(at: indexPath) as? Person {
                        html += "\t\t\t\t<th>\(person.userName())</th>\n"
                    }
                }
            }
        }
        html += "\t\t\t</tr>\n"
        
        if let sections = fetchedResultsControllerExpenses.sections {
            for sectionIndex in 0..<sections.count {
                let sectionInfo = sections[sectionIndex]
                
                for row in 0..<sectionInfo.numberOfObjects {
                    let indexPath = IndexPath(row: row, section: sectionIndex)
                    let expense = fetchedResultsControllerExpenses.object(at: indexPath)
                    
                    var title = ""
                    if let expenseTitle = expense.title {
                        title = expenseTitle
                    }
                    
                    html += "\t\t\t<tr align=\"center\">\n"
                    if let date = expense.creationDate {
                        if title.characters.count == 0 {
                            title = DateIsoFormatter.creationDateFormatter().string(from: date)
                        }
                        else {
                            title += "<br>(\(DateIsoFormatter.creationDateFormatter().string(from: date)))</br>"
                        }
                    }
                    html += "\t\t\t\t<td align=\"right\">\(title)</td>\n"
                    
                    if let sectionsPerson = fetchedResultsControllerTeamMember.sections {
                        for sectionIndexPerson in 0..<sectionsPerson.count {
                            let sectionInfoPerson = sectionsPerson[sectionIndexPerson]
                            
                            for rowPerson in 0..<sectionInfoPerson.numberOfObjects {
                                let indexPath = IndexPath(row: rowPerson, section: sectionIndexPerson)
                                
                                if let person = fetchedResultsControllerTeamMember.object(at: indexPath) as? Person {
                                    if let creator = expense.creator {
                                        if creator == person {
                                            if let price = expense.price {
                                                html += "\t\t\t\t<td>"
                                                
                                                if BillManager.sharedInstance.isExpenPersonsContainsPerson(expense: expense, person: person) {
                                                    if let price = PriceFormatter.roundDecimalForTwoPlacesToString(Double(price)) {
                                                        html += price
                                                    }
                                                }
                                                else {
                                                    if let price = PriceFormatter.roundDecimalForTwoPlacesToString(Double(price)) {
                                                        html += "\(price) (✕)"
                                                    }
                                                }
                                                
                                                html += "</td>\n"
                                            }
                                        }
                                        else {
                                            if BillManager.sharedInstance.isExpenPersonsContainsPerson(expense: expense, person: person) {
                                                html += "\t\t\t\t<td>"
                                                
                                                if let price = PriceFormatter.roundDecimalForTwoPlacesToString(0.0) {
                                                    html += price
                                                }
                                                
                                                html += "</td>\n"
                                            }
                                            else {
                                                html += "\t\t\t\t<td bgcolor=\"\(defaultBackgroundHEXColor)\">✕</td>\n"
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        if let items = splitItems {
            html += "\t\t\t<tr align=\"center\">\n"
            
            if let model = items.first , model.type == SplitModelType.summary {
                html += "\t\t\t\t<td></td>\n"
                
                if let numberOfTeamMemberObjects = fetchedResultsControllerTeamMember.sections?.first?.numberOfObjects {
                    for i in 0..<numberOfTeamMemberObjects {
                        let indexPath = IndexPath(row: i, section: 0)
                        
                        html += "\t\t\t\t<td><b>"
                        if let person = fetchedResultsControllerTeamMember.object(at: indexPath) as? Person {
                            for splitModel in items {
                                if let personID = splitModel.personID , personID == person.objectID {
                                    if let price = PriceFormatter.roundDecimalForTwoPlacesToString(splitModel.value) {
                                        html += price
                                    }
                                }
                            }
                        }
                        html += "</b></td>\n"
                    }
                }
            }
            
            html += "\t\t\t</tr>\n"
        }
        
        if let numberOfTeamMemberObjects = fetchedResultsControllerTeamMember.sections?.first?.numberOfObjects {
            html += "\t\t\t<tr>\n"
            html += "\t\t\t\t<td colspan=\"\(numberOfTeamMemberObjects + 1)\" bgcolor=\"\(defaultBackgroundHEXColor)\">✕ - \(LocalisationManager.splitCrossCharacterExplanation())</td>\n"
            html += "\t\t\t</tr>\n"
        }
        
        html += "\t\t</table>\n"
        
        return html
    }
    
    private func generateSplitReport(_ items: [SplitModel]) -> String {
        var html = "\n\t\t<br>\n"
        html += "\t\t<table border=\"1\" style=\"border-collapse: collapse;\">\n"
        html += "\t\t\t<caption style=\"text-align:left\">\(LocalisationManager.split())</caption>\n"
        for model in items {
            html += "\t\t\t<tr>\n"
            html += "\t\t\t\t<td>\n"
            
            if let underPaidPersonID = model.underPaidPersonID {
                if let underPaidModel = ExpenseManager.sharedInstance.managedObjectContext.object(with: underPaidPersonID) as? Person {
                    html += "\t\t\t\t\t\(underPaidModel.userName())"
                }
            }
            
            html += " » "
            
            if let overPaidPersonID = model.overPaidPersonID {
                if let overPaidModel = ExpenseManager.sharedInstance.managedObjectContext.object(with: overPaidPersonID) as? Person {
                    html += "\(NameFormatter.userName(overPaidModel, replace: LocalisationManager.splitPersonOwnToMe()))\n"
                }
            }

            html += "\t\t\t\t</td>\n"
            
            html += "\t\t\t\t<td>\n"
            if let price = PriceFormatter.roundDecimalForTwoPlacesToString(model.value) {
                html += "\t\t\t\t\t\(price)\n"
            }
            html += "\t\t\t\t</td>\n"
            html += "\t\t\t</tr>\n"
        }
        html += "\t\t</table>\n"
        
        return html
    }
    
    private func generateShouldBeReturnedReport(_ items: [SplitModel], fetchedResultsControllerTeamMember: NSFetchedResultsController<BaseModel>) -> String {
        var html = "\n\t\t<br>\n"
        html += "\t\t<table width=100%% border=\"1\" style=\"border-collapse: collapse;\">\n"
        html += "\t\t\t<caption style=\"text-align:left\">\(LocalisationManager.shouldBeReturnedBackPaid())</caption>\n"
        
        html += "\t\t\t<tr align=\"center\">\n"
        if let numberOfObjects = fetchedResultsControllerTeamMember.sections?.first?.numberOfObjects {
            for i in 0..<numberOfObjects {
                let indexPath = IndexPath(row: i, section: 0)
                
                if let teamMember = fetchedResultsControllerTeamMember.object(at: indexPath) as? Person {
                    html += "\t\t\t\t<th>\(teamMember.userName())</th>\n"
                }
            }
        }
        html += "\t\t\t</tr>\n"
        
        html += "\t\t\t<tr align=\"center\">\n"
        
        let numberOfObjects = fetchedResultsControllerTeamMember.sections?.first?.numberOfObjects ?? 0
        for i in 0..<numberOfObjects {
            html += "\t\t\t\t<td>"
            let indexPath = IndexPath(row: i, section: 0)
            
            for model in items {
                let teamMember = fetchedResultsControllerTeamMember.object(at: indexPath) as? Person
                if let person = teamMember, person.objectID == model.personID {
                    if let splitValue = PriceFormatter.roundDecimalForTwoPlacesToString(model.differenceFromAvarage) {
                        if model.differenceFromAvarage > 0.0 {
                            html += "<font color=#43A047>+\(splitValue)</font>"
                        }
                        else {
                            html += "<font color=#E53935>\(splitValue)</font>"
                        }
                    }
                }
            }
            
            html += "</td>\n"
        }
        
        html += "\t\t\t</tr>\n"
        html += "\t\t</table>\n"
        
        return html
    }
    
    private func generatePaidInSummaryReport(_ items: [SplitModel], fetchedResultsControllerTeamMember: NSFetchedResultsController<BaseModel>) -> String {
        var html = "\n\t\t<br>\n"
        html += "\t\t<table width=100%% border=\"1\" style=\"border-collapse: collapse;\">\n"
        html += "\t\t\t<caption style=\"text-align:left\">\(LocalisationManager.paidInSummary())</caption>\n"
        
        html += "\t\t\t<tr align=\"center\">\n"
        if let numberOfObjects = fetchedResultsControllerTeamMember.sections?.first?.numberOfObjects {
            for i in 0..<numberOfObjects {
                let indexPath = IndexPath(row: i, section: 0)
                
                let teamMember = fetchedResultsControllerTeamMember.object(at: indexPath) as? Person
                if let person = teamMember {
                    html += "\t\t\t\t<th>\(person.userName())</th>\n"
                }
            }
        }
        html += "\t\t\t</tr>\n"
        html += "\t\t\t<tr align=\"center\">\n"
        
        let numberOfObjects = fetchedResultsControllerTeamMember.sections?.first?.numberOfObjects ?? 0
        for i in 0..<numberOfObjects {
            html += "\t\t\t\t<td>"
            let indexPath = IndexPath(row: i, section: 0)
            
            for model in items {
                let teamMember = fetchedResultsControllerTeamMember.object(at: indexPath) as? Person
                if let person = teamMember, person.objectID == model.personID {
                    if let splitValue = PriceFormatter.roundDecimalForTwoPlacesToString(model.value - model.differenceFromAvarage) {
                        html += splitValue
                    }
                }
            }
            html += "</td>\n"
        }
        
        html += "\t\t\t</tr>\n"
        html += "\t\t</table>\n"
        
        return html
    }
    
    // MARK: - Public methods
    
    func generateReport(_ items: [[SplitModel]], currentEventID: NSManagedObjectID?) -> String {
        var event: Event?
        if let currentEventID = currentEventID {
            event = ExpenseManager.sharedInstance.managedObjectContext.object(with: currentEventID) as? Event
        }
        
        let fetchedResultsControllerTeamMember = ExpenseManager.sharedInstance.eventPersonFetchController(currentEventID, managedObjectContext: ExpenseManager.sharedInstance.managedObjectContext)
        let fetchedResultsControllerExpenses = ExpenseManager.sharedInstance.expensesFetchControllerForEvent(currentEventID, managedObjectContext: ExpenseManager.sharedInstance.managedObjectContext)
        
        do {
            try fetchedResultsControllerTeamMember.performFetch()
            try fetchedResultsControllerExpenses.performFetch()
        }
        catch let error as NSError {
            XCGLogger.default.error("SplitExpensesViewController: couldn't fetch team mebers\n \(error)")
        }
        
        var html = self.generateSummaryReport(fetchedResultsControllerTeamMember, fetchedResultsControllerExpenses: fetchedResultsControllerExpenses, splitItems: items.first, event: event)
        
        if let splitItems = items.last {
            if let model = splitItems.first , model.type == SplitModelType.split {
                html += self.generateSplitReport(splitItems)
            }
        }
        
        if let splitItems = items.first {
            if let model = splitItems.first , model.type == SplitModelType.summary {
                html += self.generateShouldBeReturnedReport(splitItems, fetchedResultsControllerTeamMember: fetchedResultsControllerTeamMember)
                html += self.generatePaidInSummaryReport(splitItems, fetchedResultsControllerTeamMember: fetchedResultsControllerTeamMember)
            }
        }
        
        html += "\n\t\t<br>\n"
        html += "\t\t<table width=\"100%%\" border=\"1\" style=\"border-collapse: collapse;\">\n"
        html += "\t\t\t<tr bgcolor=\"\(defaultApperanceColorHEXColor)\">\n"
        html += "\t\t\t\t<td>\n"
        html += "\t\t\t\t\t<a href=\"https://www.facebook.com/apppitchin/\"><font color=white>Pitch In!</font></a>\n"
        html += "\t\t\t\t\t<br>\n"
        html += "\t\t\t\t\t<font color=white>\(DateIsoFormatter.creationDateFormatter().string(from: Date()))</font>\n"
        html += "\t\t\t\t</td>\n"
        html += "\t\t\t</tr>\n"
        html += "\t\t</table>\n"
        
        html += "\t</body>\n"
        html += "</html>\n"
        
//        DDLogWrapper.logInfo("Split report \(html)")
        
        return html
    }
}
