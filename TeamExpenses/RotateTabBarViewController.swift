//
//  RotateTabBarViewController.swift
//  Muzarium
//
//  Created by Ned on 27.06.15.
//  Copyright (c) 2015 Denys Meloshyn. All rights reserved.
//

import UIKit

class RotateTabBarViewController: UITabBarController {
    override var shouldAutorotate: Bool {
        get {
            if let selectedVC = self.selectedViewController {
                return selectedVC.shouldAutorotate
            }
            
            return false
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get {
            if let selectedVC = self.selectedViewController {
                return selectedVC.supportedInterfaceOrientations
            }
            
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        get {
            if let selectedVC = self.selectedViewController {
                return selectedVC.preferredInterfaceOrientationForPresentation
            }
            
            return UIInterfaceOrientation.portrait
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return UIStatusBarStyle.lightContent
        }
    }
}
