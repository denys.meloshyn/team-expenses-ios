//
//  EventListViewController.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 27/10/15.
//  Copyright © 2015 Denys Meloshyn. All rights reserved.
//

import UIKit

import CoreData
import Crashlytics

class EventListViewController: UIViewController {
    @IBOutlet private var eventListView: EventListView?
    
    private let fetchedResultsController = ExpenseManager.sharedInstance.eventFetchController(ExpenseManager.sharedInstance.managedObjectContext)
    
    // MARK: - Life cycle methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.title = self.eventListView?.presenter.pageTitle()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.eventListView?.initialConfiguration()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AnalyticsManager.trackScreenVisible(name: kEventListScreen)
    }
}
