//
//  DateIsoFormatter.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 17/12/15.
//  Copyright © 2015 Denys Meloshyn. All rights reserved.
//

import Foundation

class DateIsoFormatter: NSObject {
    class func dateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        let enUSPosixLocale = Locale(identifier: "en_US_POSIX")
        dateFormatter.locale = enUSPosixLocale
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS"
        
        return dateFormatter
    }
    
    class func creationDateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = DateFormatter.Style.medium
        dateFormatter.timeStyle = DateFormatter.Style.short
        
        return dateFormatter
    }
}
