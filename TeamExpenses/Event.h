//
//  Event+CoreDataClass.h
//  
//
//  Created by Denys.Meloshyn on 08/09/16.
//
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

@class Expense, Person;

NS_ASSUME_NONNULL_BEGIN

@interface Event : BaseModel

@end

NS_ASSUME_NONNULL_END

#import "Event+CoreDataProperties.h"
