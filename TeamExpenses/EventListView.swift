//
//  EventListView.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 17/08/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit
import CoreData

class EventListView: NSObject, UITableViewDataSource, UITableViewDelegate, EventListPresenterDelegate {
    @IBOutlet private var tableView: UITableView?
    @IBOutlet private weak var viewController: UIViewController?
    
    private let cellIdentifier = "EventTableViewCell"
    
    let presenter = EventListPresenter()
    
    func initialConfiguration() {
        self.presenter.delegate = self
        self.presenter.viewController = self.viewController
        self.presenter.initialConfiguration()
        
        let addBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.add, target: self.presenter.interaction, action: #selector(EventListInteraction.addEvent))
        self.viewController?.navigationItem.rightBarButtonItem = addBarButtonItem
        
        self.tableView?.estimatedRowHeight = 44.0
        self.tableView?.rowHeight = UITableViewAutomaticDimension
        self.tableView?.backgroundColor = Constatnts.defaultBackgroundColor()
        
        // Add expense cell
        let nib = UINib(nibName: cellIdentifier, bundle:nil)
        self.tableView?.register(nib, forCellReuseIdentifier: cellIdentifier)
    }
    
    // MARK: - UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRowsInSection()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) {
            let event = self.presenter.eventModel(forIndexPath: indexPath)
            self.configureCell(cell, model: event)
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter.interaction.trackSwitchingToDetailPage()
        
        self.tableView?.deselectRow(at: indexPath, animated: false)
        self.presenter.showDetailPage(for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let editAction = UITableViewRowAction(style: UITableViewRowActionStyle.normal, title: LocalisationManager.edit()) { (action: UITableViewRowAction, indexPath: IndexPath) in
            self.presenter.showEditEvenDialog(indexPath)
        }
        editAction.backgroundColor = Constatnts.defaultActiveColor();
        
        let deleteAction = UITableViewRowAction(style: UITableViewRowActionStyle.destructive, title: LocalisationManager.delete()) { (action: UITableViewRowAction, indexPath: IndexPath) in
            self.presenter.showDeleteEvenDialog(indexPath)
        }
        
        return [editAction, deleteAction]
    }
    
    // MARK: - EventListPresenterDelegate methods
    
    func showCreateNewEvenDialog(withTitle title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let createAction = UIAlertAction(title: self.presenter.createTitleForNewEvent(), style: .default) { (_) in
            if let titleTextField = alertController.textFields?.first as UITextField? {
                titleTextField.text = titleTextField.text?.trimmingCharacters(in: CharacterSet.whitespaces)
                
                self.tableView?.isEditing = false
                if !self.presenter.isEventTitleValid(titleTextField.text) {
                    return
                }
                
                if let newEvent = self.presenter.interaction.createNewEvent() {
                    newEvent.title = titleTextField.text
                    
                    self.presenter.interaction.saveEvent()
                    self.presenter.showDetailPage(event: newEvent)
                }
            }
        }
        createAction.isEnabled = false
        
        let cancelAction = UIAlertAction(title: self.presenter.cancelTitleForCreatingNewEvent(), style: UIAlertActionStyle.cancel) { (_) in
            self.presenter.interaction.cancelCreatingNewEvent()
        }
        
        alertController.addTextField { (textField) in
            textField.placeholder = self.presenter.placeholderForCreatingNewEvent()
            textField.autocapitalizationType = UITextAutocapitalizationType.sentences
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textField, queue: OperationQueue.main) { (notification) in
                createAction.isEnabled = self.presenter.isEventTitleValid(textField.text)
            }
        }
        
        alertController.addAction(createAction)
        alertController.addAction(cancelAction)
        
        self.viewController?.present(alertController, animated: true, completion: nil)
        alertController.view.tintColor = Constatnts.defaultActiveColor()
    }
    
    func showEditEvenDialog(withTitle title: String, message: String, cancelTitle: String, indexPath: IndexPath) {
        self.presenter.interaction.trackEditingOfEvent()
        
        let event = self.presenter.interaction.eventModel(for: indexPath)
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let updateAction = UIAlertAction(title: LocalisationManager.update(), style: UIAlertActionStyle.default) { (_) in
            if let titleTextField = alertController.textFields?.first as UITextField? {
                titleTextField.text = self.presenter.removeWhiteSpacesIn(eventTitle: titleTextField.text)
                
                self.tableView?.isEditing = false
                
                if !self.presenter.isEventTitleValid(titleTextField.text) {
                    return
                }
                
                event.title = titleTextField.text
                
                self.presenter.interaction.saveEvent()
            }
        }
        
        let cancelAction = UIAlertAction(title: cancelTitle, style: UIAlertActionStyle.cancel) { (_) in
            self.tableView?.isEditing = false
            self.presenter.interaction.trackCancelingEditingOfEvent()
        }
        
        alertController.addTextField { (textField) in
            textField.placeholder = self.presenter.placeholderForCreatingNewEvent()
            textField.autocapitalizationType = UITextAutocapitalizationType.sentences
            textField.text = event.title
            
            NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: textField, queue: OperationQueue.main) { (notification) in
                updateAction.isEnabled = self.presenter.isEventTitleValid(textField.text)
            }
        }
        
        alertController.addAction(updateAction)
        alertController.addAction(cancelAction)
        
        self.viewController?.present(alertController, animated: true, completion: nil)
        alertController.view.tintColor = Constatnts.defaultActiveColor()
    }
    
    func showDeleteEvenDialog(withTitle title: String, message: String, cancelTitle: String, deleteTitle: String, indexPath: IndexPath) {
        let event = self.presenter.interaction.eventModel(for: indexPath)
        
        let alertController = UIAlertController(title: LocalisationManager.event(), message: LocalisationManager.deleteEventWarning(), preferredStyle: UIAlertControllerStyle.alert)
        
        let deleteAction = UIAlertAction(title: deleteTitle, style: UIAlertActionStyle.destructive, handler: { (alertAction: UIAlertAction) in
            self.presenter.interaction.trackDeleteEditingOfEvent()
            self.presenter.interaction.delete(event: event)
        })
        
        let cancelAction = UIAlertAction(title: cancelTitle, style: UIAlertActionStyle.cancel) { (_) in
            self.tableView?.isEditing = false
            self.presenter.interaction.trackCancelingEditingOfEvent()
        }
        
        alertController.addAction(deleteAction)
        alertController.addAction(cancelAction)
        
        self.viewController?.present(alertController, animated: true, completion: nil)
        alertController.view.tintColor = Constatnts.defaultActiveColor()
    }
    
    func listOfEventsWillChange() {
        self.tableView?.beginUpdates()
    }
    
    func listOfEventsDidChange() {
        self.tableView?.endUpdates()
    }
    
    func listOfEventsChanged(at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case NSFetchedResultsChangeType.insert:
            if let newIndexPath = newIndexPath {
                self.tableView?.insertRows(at: [newIndexPath], with: UITableViewRowAnimation.automatic)
            }
            
        case NSFetchedResultsChangeType.delete:
            if let indexPath = indexPath {
                self.tableView?.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            }
            
        case NSFetchedResultsChangeType.update:
            if let indexPath = indexPath {
                self.tableView?.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
            }
            
        default:
            break
        }
    }
    
    // MARK: - Private method
    
    private func configureCell(_ cell: UITableViewCell?, model: Event?) {
        let selectedView = UIView()
        selectedView.backgroundColor = Constatnts.defaultActiveColor()
        cell?.selectedBackgroundView = selectedView
        cell?.backgroundColor = UIColor.clear
        
        cell?.textLabel?.highlightedTextColor = UIColor.white
        cell?.detailTextLabel?.highlightedTextColor = UIColor.white
        
        cell?.detailTextLabel?.text = ""
        
        cell?.textLabel?.text = model?.title
        if let date = model?.creationDate {
            cell?.detailTextLabel?.text = DateIsoFormatter.creationDateFormatter().string(from: date)
        }
    }
}
