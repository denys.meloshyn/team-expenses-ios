//
//  LocalisationManager.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 27/04/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit
import Rswift

struct localisationManager {
    static let event = NSLocalizedString("Event", comment: "Title in the alert view")
    
    static func priceTooBig() -> String {
        if let maxPrice = PriceFormatter.roundDecimalForTwoPlacesToString(maxPrice) {
            let str = NSLocalizedString("Price is too big (maximum value is: %@)", comment: "Price is too big")
            
            return String(format: str, maxPrice)
        }
        
        return ""
    }
}

@objc class LocalisationManager: NSObject {
    class func event() -> String {
        return localisationManager.event
    }
    
    class func events() -> String {
        return NSLocalizedString("Events", comment: "Title in the alert view")
    }
    
    class func expense() -> String {
        return NSLocalizedString("Expense", comment: "Title in the alert view")
    }
    
    class func teamMember() -> String {
        return NSLocalizedString("Team member", comment: "Title in the alert view")
    }
    
    class func createNewEvent() -> String {
        return NSLocalizedString("Create new event", comment: "Message in the alert view")
    }
    
    class func create() -> String {
        return NSLocalizedString("Create", comment: "Generic Create title")
    }
    
    class func cancel() -> String {
        return NSLocalizedString("Cancel", comment: "Generic Cancel")
    }
    
    class func title() -> String {
        return NSLocalizedString("Title", comment: "Placeholder in alert view")
    }
    
    class func splitExpense() -> String {
        return NSLocalizedString("Split expense", comment: "Navigation title for split expense screen")
    }
    
    class func summary() -> String {
        return NSLocalizedString("Summary", comment: "Title in the table header view")
    }
    
    class func split() -> String {
        return NSLocalizedString("Split", comment: "Title in the table header view")
    }
    
    class func all() -> String {
        return NSLocalizedString("All", comment: "Title when expense is splitted between all team members")
    }
    
    class func splitExpenseBetweenAll() -> String {
        return NSLocalizedString("split_expense_between_all", comment: "Title when expense is splitted between all team members")
    }
    
    class func next() -> String {
        return NSLocalizedString("Next", comment: "Next title in the keyboard")
    }
    
    class func update() -> String {
        return NSLocalizedString("Update", comment: "Update title")
    }
    
    @objc class func me() -> String {
        return NSLocalizedString("Me", comment: "Title for current user")
    }
    
    class func wrongPriceFormat() -> String {
        return NSLocalizedString("Wrong number format", comment: "Error description when user enter wrong price")
    }
    
    class func edit() -> String {
        return NSLocalizedString("Edit", comment: "Edit generic title")
    }
    
    class func delete() -> String {
        return NSLocalizedString("Delete", comment: "Delete generic title")
    }
    
    class func deleteEventWarning() -> String {
        return NSLocalizedString("Are you sure you want to delete this event?", comment: "Warning message when user tries to delete the event")
    }
    
    class func deleteExpenseWarning() -> String {
        return NSLocalizedString("Are you sure you want to delete this expense?", comment: "Warning message when user tries to delete the expense")
    }
    
    class func deleteTeamMemberWarning() -> String {
        return NSLocalizedString("Are you sure you want to delete this team member?\nAll his expenses will be removed too.", comment: "Warning message when user tries to delete the team member")
    }
    
    class func firstOrLastNameIsEmpty() -> String {
        return NSLocalizedString("First name or last name shouldn't be empty.", comment: "Warning message when user tries to delete the team member")
    }
    
    class func emailSubject () -> String {
        return NSLocalizedString("Suggestions or questions about iOS application", comment: "Email subject")
    }
    
    class func about() -> String {
        return NSLocalizedString("About", comment: "Title in the About page")
    }
    
    class func paidByMe() -> String {
        return NSLocalizedString("Myself", comment: "Paid by me title")
    }
    
    class func splitBetweenAll() -> String {
        return NSLocalizedString("Everyone team member", comment: "Split between everyone")
    }
    
    class func payForAll() -> String {
        return NSLocalizedString("Everyone", comment: "Pay for everyone title")
    }
    
    class func expenseSplittedWithMe() -> String {
        return NSLocalizedString("expense_splitted_with_me", comment: "Expense split with me")
    }
    
    class func payedByOtherButSplitWithMe() -> String {
        return NSLocalizedString("payed_by_other_but_split_with_me", comment: "Expense split with me but payed by other")
    }
    
    class func splitCrossCharacterExplanation() -> String {
        return NSLocalizedString("User doesn't take part in splitting the expense", comment: "Split Cross Character Explanation")
    }
    
    class func splitPersonOwnToMe() -> String {
        return NSLocalizedString("split_person_own_to_me", comment: "Person own to me")
    }
    
    class func shouldBeReturnedBackPaid() -> String {
        return NSLocalizedString("Should be returned back / paid", comment: "Should be returned back / paid")
    }
    
    class func paidInSummary() -> String {
        return NSLocalizedString("Paid in summary", comment: "Paid in summary")
    }
    
    class func creationDate() -> String {
        return NSLocalizedString("Creation date", comment: "Creation date")
    }
    
    class func expenseDetails() -> String {
        return NSLocalizedString("Expense details:", comment: "Expense details:")
    }
    
    class func paid() -> String {
        return NSLocalizedString("Paid:", comment: "Paid:")
    }
    
    class func inTotal() -> String {
        return NSLocalizedString("In Total", comment: "In Total")
    }
    
    class func priceIsNegative() -> String {
        return NSLocalizedString("Price couldn't be negative", comment: "Price is negative")
    }
    
    class func priceTooBig() -> String {
        if let maxPrice = PriceFormatter.roundDecimalForTwoPlacesToString(maxPrice) {
            let str = NSLocalizedString("Price is too big (maximum value is: %@)", comment: "Price is too big")
            
            return String(format: str, maxPrice)
        }
        
        return ""
    }
}
