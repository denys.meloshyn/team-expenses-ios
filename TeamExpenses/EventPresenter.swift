//
//  EventPresenter.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 23/08/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit

enum EventState {
    case Expense
    case Person
}

protocol EventPresenterDelegate: class {
    func showPersonList()
    func showExpenseList()
    func showEmptyMessage()
    func refreshExpensesList()
    func state() -> EventState
    func showPageTitle(_ title: String?)
    func listDidChange(for state:EventState)
    func listWillChange(for state:EventState)
    func applyPageBackgroundColor(_ color: UIColor)
    func configurePeopleList(with cellName: String)
    func configureExpenseList(with cellName: String)
    func deselectPersonCell(at indexPath: IndexPath)
    func deselectExpenseCell(at indexPath: IndexPath)
    func state(for table: UITableView?) -> EventState
    func applySwitcherBackgroundColor(_ color: UIColor)
    func configureSwitcher(with selectedState: EventState)
    func showSplitButtonAndNewExpense(_ splitIcon: String)
    func showSplitButton(_ splitIcon: String, and createUser: String)
    func showDeletePersonDialog(with title: String, message: String, person: Person?)
    func showDeleteExpenseDialog(with title: String, message: String, expense: Expense?)
    func createPersonCell(name cellIdentifier:String, and model: Person?) -> UITableViewCell?
    func createExpenseCell(name cellIdentifier:String, and model: Expense?) -> UITableViewCell?
    func createRowAction(title: String?, handler: @escaping (UITableViewRowAction, IndexPath) -> Swift.Void) -> UITableViewRowAction
    func listChanged(at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?, for state:EventState)
}

class EventPresenter: NSObject, EventInteractionDelegate, UITableViewDelegate, UITableViewDataSource {
    let router = EventRouter()
    let interaction = EventInteraction()
    
    weak var viewController: UIViewController?
    weak var delegate: EventPresenterDelegate?
    
    func initialConfiguration() {
        self.interaction.delegate = self
        self.router.viewController = self.viewController
        
        self.delegate?.showPageTitle(self.interaction.currentEvent?.title)
        self.delegate?.applyPageBackgroundColor(Constatnts.defaultBackgroundColor())
        self.delegate?.configureExpenseList(with: kExpenseTableViewCell)
        self.delegate?.configurePeopleList(with: kTeamMemberCell)
        self.delegate?.configureSwitcher(with: EventState.Expense)
        self.delegate?.applySwitcherBackgroundColor(Constatnts.defaultApperanceColor())
        
        self.updateEmptyMessageScreen()
        self.needUpdateNavigationBar()
    }
    
    func eventState(with index: Int?) -> EventState {
        if let selectedSegmentIndex = index {
            if selectedSegmentIndex == 0 {
                return EventState.Expense
            }
        }
        
        return EventState.Person
    }
    
    // MARK: - UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let state = self.delegate?.state(for: tableView) {
            if state == EventState.Expense {
                return self.interaction.numberOfExpenses()
            }
            
            return self.interaction.numberOfPersons()
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        
        if let state = self.delegate?.state(for: tableView) {
            if state == EventState.Expense {
                let expense = self.interaction.expense(at: indexPath)
                cell = self.delegate?.createExpenseCell(name: kExpenseTableViewCell, and: expense)
            }
            else {
                let person = self.interaction.person(at: indexPath)
                cell = self.delegate?.createPersonCell(name: kTeamMemberCell, and: person)
            }
        }
        
        if let cell = cell {
            return cell
        }
        
        return UITableViewCell()
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let state = self.delegate?.state(for: tableView) {
            if state == EventState.Expense {
                let expense = self.interaction.expense(at: indexPath)
                
                self.interaction.trackOpenningExpenseDetailScreen()
                self.router.showExpenseDetailScreen(with: expense, and: self.interaction.currentEvent)
                self.delegate?.deselectExpenseCell(at: indexPath)
            }
            else {
                let person = self.interaction.person(at: indexPath)
                
                if let person = person, !person.isUserCurrent()  {
                    self.interaction.trackOpenningContactDetailScreen()
                    self.router.showPersonDetailScreen(with: person, and: self.interaction.currentEvent)
                }
                self.delegate?.deselectPersonCell(at: indexPath)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        if let state = self.delegate?.state(for: tableView) {
            if state == EventState.Person {
                let person = self.interaction.person(at: indexPath)
                if let isCurrentUser = person?.isCurrentUser?.boolValue, isCurrentUser {
                    return UITableViewCellEditingStyle.none
                }
            }
            
            return UITableViewCellEditingStyle.delete
        }
        
        return UITableViewCellEditingStyle.none
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if let state = self.delegate?.state(for: tableView) {
            if state == EventState.Expense {
                let expense = self.interaction.expense(at: indexPath)
                
                let deleteAction = self.delegate?.createRowAction(title: LocalisationManager.delete(), handler: { (action: UITableViewRowAction, indexPath: IndexPath) in
                    self.delegate?.showDeleteExpenseDialog(with: LocalisationManager.expense(), message: LocalisationManager.deleteExpenseWarning(), expense: expense)
                })
                
                if let deleteAction = deleteAction {
                    return [deleteAction]
                }
                
            }
            else {
                let person = self.interaction.person(at: indexPath)
                
                let deleteAction = self.delegate?.createRowAction(title: LocalisationManager.delete(), handler: { (action: UITableViewRowAction, indexPath: IndexPath) in
                    self.delegate?.showDeletePersonDialog(with: LocalisationManager.teamMember(), message: LocalisationManager.deleteTeamMemberWarning(), person: person)
                })
                
                if let deleteAction = deleteAction {
                    return [deleteAction]
                }
            }
        }
        
        return nil
    }
    
    //MARK: - EventInteractionDelegate methods
    
    func createNewExpense() {
        self.router.showExpenseDetailScreen(with: nil, and: self.interaction.currentEvent)
    }
    
    func createNewPerson() {
        self.router.showPersonDetailScreen(with: nil, and: self.interaction.currentEvent)
    }
    
    func didChangeContent(for state: EventState) {
        self.updateEmptyMessageScreen()
        self.delegate?.listDidChange(for: state)
        
        if state == EventState.Person {
            self.delegate?.refreshExpensesList()
        }
    }
    
    func willChangeContent(for state: EventState) {
        self.delegate?.listWillChange(for: state)
    }
    
    func changed(at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?, for state: EventState) {
        self.delegate?.listChanged(at: indexPath, for: type, newIndexPath: newIndexPath, for: state)
    }
    
    func showResultScreen(with event: Event?) {
        self.router.showResultScreen(with: event)
    }
    
    // MARK: - Public methods
    
    func segmentValueChangedAction(_ sender: UISegmentedControl?) {
        self.updateEmptyMessageScreen()
        self.needUpdateNavigationBar()
    }
    
    // MARK: - Private methods 
    
    private func updateEmptyMessageScreen() {
        if let state = self.delegate?.state() {
            if state == EventState.Expense {
                if self.interaction.numberOfExpenses() == 0 {
                    self.delegate?.showEmptyMessage()
                    return
                }
                
                self.delegate?.showExpenseList()
            }
            else {
                self.delegate?.showPersonList()
            }
        }
    }
    
    private func needUpdateNavigationBar() {
        if let state = self.delegate?.state() {
            if state == EventState.Expense {
                self.delegate?.showSplitButtonAndNewExpense("split_expenses")
            }
            else {
                self.delegate?.showSplitButton("split_expenses", and: "account_plus")
            }
        }
    }
}
