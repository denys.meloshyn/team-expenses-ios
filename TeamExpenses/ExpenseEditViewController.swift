//
//  ExpenseEditViewController.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 28/12/15.
//  Copyright © 2015 Denys Meloshyn. All rights reserved.
//

import UIKit
import CoreData

class ExpenseEditViewController: UIViewController {
    // MARK: - IBOutlet properties
    
     @IBOutlet private var expenseEditView: ExpenseEditView?
    
    // MARK: - Public properties
    
    var currentEventModelID: NSManagedObjectID?
    var currentExpenseModelID: NSManagedObjectID?
    
    // MARK: - Life cycle methods
    
    override func viewDidLoad() {
        self.expenseEditView?.presenter.interaction.currentEventModelID = self.currentEventModelID
        self.expenseEditView?.presenter.interaction.currentExpenseModelID = self.currentExpenseModelID
        
        self.expenseEditView?.initialConfiguration()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.expenseEditView?.viewWillAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.expenseEditView?.viewWillDisappear()
    }
}
