//
//  Expense+CoreDataClass.h
//  
//
//  Created by Denys.Meloshyn on 08/09/16.
//
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

@class Event, Person;

NS_ASSUME_NONNULL_BEGIN

@interface Expense : BaseModel

@end

NS_ASSUME_NONNULL_END

#import "Expense+CoreDataProperties.h"
