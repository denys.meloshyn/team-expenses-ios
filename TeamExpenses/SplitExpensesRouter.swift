//
//  SplitExpensesRouter.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 08/09/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit
import MessageUI

class SplitExpensesRouter: NSObject {
    weak var viewController: UIViewController?
    
    func presentMailPage(_ mailViewController: MFMailComposeViewController, with report:String) {
        mailViewController.modalTransitionStyle = UIModalTransitionStyle.coverVertical
        
        if let data = report.data(using: String.Encoding.utf8) {
            mailViewController.addAttachmentData(data, mimeType: "text/plain", fileName: "Report.html")
        }
        
        self.viewController?.present(mailViewController, animated: true, completion: { () -> Void in
            UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        })
    }
}
