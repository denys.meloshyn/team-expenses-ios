//
//  EventViewController.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 30/10/15.
//  Copyright © 2015 Denys Meloshyn. All rights reserved.
//

import UIKit
import CoreData

class EventViewController: UIViewController {
    @IBOutlet private var eventView: EventView?
    
    var currentEventID: NSManagedObjectID?

    // MARK: - Life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.eventView?.presenter.interaction.currentEventID = self.currentEventID
        self.eventView?.initialConfiguration()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AnalyticsManager.trackScreenVisible(name: kEventScreen)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.eventView?.layoutSubViews()
    }
}
