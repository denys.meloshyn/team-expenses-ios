//
//  AsynchronousURLConnection.swift
//  EncryptedEmail
//
//  Created by Denys.Meloshyn on 11/06/15.
//  Copyright (c) 2015 Denys Meloshyn. All rights reserved.
//

import UIKit

class AsynchronousURLConnection: NSObject
{
    typealias AsynchronousCompletionBlock = (AnyObject?, NSError?) -> (Void)
    
    class var sharedInstance: AsynchronousURLConnection
    {
        struct Static {
            static let instance: AsynchronousURLConnection = AsynchronousURLConnection()
        }
        
        return Static.instance
    }
    
    func sendAsynchronousRequest(_ request: URLRequest, completionBlock: AsynchronousCompletionBlock?) -> URLSessionDataTask?
    {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        
        let complitionResponseBlock = { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            var result: Any?
            var jsonError: NSError?
            
            if let dataCheck = data {
                do {
                    result = try JSONSerialization.jsonObject(with: dataCheck, options: JSONSerialization.ReadingOptions.mutableContainers)
                }
                catch let error as NSError {
                    jsonError = error
                    result = nil
                }
                catch {
                    fatalError()
                }
            }
            
            if let _ = error {
                completionBlock?(nil, error as NSError?)
                
                return
            }
            else if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    
                    completionBlock?(result as AnyObject?, NSError.swiftError(httpResponse.statusCode))
                    
                    return
                }
            }
            
            if jsonError != nil {
                completionBlock?(nil, jsonError)
                
                return;
            }
            
            completionBlock?(result as AnyObject?, nil)
        }
        
        let task = session.dataTask(with: request, completionHandler: complitionResponseBlock)
        task.resume()
        
        return task
    }
}
