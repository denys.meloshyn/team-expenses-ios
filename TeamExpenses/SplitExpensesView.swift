//
//  SplitExpensesView.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 07/09/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit

class SplitExpensesView: NSObject, SplitExpensesPresenterDelegate {
    @IBOutlet private var tableView: UITableView?
    @IBOutlet weak var viewController: UIViewController?
    
    let presenter = SplitExpensesPresenter()
    
    func initialConfiguration() {
        self.presenter.delegate = self
        self.presenter.viewController = self.viewController
        self.presenter.initialConfiguration()
        
        self.tableView?.delegate = self.presenter
        self.tableView?.dataSource = self.presenter
    }
    
    // MARK: - SplitExpensesPresenterDelegate methods
    
    func showShareButton() {
        let shareBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.action, target: self.presenter, action: #selector(SplitExpensesPresenter.exportReport))
        self.viewController?.navigationItem.rightBarButtonItem = shareBarButtonItem
    }
    
    func showPageTitle(_ title: String) {
        self.viewController?.navigationItem.title = title
    }
    
    func applyTableBackgroundColor(_ color: UIColor) {
        self.tableView?.backgroundColor = color
    }
    
    func createCell(with title: String?, and detailText: NSAttributedString?) -> UITableViewCell? {
        let cell = self.tableView?.dequeueReusableCell(withIdentifier: "UserSumCell")
        
        cell?.textLabel?.text = title
        cell?.detailTextLabel?.attributedText = detailText
        
        return cell
    }
    
    func showHeader(_ view: UIView?, with tintColor: UIColor, and textColor: UIColor) {
        let headerView = view as? UITableViewHeaderFooterView
        
        headerView?.tintColor = tintColor
        headerView?.textLabel?.textColor = textColor
    }
}
