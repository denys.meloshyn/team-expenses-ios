//
//  BaseModel+CoreDataProperties.h
//  
//
//  Created by Denys.Meloshyn on 08/09/16.
//
//

#import "BaseModel.h"


NS_ASSUME_NONNULL_BEGIN

@interface BaseModel (CoreDataProperties)

+ (NSFetchRequest<BaseModel *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *tmpDate;
@property (nullable, nonatomic, copy) NSDate *updateDate;

@end

NS_ASSUME_NONNULL_END
