//
//  EventRouter.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 24/08/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit

class EventRouter: NSObject {
    weak var viewController: UIViewController?
    
    func showExpenseDetailScreen(with expense: Expense?, and event: Event?) {
        if let viewController = self.viewController?.storyboard?.instantiateViewController(withIdentifier: "ExpenseEditViewController") as? ExpenseEditViewController {
            viewController.currentEventModelID = event?.objectID
            viewController.currentExpenseModelID = expense?.objectID
            
            self.viewController?.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func showPersonDetailScreen(with person: Person?, and event: Event?) {
        if let viewController = self.viewController?.storyboard?.instantiateViewController(withIdentifier: "ContactEditViewController") as? ContactEditViewController {
            viewController.managedObjectContext = ExpenseManager.sharedInstance.managedObjectContext
            viewController.currentEventID = event?.objectID
            viewController.currentPersonID = person?.objectID
            
            self.viewController?.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func showResultScreen(with event: Event?) {
        if let viewController = self.viewController?.storyboard?.instantiateViewController(withIdentifier: "SplitExpensesViewController") as? SplitExpensesViewController {
            viewController.currentEventID = event?.objectID
            
            self.viewController?.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}
