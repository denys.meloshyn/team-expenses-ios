//
//  EventListRouter.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 22/08/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit

class EventListRouter: NSObject {
    weak var viewController: UIViewController?
    
    func showDetailPage(for event: Event) {
        if let eventViewController = self.viewController?.storyboard?.instantiateViewController(withIdentifier: "EventViewController") as? EventViewController {
            eventViewController.currentEventID = event.objectID
            
            self.viewController?.navigationController?.pushViewController(eventViewController, animated: true)
        }
    }
}
