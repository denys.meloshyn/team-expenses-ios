//
//  EditEventViewController.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 29/10/15.
//  Copyright © 2015 Denys Meloshyn. All rights reserved.
//

import UIKit
import CoreData

class EditEventViewController: UIViewController {
    @IBOutlet private var tableView: UITableView?
    @IBOutlet private var textField: UITextField?
    
    var persons = [Person]()
    var currentEventID: NSManagedObjectID?
    let temporaryManagedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(ExpenseManager.sharedInstance.managedObjectContext)
    
    private var currentEvent: Event?
    
    // MARK: - Life cicle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let currentEventID = self.currentEventID {
            self.currentEvent = self.temporaryManagedObjectContext.object(with: currentEventID) as? Event
            
            self.textField?.text = self.currentEvent?.title
            
            let barItem = UIBarButtonItem(title: "Update", style: UIBarButtonItemStyle.plain, target: self, action: #selector(EditEventViewController.save))
            self.navigationItem.rightBarButtonItem = barItem
        }
        else {
            self.currentEvent = ExpenseManager.sharedInstance.createNewEvent(self.temporaryManagedObjectContext)
            
            let barItem = UIBarButtonItem(title: "Save", style: UIBarButtonItemStyle.plain, target: self, action: #selector(EditEventViewController.save))
            self.navigationItem.rightBarButtonItem = barItem
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tableView?.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ContactsSegue" {
            if let viewController = segue.destination as? ContactListViewController {
                viewController.currentEventID = self.currentEvent?.objectID
                viewController.managedObjectContext = self.temporaryManagedObjectContext
            }
        }
    }
    
    // MARK: - UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.persons.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAtIndexPath indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "EditEventCell") {
            _ = self.persons[ (indexPath as NSIndexPath).row ]
//            cell.textLabel?.text = person.firstName
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, didSelectRowAtIndexPath indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    // MARK: - Private methods
    
    private func dismiss() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func save() {
        self.currentEvent?.title = self.textField?.text
        
//        self.currentEvent?.addChangedProperty(Event.kTitle, managedObjectContext: self.temporaryManagedObjectContext)
        
        ExpenseManager.sharedInstance.saveTemporaryManagedObjectContext(self.temporaryManagedObjectContext) {[weak self] (response, error) -> (Void) in
            DispatchQueue.main.async(execute: { () -> Void in
                self?.dismiss()
            })
        }
    }
    
    func update() {
        self.save ()
    }
    
    // MARK: - IBAction methods
    
    @IBAction private func textFieldValueChanged() {
        
    }
    
    @IBAction private func saveAction(_ sender: UIBarButtonItem) {
        self.save()
    }
    
    @IBAction private func cancelAction(_ sender: UIBarButtonItem) {
        self.dismiss()
    }
}
