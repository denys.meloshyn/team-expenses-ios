//
//  BaseModel+CoreDataProperties.m
//  
//
//  Created by Denys.Meloshyn on 08/09/16.
//
//

#import "BaseModel+CoreDataProperties.h"

@implementation BaseModel (CoreDataProperties)

+ (NSFetchRequest<BaseModel *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"BaseModel"];
}

@dynamic tmpDate;
@dynamic updateDate;

@end
