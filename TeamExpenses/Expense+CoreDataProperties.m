//
//  Expense+CoreDataProperties.m
//  
//
//  Created by Denys.Meloshyn on 08/09/16.
//
//

#import "Expense+CoreDataProperties.h"

@implementation Expense (CoreDataProperties)

+ (NSFetchRequest<Expense *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Expense"];
}

@dynamic creationDate;
@dynamic price;
@dynamic title;
@dynamic creator;
@dynamic event;
@dynamic personsInDeal;

@end
