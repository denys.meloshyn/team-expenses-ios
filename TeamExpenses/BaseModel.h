//
//  BaseModel+CoreDataClass.h
//  
//
//  Created by Denys.Meloshyn on 08/09/16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseModel : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "BaseModel+CoreDataProperties.h"
