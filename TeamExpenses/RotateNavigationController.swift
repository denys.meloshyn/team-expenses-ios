//
//  RotateNavigationController.swift
//  Muzarium
//
//  Created by Denys.Meloshyn on 26/06/15.
//  Copyright (c) 2015 Denys Meloshyn. All rights reserved.
//

import UIKit

/*
* The class is intended to implement rotation methods
* in supported interface orientation it asks the content controllers about the supported interface orientations
* what makes much easier to set the interface orientations explicitly in each controller
*/

class RotateNavigationController: UINavigationController {
    override var shouldAutorotate: Bool {
        get {
            if let topViewController = self.topViewController {
                return topViewController.shouldAutorotate
            }
            
            return true
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get {
            if let topViewController = self.topViewController {
                return topViewController.supportedInterfaceOrientations
            }
            
            return UIInterfaceOrientationMask.portrait
        }
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        get {
            if let topViewController = self.topViewController {
                return topViewController.preferredInterfaceOrientationForPresentation
            }
            
            return UIInterfaceOrientation.portrait
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return UIStatusBarStyle.lightContent
        }
    }
}
