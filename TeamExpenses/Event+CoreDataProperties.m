//
//  Event+CoreDataProperties.m
//  
//
//  Created by Denys.Meloshyn on 08/09/16.
//
//

#import "Event+CoreDataProperties.h"

@implementation Event (CoreDataProperties)

+ (NSFetchRequest<Event *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Event"];
}

@dynamic creationDate;
@dynamic title;
@dynamic expenses;
@dynamic persons;

@end
