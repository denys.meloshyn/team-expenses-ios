//
//  ContactEditViewController.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 12/01/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit
import CoreData

class ContactEditViewController: UIViewController {
    @IBOutlet private var lastNameBorderView: UIView?
    @IBOutlet private var firstNameBorderView: UIView?
    @IBOutlet private var emailTextField: UITextField?
    @IBOutlet private var lastNameTextField: UITextField?
    @IBOutlet private var firstNameTextField: UITextField?
    
    // MARK: - Public properties
    
    var currentEventID: NSManagedObjectID?
    var currentPersonID: NSManagedObjectID?
    var currentExpenseID: NSManagedObjectID?
    var currentTeamMemberID: NSManagedObjectID?
    var managedObjectContext: NSManagedObjectContext?
    
    // MARK: - Private properties
    
    private var currentEvent: Event?
    private var currentPerson: Person?
    private var currentExpense: Expense?
    private let childrenManagedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(ExpenseManager.sharedInstance.managedObjectContext)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = Constatnts.defaultBackgroundColor()
        
        // Get event by ID
        if let currentEventID = self.currentEventID {
            self.currentEvent = self.childrenManagedObjectContext.object(with: currentEventID) as? Event
        }
        
        // Get expense by ID
        if let currentExpenseID = self.currentExpenseID {
            self.currentExpense = self.childrenManagedObjectContext.object(with: currentExpenseID) as? Expense
        }
        
        // Get person by ID
        if let currentPersonID = self.currentPersonID {
            self.currentPerson = self.childrenManagedObjectContext.object(with: currentPersonID) as? Person
            
            let updateBarButtonItem = UIBarButtonItem(title: LocalisationManager.update(), style: UIBarButtonItemStyle.done, target: self, action: #selector(ContactEditViewController.save))
            self.navigationItem.rightBarButtonItem = updateBarButtonItem
        }
        else {
            // Create new person
            self.currentPerson = ExpenseManager.sharedInstance.createNewPerson(self.childrenManagedObjectContext)
            if let currentPerson = currentPerson {
                if let currentEvent = self.currentEvent {
                    currentPerson.addEventObject(currentEvent)
                }
                
                self.currentEvent?.addPersonsObject(currentPerson)
            }
            
            let saveBarButtonItem = UIBarButtonItem(title: LocalisationManager.create(), style: UIBarButtonItemStyle.done, target: self, action: #selector(ContactEditViewController.save))
            self.navigationItem.rightBarButtonItem = saveBarButtonItem
        }
        
        self.lastNameBorderView?.layer.borderWidth = 1.0
        self.lastNameBorderView?.layer.cornerRadius = 5.0
        self.firstNameBorderView?.layer.borderWidth = 1.0
        self.firstNameBorderView?.layer.cornerRadius = 5.0
        
        self.firstNameTextField?.text = self.currentPerson?.name
        
        self.firstNameTextField?.becomeFirstResponder()
        self.updateTextBorderViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AnalyticsManager.trackScreenVisible(name: kContactEditScreen)
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.firstNameTextField {
            self.lastNameTextField?.becomeFirstResponder()
        }
        else {
            self.save()
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.updateTextBorderViews()
    }
    
    // MARK: - Public methods
    
    private func updateTextBorderViews() {
        let defaultBorderColor = Constatnts.defaultTextBorderColor()
        self.firstNameBorderView?.layer.borderColor = defaultBorderColor.cgColor
        self.lastNameBorderView?.layer.borderColor = defaultBorderColor.cgColor
        
        if let isFirstResponder = self.firstNameTextField?.isFirstResponder , isFirstResponder {
            self.firstNameBorderView?.layer.borderColor = Constatnts.defaultActiveColor().cgColor
        }
        else if let isFirstResponder = self.lastNameTextField?.isFirstResponder , isFirstResponder {
            self.lastNameBorderView?.layer.borderColor = Constatnts.defaultActiveColor().cgColor
        }
    }
    
    func save() {
        var lastNameText = ""
        if let text = self.lastNameTextField?.text {
            lastNameText = text.trimmingCharacters(in: CharacterSet.whitespaces)
            self.lastNameTextField?.text = lastNameText
        }
        
        var firstNameText = ""
        if let text = self.firstNameTextField?.text {
            firstNameText = text.trimmingCharacters(in: CharacterSet.whitespaces)
            self.firstNameTextField?.text = firstNameText
        }
        
        if firstNameText.characters.count == 0 && lastNameText.characters.count == 0 {
            AnalyticsManager.track(kContactEditScreen, action: kSaveEmptyNameLastNameAction, label: "")
            
            self.firstNameTextField?.becomeFirstResponder()
            
            self.firstNameBorderView?.layer.borderColor = Constatnts.defaultErrorColor().cgColor
            self.lastNameBorderView?.layer.borderColor = Constatnts.defaultErrorColor().cgColor
            
            let alertController = UIAlertController(title: LocalisationManager.teamMember(), message: LocalisationManager.firstOrLastNameIsEmpty(), preferredStyle: UIAlertControllerStyle.alert)
            let cancelAction = UIAlertAction(title: LocalisationManager.cancel(), style: UIAlertActionStyle.cancel) { (_) in
                AnalyticsManager.track(kContactEditScreen, action: kCancelAction, label: kSaveEmptyNameLastNameAction)
            }
            
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
            alertController.view.tintColor = Constatnts.defaultActiveColor()
            
            return
        }
        
        AnalyticsManager.track(kContactEditScreen, action: kEditAction, label: "")
        
        self.currentPerson?.name = self.firstNameTextField?.text
        
        ExpenseManager.sharedInstance.saveTemporaryManagedObjectContext(self.childrenManagedObjectContext, completionBlock: { [weak self] (response, error)  -> (Void) in
            DispatchQueue.main.async(execute: { () -> Void in
                _ = self?.navigationController?.popViewController(animated: true)
            })
        })
    }
}
