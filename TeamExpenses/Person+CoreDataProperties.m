//
//  Person+CoreDataProperties.m
//  
//
//  Created by Denys.Meloshyn on 08/09/16.
//
//

#import "Person+CoreDataProperties.h"

@implementation Person (CoreDataProperties)

+ (NSFetchRequest<Person *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Person"];
}

@dynamic isCurrentUser;
@dynamic name;
@dynamic event;
@dynamic expenseCreator;
@dynamic expensePersonsInDeal;

@end
