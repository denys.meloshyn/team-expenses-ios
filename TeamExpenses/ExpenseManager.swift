//
//  ExpenseManager.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 27/10/15.
//  Copyright © 2015 Denys Meloshyn. All rights reserved.
//

import UIKit
import CoreData
import XCGLogger

class ExpenseManager: NSObject {
    class var sharedInstance: ExpenseManager {
        struct Static {
            static let instance: ExpenseManager = ExpenseManager()
        }
        
        return Static.instance
    }
    
    override init() {
        super.init()
    }
    
    // MARK: - Private properties
    
    private lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.denys.meloshyn.TeamExpenses" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    private lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "TeamExpenses", withExtension: "momd")!
        
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    private lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent(self.dataBaseName())
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            // Report any error we got.
            var dict = [String: Any]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data"
            dict[NSLocalizedFailureReasonErrorKey] = failureReason
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            XCGLogger.default.error("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    
    // MARK: - Class methods
    
    class func createNewExpense(_ managedObjectContext: NSManagedObjectContext) -> Expense? {
        if let entity = NSEntityDescription.entity(forEntityName: "Expense", in: managedObjectContext) {
            if let model = NSManagedObject(entity: entity, insertInto: managedObjectContext) as? Expense {
                return model
            }
        }
        
        return nil
    }
    
    func eventFetchController(_ managedObjectContext: NSManagedObjectContext) -> NSFetchedResultsController<Event> {
        return ExpenseManager.eventFetchController(managedObjectContext)
    }
    
    class func eventFetchController(_ managedObjectContext: NSManagedObjectContext, includeRemoved: Bool = false) -> NSFetchedResultsController<Event> {
        let fetchRequest = NSFetchRequest<Event>(entityName: "Event")
        fetchRequest.fetchBatchSize = 30
        
        let sortDescriptor = NSSortDescriptor(key: "creationDate", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        return fetchedResultsController
    }
    
    class func expenseFetchController(_ eventModel: Event?, managedObjectContext: NSManagedObjectContext) -> NSFetchedResultsController<Expense> {
        let fetchRequest = NSFetchRequest<Expense>(entityName: "Expense")
        fetchRequest.fetchBatchSize = 30
        
        let sortDescriptor = NSSortDescriptor(key: "creationDate", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        var predicates = [NSPredicate]()
        let orPredicate = [NSPredicate]()
        
        if orPredicate.count > 0 {
            let idCompoundPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: orPredicate)
            predicates.append(idCompoundPredicate)
        }
        
        let compoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        fetchRequest.predicate = compoundPredicate
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        return fetchedResultsController
    }
    
    class func teamMembersFetchController(_ eventModel: Event?, managedObjectContext: NSManagedObjectContext, includeRemoved: Bool = false, isDisplayCurrentUser: Bool = false) -> NSFetchedResultsController<Person> {
        let fetchRequest = NSFetchRequest<Person>(entityName: "Person")
        fetchRequest.fetchBatchSize = 30
        
        let sortDescriptor = NSSortDescriptor(key: "lastName", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        var predicates = [NSPredicate]()
        var predicate = NSPredicate(format: "isRemoved == NO")
        
        var orPredicates = [NSPredicate]()
        
        if isDisplayCurrentUser {
            predicate = NSPredicate(format: "userID == %@", UserInfo.sharedInstance.userID as NSObject)
            orPredicates.append(predicate)
        }
        
        if orPredicates.count > 0 {
            let idCompoundPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: orPredicates)
            predicates.append(idCompoundPredicate)
        }
        
        let compoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
        fetchRequest.predicate = compoundPredicate
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        return fetchedResultsController
    }
    
    // MARK: - Private methods
    
    func dataBaseName() -> String {
        // Check if we are running as test or not
        let environment = ProcessInfo.processInfo.environment as [String : AnyObject]
        if let isTest = environment["XCInjectBundle"] as? String {
            let url = URL(fileURLWithPath: isTest)
            
            if url.pathExtension == "xctest" {
                return "SingleViewCoreDataTest.sqlite"
            }
        }
        
        // Create the module name
        return "SingleViewCoreData.sqlite"
    }
    
    private func dropEntityName(_ entityName: String, managedObjectContext: NSManagedObjectContext) {
        let entityDescription = NSEntityDescription.entity(forEntityName: entityName, in: managedObjectContext)
        let fetchRequest = NSFetchRequest<NSManagedObject>()
        fetchRequest.entity = entityDescription
        fetchRequest.includesPropertyValues = false
        
        var items: [NSManagedObject]?
        do {
            try items = managedObjectContext.fetch(fetchRequest)
            
            if let items = items {
                for model in items {
                    managedObjectContext.delete(model)
                }
            }
        }
        catch let error as NSError {
            XCGLogger.default.error("ExpenseManager error: couldn't execute fetch request to drop \(entityName) \n \(error)")
        }
    }
    
    private func findEventModel(_ eventID: Int, porpertyName: String) -> Event? {
        let fetchRequest = NSFetchRequest<Event>(entityName: "Event")
        fetchRequest.fetchLimit = 1
        
        let predicate = NSPredicate(format: "%K == %@", porpertyName, eventID as NSObject)
        fetchRequest.predicate = predicate
        
        do {
            if let model = try self.managedObjectContext.fetch(fetchRequest).first {
                return model
            }
        }
        catch {
            
        }
        
        return nil
    }
    
    // MARK: - Public methods
    
    func createChildrenManagedObjectContextFromParentContext(_ parentContext: NSManagedObjectContext?) -> NSManagedObjectContext {
        let childrenManagedObjectContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
        childrenManagedObjectContext.parent = parentContext
        
        return childrenManagedObjectContext
    }
    
    func saveManagedObjectContextWithPerformBlock(_ childrenManagedObjectContext: NSManagedObjectContext?, completionBlock: AsynchronousURLConnection.AsynchronousCompletionBlock?) {
        childrenManagedObjectContext?.perform({ 
            do {
                try self.saveContext(childrenManagedObjectContext)
                
                completionBlock?(nil, nil)
            }
            catch let error as NSError {
                XCGLogger.default.error("Error saving parent context \(error)")
                completionBlock?(nil, error)
            }
        })
    }
    
    func saveTemporaryManagedObjectContext(_ temporaryManagedObjectContext: NSManagedObjectContext?, completionBlock: AsynchronousURLConnection.AsynchronousCompletionBlock?) {
        temporaryManagedObjectContext?.perform { () -> Void in
            do {
                try self.saveContext(temporaryManagedObjectContext)
                
                temporaryManagedObjectContext?.parent?.perform({() -> Void in
                    do {
                        try self.saveContext(temporaryManagedObjectContext?.parent)
                        
                        completionBlock?(nil, nil)
                    }
                    catch let error as NSError {
                        AnalyticsManager.trackError("Error saving parent context \(error)")
                        completionBlock?(nil, error)
                    }
                    })
            }
            catch let error as NSError {
                AnalyticsManager.trackError("Error saving children context \(error)")
                completionBlock?(nil, error)
            }
        }
    }
    
    func saveContext(_ managedObjectContext: NSManagedObjectContext?) throws {
        if let hasChanges = managedObjectContext?.hasChanges {
            if hasChanges {
                try managedObjectContext?.save()
            }
        }
    }
    
    func nextInternalID(_ managedObjectContext: NSManagedObjectContext) -> Int {
        return 0
    }
    
    func markAllAsSynchronised(_ managedObjectContext: NSManagedObjectContext) {
        let fetchedResultsController = ExpenseManager.eventFetchController(managedObjectContext)
        
        do {
            try fetchedResultsController.performFetch()
            
            if let objects = fetchedResultsController.fetchedObjects {
                for _ in objects {
                    
                }
            }
        }
        catch {
            XCGLogger.default.error("Error marking all objects as synchronised")
        }
    }
    
    func dropDataBase(_ completionBlock: AsynchronousURLConnection.AsynchronousCompletionBlock?) {
        let temporayManagedObjectContext = self.createChildrenManagedObjectContextFromParentContext(self.managedObjectContext)
        
        self.dropEntityName("BaseModel", managedObjectContext: temporayManagedObjectContext)
        
        self.saveTemporaryManagedObjectContext(temporayManagedObjectContext) { (response, error) -> (Void) in
            completionBlock?(response, error)
        }
    }
    
    // MARK: - Entity
    
    func createNewEntityWithName(_ entityName: String, managedObjectContext: NSManagedObjectContext) -> BaseModel? {
        var model: BaseModel?
        
        if let entity = NSEntityDescription.entity(forEntityName: entityName, in: managedObjectContext) {
            model = NSManagedObject(entity: entity, insertInto: managedObjectContext) as? BaseModel
        }
        
        return model
    }
    
    class func createNewEntityWithName(_ entityName: String, managedObjectContext: NSManagedObjectContext) -> BaseModel? {
        var model: BaseModel?
        
        if let entity = NSEntityDescription.entity(forEntityName: entityName, in: managedObjectContext) {
            model = NSManagedObject(entity: entity, insertInto: managedObjectContext) as? BaseModel
        }
        
        return model
    }
    
    func synchroniseEntityFetchController(_ entityName: String, managedObjectContext: NSManagedObjectContext) -> NSFetchedResultsController<BaseModel> {
        let fetchRequest = NSFetchRequest<BaseModel>(entityName: entityName)
        fetchRequest.fetchBatchSize = 30
        
        let predicate = NSPredicate(format: "changedProperties.@count > 0")
        fetchRequest.predicate = predicate
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        
        return fetchedResultsController
    }
    
    // MARK: - BaseModel
    
    func findModelWithInternalID(_ internalID: Int, managedObjectContext: NSManagedObjectContext) -> BaseModel? {
        let fetchRequest = NSFetchRequest<BaseModel>(entityName: "BaseModel")
        fetchRequest.fetchLimit = 1
        
        let predicate = NSPredicate(format: "internalID == %@", internalID as NSObject)
        fetchRequest.predicate = predicate
        
        var model: BaseModel?
        do {
            model = try managedObjectContext.fetch(fetchRequest).first
        }
        catch {
            XCGLogger.default.error("Error: Couldn't execute fetch request to find model with internal ID: \(internalID)")
        }
        
        return model
    }
    
    // MARK: - Event
    
    func createNewEvent(_ managedObjectContext: NSManagedObjectContext) -> Event? {
        let entity = self.createNewEntityWithName("Event", managedObjectContext: managedObjectContext) as? Event
        
        return entity
    }
    
    func eventPersonFetchController(_ eventObjectID: NSManagedObjectID?, managedObjectContext: NSManagedObjectContext) -> NSFetchedResultsController<BaseModel> {
        let fetchRequest = NSFetchRequest<BaseModel>(entityName: "Person")
        fetchRequest.fetchBatchSize = 30
        
        var sortDescriptors = [NSSortDescriptor]()
        
        var sortDescriptor = NSSortDescriptor(key: "isCurrentUser", ascending: false)
        sortDescriptors.append(sortDescriptor)
        
        sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        sortDescriptors.append(sortDescriptor)
        
        fetchRequest.sortDescriptors = sortDescriptors
        
        if let eventObjectID = eventObjectID {
            let model = managedObjectContext.object(with: eventObjectID)
            let predicate = NSPredicate(format: "%@ IN event", model)
            fetchRequest.predicate = predicate
        }
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        return fetchedResultsController
    }
    
    func findEventModel(_ eventID: Int) -> Event? {
        return self.findEventModel(eventID, porpertyName: "eventID")
    }
    
    func findEventModel(_ eventID: Int, managedObjectContext: NSManagedObjectContext) -> Event? {
        let fetchRequest = NSFetchRequest<Event>(entityName: "Event")
        fetchRequest.fetchLimit = 1
        
        let predicate = NSPredicate(format: "eventID == %@", eventID as NSObject)
        fetchRequest.predicate = predicate
        
        var model: Event?
        do {
            model = try managedObjectContext.fetch(fetchRequest).first
        }
        catch {
            XCGLogger.default.error("Error: Couldn't execute fetch request to find event with ID: \(eventID)")
        }
        
        return model
    }
    
    func findInternalEventModel(_ internalEventID: Int) -> Event? {
        return self.findEventModel(internalEventID, porpertyName: "internalEventID")
    }
    
    // MARK: - Expenses
    
    func createNewExpense(_ managedObjectContext: NSManagedObjectContext) -> Expense? {
        let entity = self.createNewEntityWithName("Expense", managedObjectContext: managedObjectContext) as? Expense
        
        return entity
    }
    
    func addExpense(_ model: Expense?) {
        if let model = model {
            self.managedObjectContext.insert(model)
        }
    }
    
    func findExpenseModel(_ expenseID: Int, managedObjectContext: NSManagedObjectContext) -> Expense? {
        let fetchRequest = NSFetchRequest<Expense>(entityName: "Expense")
        fetchRequest.fetchLimit = 1
        
        let predicate = NSPredicate(format: "expenseID == %@", expenseID as NSObject)
        fetchRequest.predicate = predicate
        
        var model: Expense?
        do {
            model = try managedObjectContext.fetch(fetchRequest).first
        }
        catch let error as NSError {
            XCGLogger.default.error("ExpenseManager error: Couldn't execute fetch request to find expense \(error)")
        }
        
        return model
    }
    
    func expensesFetchControllerForEvent(_ eventModelID: NSManagedObjectID?, managedObjectContext: NSManagedObjectContext, includeRemoved: Bool = false) -> NSFetchedResultsController<Expense> {
        let fetchRequest = NSFetchRequest<Expense>(entityName: "Expense")
        fetchRequest.fetchBatchSize = 30
        
        let sortDescriptor = NSSortDescriptor(key: "creationDate", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        if let eventModelID = eventModelID {
            let event = managedObjectContext.object(with: eventModelID)
            let predicate = NSPredicate(format: "event == %@", event)
            fetchRequest.predicate = predicate
        }
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        return fetchedResultsController
    }
    
    func expensesFetchControllerForPerson(_ personModelID: NSManagedObjectID?, in eventModelID: NSManagedObjectID?, managedObjectContext: NSManagedObjectContext) -> NSFetchedResultsController<Expense> {
        let fetchRequest = NSFetchRequest<Expense>(entityName: "Expense")
        fetchRequest.fetchBatchSize = 30
        
        let sortDescriptor = NSSortDescriptor(key: "creationDate", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        if let eventModelID = eventModelID {
            if let personModelID = personModelID {
                let event = managedObjectContext.object(with: eventModelID)
                let person = managedObjectContext.object(with: personModelID)
                
                let predicate = NSPredicate(format: "event == %@ AND creator == %@", event, person)
                fetchRequest.predicate = predicate
            }
        }
        
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        return fetchedResultsController
    }
    
    func removeAllExpensesForPerson(_ eventModelID: NSManagedObjectID, personModelID: NSManagedObjectID, managedObjectContext: NSManagedObjectContext) {
        let person = managedObjectContext.object(with: personModelID) as? Person
        let event = managedObjectContext.object(with: eventModelID) as? Event
        
        let fetchRequest = NSFetchRequest<Expense>(entityName: "Expense")
        let sortDescriptor = NSSortDescriptor(key: "creationDate", ascending: false)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        var predicates = [NSPredicate]()
        if let person = person {
            if let event = event {
                let eventPredicate = NSPredicate(format: "event == %@", event)
                let creatorPredicate = NSPredicate(format: "creator == %@", person)
                
                predicates.append(eventPredicate)
                predicates.append(creatorPredicate)
            }
        }
        
        if predicates.count > 0 {
            let compoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
            fetchRequest.predicate = compoundPredicate
            let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
            
            do {
                try fetchedResultsController.performFetch()
                
                if let sections = fetchedResultsController.sections {
                    var i = 0;
                    for section in sections {
                        for j in 0..<section.numberOfObjects {
                            let indexPath = IndexPath(row: j, section: i)
                            let expense = fetchedResultsController.object(at: indexPath)
                            if let personsInDeal = expense.personsInDeal {
                                for expensePerson in personsInDeal {
                                    expensePerson.removeExpensePersons(inDealObject: expense)
                                }
                            }
                            event?.removeExpensesObject(expense)
                            
                            managedObjectContext.delete(expense)
                        }
                    }
                    
                    i += 1
                }
                
                if let person = person {
                    event?.removePersonsObject(person)
                }
            }
            catch {
                XCGLogger.default.error("ExpenseManager error: Couldn't execute fetch request to remove expenses \(error)")
            }
        }
        
        predicates = [NSPredicate]()
        if let person = person {
            if let event = event {
                let eventPredicate = NSPredicate(format: "event == %@", event)
                let creatorPredicate = NSPredicate(format: "%@ IN personsInDeal", person)
                
                predicates.append(eventPredicate)
                predicates.append(creatorPredicate)
            }
        }
        
        if predicates.count > 0 {
            let compoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: predicates)
            fetchRequest.predicate = compoundPredicate
            let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
            
            do {
                try fetchedResultsController.performFetch()
                
                if let sections = fetchedResultsController.sections {
                    var i = 0;
                    for section in sections {
                        for j in 0..<section.numberOfObjects {
                            let indexPath = IndexPath(row: j, section: i)
                            let expense = fetchedResultsController.object(at: indexPath)
                            if let person = person {
                                expense.removePersons(inDealObject: person)
                            }
                        }
                    }
                    i += 1
                }
            }
            catch {
                XCGLogger.default.error("ExpenseManager error: Couldn't execute fetch request to remove expenses \(error)")
            }
        }
        
        if let person = person {
            managedObjectContext.delete(person)
        }
    }
    
    // MARK: - Person
    
    func currentUser(_ managedObjectContext: NSManagedObjectContext) -> Person? {
        let fetchRequest = NSFetchRequest<Person>(entityName: "Person")
        fetchRequest.fetchLimit = 1
        
        let predicate = NSPredicate(format: "isCurrentUser == YES")
        fetchRequest.predicate = predicate
        
        var model: Person?
        do {
            model = try managedObjectContext.fetch(fetchRequest).first
        }
        catch {
            XCGLogger.default.error("Error: Couldn't execute fetch request to find current user \(UserInfo.sharedInstance.userID)")
        }
        
        if model == nil {
            model = ExpenseManager.sharedInstance.createNewPerson(managedObjectContext)
            model?.isCurrentUser = true
        }
        
        return model
    }
    
    func createNewPerson(_ managedObjectContext: NSManagedObjectContext) -> Person? {
        let entity = self.createNewEntityWithName("Person", managedObjectContext: managedObjectContext) as? Person
        
        return entity
    }
    
    func findPersonModel(_ userID: Int, managedObjectContext: NSManagedObjectContext) -> Person? {
        let fetchRequest = NSFetchRequest<Person>(entityName: "Person")
        fetchRequest.fetchLimit = 1
        
        let predicate = NSPredicate(format: "userID == %@", userID as NSObject)
        fetchRequest.predicate = predicate
        
        var model: Person?
        do {
            model = try managedObjectContext.fetch(fetchRequest).first
        }
        catch {
            XCGLogger.default.error("Error: Couldn't execute fetch request to find user: \(userID)")
        }
        
        return model
    }
    
    func addPerson(_ model: Person?) {
        if let model = model {
            self.managedObjectContext.insert(model)
        }
    }
    
    func events () -> [Event] {
        let fetchRequest = NSFetchRequest<Event>()
        let entity = NSEntityDescription.entity(forEntityName: "Event", in: self.managedObjectContext)
        fetchRequest.entity = entity
        
        var result = [Event]()
        
        do {
            let fetchedObjects = try self.managedObjectContext.fetch(fetchRequest)
            result = fetchedObjects
        }
        catch let error as NSError {
            XCGLogger.default.error("Error extracting events from Core Data \(error)")
        }
        
        return result
    }
}
