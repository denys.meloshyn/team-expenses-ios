//
//  ExpenseTableViewCell.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 15/01/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit
import CoreData

class ExpenseTableViewCell: UITableViewCell {
    @IBOutlet private var statusView: UIView?
    @IBOutlet private var titleLabel: UILabel?
    @IBOutlet private var priceLabel: UILabel?
    @IBOutlet private var subTitleLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let selectedView = UIView()
        selectedView.backgroundColor = Constatnts.defaultActiveColor()
        self.selectedBackgroundView = selectedView
        
        self.priceLabel?.textColor = Constatnts.defaultPriceColor()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.titleLabel?.text = ""
        self.priceLabel?.text = ""
        self.subTitleLabel?.text = ""
    }
    
    func configureWithModel(_ model: Expense?) {
        var attributes: [String: AnyObject]?
        var attributedSting: NSAttributedString?
        let resultAttributedString = NSMutableAttributedString()
        
        if let firstLastName = model?.creator?.userName() , firstLastName.characters.count > 0 {
            attributedSting = NSAttributedString(string: firstLastName)
            if let attributedSting = attributedSting {
                resultAttributedString.append(attributedSting)
            }
        }
        
        var splitBetweenPersonsTitle = self.splitMembersDescription(currentUserName: LocalisationManager.paidByMe(), model: model)
        if let isUserCurrent = model?.creator?.isUserCurrent(), !isUserCurrent {
            splitBetweenPersonsTitle = self.splitMembersDescription(currentUserName: LocalisationManager.payedByOtherButSplitWithMe(), model: model)
        }
        
        if splitBetweenPersonsTitle.characters.count > 0 {
            let title = " » "
            attributedSting = NSAttributedString(string: title)
            if let attributedSting = attributedSting {
                resultAttributedString.append(attributedSting)
            }
            
            attributes = [NSForegroundColorAttributeName: Constatnts.defaultDescriptionTextColor()]
            attributedSting = NSAttributedString(string: splitBetweenPersonsTitle, attributes: attributes)
            if let attributedSting = attributedSting {
                resultAttributedString.append(attributedSting)
            }
        }
        
        self.titleLabel?.attributedText = resultAttributedString
        
        var title = ""
        if let date = model?.creationDate {
            title = DateIsoFormatter.creationDateFormatter().string(from: date)
        }
        
        if let expenseTitle = model?.title , expenseTitle.characters.count > 0 {
            title += " / " + expenseTitle
        }
        self.subTitleLabel?.text = title
        
        if let price = model?.price {
            self.priceLabel?.text = PriceFormatter.roundDecimalForTwoPlacesToString(Double(price))
        }
    }
    
    private func splitMembersDescription(currentUserName: String, model: Expense?) -> String {
        var title = ""
        
        if let personsInDeal = model?.personsInDeal, personsInDeal.count > 0 {
            if let numberOfTeamMembers = model?.event?.persons?.count, numberOfTeamMembers == personsInDeal.count {
                title = LocalisationManager.payForAll()
            }
            else {
                var i = 0
                for teamMember in personsInDeal {
                    let firstLastName = NameFormatter.userName(teamMember, replace: currentUserName)
                    title += firstLastName
                    if i < personsInDeal.count - 1 {
                        title += ", "
                    }
                    
                    i += 1
                }
            }
        }
        
        return title
    }
}
