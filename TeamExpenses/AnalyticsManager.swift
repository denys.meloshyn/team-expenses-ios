//
//  AnalyticsManager.swift
//  TeamExpenses
//
//  Created by Denys Meloshyn on 03.05.16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

let kEditAction = "Edit"
let kSplitAction = "Split"
let kCancelAction = "Cancel"
let kDeleteAction = "Delete"
let kSendEmailAction = "SendEmail"
let kCreateEventAction = "CreateEvent"
let kOpenLicenseAction = "OpenLicense"
let kEditExpenseAction = "EditExpense"
let kCreateExpenseAction = "CreateExpense"
let kExpenseDeleteAction = "ExpenseDelete"
let kEditTeamMemberAction = "EditTeamMember"
let kOpenEventScreenAction = "OpenEventScreen"
let kCreateTeamMemberAction = "CreateTeamMember"
let kTeamMemberDeleteAction = "TeamMemberDelete"
let kChangeExpenseOwnerAction = "ChangeExpenseOwner"
let kCancelDeleteExpenseAction = "CancelDeleteExpense"
let kOpenEditContactScreenAction = "OpenEditContactScreen"
let kSaveEmptyNameLastNameAction = "SaveEmptyNameLastName"
let kCancelDeleteTeamMemberAction = "CancelDeleteTeamMembe"

let kAboutScreen = "About"
let kEventScreen = "Event"
let kLicenseScreen = "License"
let kEventListScreen = "EventList"
let kContactEditScreen = "ContactEdit"
let kExpenseEditScreen = "ExpenseEdit"
let kContactListScreen = "ContactList"
let kSplitExpenseScreen = "SplitExpense"

class AnalyticsManager: NSObject {
    class func configure() {
        var configureError: NSError?
        GGLContext.sharedInstance().configureWithError(&configureError)
        
        GAI.sharedInstance().trackUncaughtExceptions = true
        GAI.sharedInstance().defaultTracker.allowIDFACollection = true
    }
    
    class func trackError(_ title: String) {
         CLSNSLogv(title, getVaList([]))
    }
    
    class func track(_ category: String, action: String, label: String) {
        Answers.logContentView(withName: category, contentType: action, contentId: label, customAttributes: nil)
        
        let parameter = GAIDictionaryBuilder.createEvent(withCategory: category, action: action, label: label, value: 1).build() as [NSObject: AnyObject]
        GAI.sharedInstance().defaultTracker.send(parameter)
        
        CLSLogv("category: %@ action: %@ label: %@", getVaList([category, action, label]))
    }
    
    class func trackScreenVisible(name screenName: String) {
        GAI.sharedInstance().defaultTracker.set(kGAIScreenName, value: screenName)
        let parameter = GAIDictionaryBuilder.createScreenView().build() as [NSObject: AnyObject]
        GAI.sharedInstance().defaultTracker.send(parameter)
    }
}
