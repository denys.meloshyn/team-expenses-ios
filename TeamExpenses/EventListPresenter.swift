//
//  EventListPresenter.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 17/08/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit
import CoreData

protocol EventListPresenterDelegate: class {
    func listOfEventsDidChange()
    func listOfEventsWillChange()
    func showCreateNewEvenDialog(withTitle title: String, message: String)
    func showEditEvenDialog(withTitle title: String, message: String, cancelTitle: String, indexPath: IndexPath)
    func listOfEventsChanged(at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?)
    func showDeleteEvenDialog(withTitle title: String, message: String, cancelTitle: String, deleteTitle: String, indexPath: IndexPath)
}

class EventListPresenter: NSObject, EventListInteractionDelegate {
    let router = EventListRouter()
    let interaction = EventListInteraction()
    weak var viewController: UIViewController?
    weak var delegate: EventListPresenterDelegate?
    
    func initialConfiguration() {
        self.interaction.delegate = self
        self.interaction.initialConfiguration()
        
        self.router.viewController = self.viewController
    }
    
    func eventModel(forIndexPath indexPath: IndexPath) -> Event {
        return self.interaction.eventModel(for: indexPath)
    }
    
    func numberOfRowsInSection() -> Int {
        return self.interaction.numberOfRowsInSection()
    }
    
    func placeholderForCreatingNewEvent() -> String {
        return LocalisationManager.title()
    }
    
    func cancelTitleForCreatingNewEvent() -> String {
        return LocalisationManager.cancel()
    }
    
    func createTitleForNewEvent() -> String {
        return LocalisationManager.create()
    }
    
    func pageTitle() -> String {
        return LocalisationManager.events()
    }
    
    func showEditEvenDialog(_ indexPath: IndexPath) {
        self.delegate?.showEditEvenDialog(withTitle: LocalisationManager.event(), message: LocalisationManager.createNewEvent(), cancelTitle: LocalisationManager.cancel(),  indexPath: indexPath)
    }
    
    func removeWhiteSpacesIn(eventTitle title: String?) -> String? {
        let trimmedTitle = title?.trimmingCharacters(in: CharacterSet.whitespaces)
        
        return trimmedTitle
    }
    
    func isEventTitleValid(_ title: String?) -> Bool {
        let trimmedTitle = self.removeWhiteSpacesIn(eventTitle: title)
        
        if let text = trimmedTitle, text.characters.count == 0 {
            return false
        }
        
        return true
    }
    
    func showDeleteEvenDialog(_ indexPath: IndexPath) {
        self.delegate?.showDeleteEvenDialog(withTitle: LocalisationManager.event(), message: LocalisationManager.deleteEventWarning(), cancelTitle: LocalisationManager.cancel(), deleteTitle:LocalisationManager.delete(), indexPath: indexPath)
    }
    
    func showDetailPage(for indexPath: IndexPath) {
        let event = self.interaction.eventModel(for: indexPath)
        self.showDetailPage(event: event)
    }
    
    func showDetailPage(event: Event) {
        self.router.showDetailPage(for: event)
    }
    
    // MARK: - EventListInteractionDelegate methods
    
    func showCreateNewEvenDialog() {
        self.delegate?.showCreateNewEvenDialog(withTitle: LocalisationManager.event(), message: LocalisationManager.createNewEvent())
    }
    
    func willChangeContent() {
        self.delegate?.listOfEventsWillChange()
    }
    
    func didChangeContent() {
        self.delegate?.listOfEventsDidChange()
    }
    
    func changed(at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        self.delegate?.listOfEventsChanged(at: indexPath, for: type, newIndexPath: newIndexPath)
    }
}
