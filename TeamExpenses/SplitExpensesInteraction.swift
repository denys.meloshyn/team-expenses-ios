//
//  SplitExpensesInteraction.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 07/09/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit

class SplitExpensesInteraction: NSObject {
    var currentEvent: Event?
    var currentEventID: NSManagedObjectID? {
        didSet {
            if let currentEventID = currentEventID {
                self.currentEvent = ExpenseManager.sharedInstance.managedObjectContext.object(with: currentEventID) as? Event
            }
        }
    }
    
    private var items = [[SplitModel]]()
    private let childrenManagedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(ExpenseManager.sharedInstance.managedObjectContext)
    
    func initialConfiguration() {
        self.splitExpenses()
    }
    
    func numberOfSections() -> Int {
        return self.items.count
    }
    
    func numberOfRowsInSection(_ section: Int) -> Int {
        let items = self.items[section]
        
        return items.count
    }
    
    func splitModels() -> [[SplitModel]] {
        return self.items
    }
    
    func splitModel(at indexPath: IndexPath) -> SplitModel {
        let items = self.items[(indexPath as NSIndexPath).section]
        let model = items[(indexPath as NSIndexPath).row]
        
        return model
    }
    
    func sectionSplitModelType(in section: Int) -> SplitModelType? {
        let items = self.items[section]
        if let model = items.first {
            return model.type
        }
        
        return nil
    }
    
    func person(with model: SplitModel) -> Person? {
        return self.extractPerson(usgin: model.personID)
    }
    
    func overPaidPerson(with model: SplitModel) -> Person? {
        return self.extractPerson(usgin: model.overPaidPersonID)
    }
    
    func underPaidPersonID(with model: SplitModel) -> Person? {
        return self.extractPerson(usgin: model.underPaidPersonID)
    }
    
    // MARK: - Private methods
    
    private func splitExpenses() {
        let summaryExpenses = BillManager.sharedInstance.calculateAvarageDifferenceForExpenses(self.currentEventID, managedObjectContext: self.childrenManagedObjectContext)
        if summaryExpenses.count > 0 {
            var copySummaryExpenses = [SplitModel]()
            for model in summaryExpenses {
                if let copyModel = model.copy() as? SplitModel {
                    copyModel.type = SplitModelType.summary
                    copyModel.differenceFromAvarage = copyModel.differenceFromAvarage * -1.0
                    copySummaryExpenses.append(copyModel)
                }
            }
            
            self.items.append(copySummaryExpenses)
        }
        
        let userBills = BillManager.sharedInstance.splitExpenseBetweenUsers(summaryExpenses)
        if userBills.count > 0 {
            self.items.append(userBills)
        }
    }
    
    private func extractPerson(usgin modelID: NSManagedObjectID?) -> Person? {
        var person: Person?
        
        if let personID = modelID {
            person = self.childrenManagedObjectContext.object(with: personID) as? Person
        }
        
        return person
    }
}
