//
//  ExpenseEditInteraction.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 25/08/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit

protocol ExpenseEditInteractionDelegate: class {
    func contentSaved()
    func creationDateChanged(_ date: Date?)
}

class ExpenseEditInteraction: NSObject, NSFetchedResultsControllerDelegate {
    var currentEventModelID: NSManagedObjectID? {
        didSet {
            if let eventModelID = self.currentEventModelID {
                self.currentEventModel = self.childrenManagedObjectContext.object(with: eventModelID) as? Event
            }
        }
    }
    var currentExpenseModelID: NSManagedObjectID? {
        didSet {
            if let currentExpenseModelID = self.currentExpenseModelID {
                self.currentExpenseModel = self.childrenManagedObjectContext.object(with: currentExpenseModelID) as? Expense
            }
        }
    }
    var currentEventModel: Event?
    var currentExpenseModel: Expense?
    weak var delegate: ExpenseEditInteractionDelegate?
    var childrenManagedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(ExpenseManager.sharedInstance.managedObjectContext)
    
    func initialConfiguration() {
        if self.currentExpenseModelID == nil {
            self.currentExpenseModel = ExpenseManager.sharedInstance.createNewExpense(self.childrenManagedObjectContext)
            
            self.currentExpenseModel?.creationDate = Date()
            self.delegate?.creationDateChanged(self.currentExpenseModel?.creationDate)
            
            let person = ExpenseManager.sharedInstance.currentUser(self.childrenManagedObjectContext)
            if let currentExpenseModel = self.currentExpenseModel {
                // Configure inverse reference
                person?.addExpenseCreatorObject(currentExpenseModel)
            }
            self.currentExpenseModel?.creator = person
            
            if let currentExpenseModel = self.currentExpenseModel {
                self.currentEventModel?.addExpensesObject(currentExpenseModel)
            }
            
            if let persons = self.currentEventModel?.persons {
                for person in persons {
                    // Configure inverse reference
                    if let currentExpenseModel = self.currentExpenseModel {
                        person.addExpensePersons(inDealObject: currentExpenseModel)
                    }
                }
                
                self.currentExpenseModel?.addPersons(inDeal: persons)
            }
        }
    }
    
    func trackOpenningScreen() {
        AnalyticsManager.trackScreenVisible(name: kExpenseEditScreen)
    }
    
    func saveContent() {
        ExpenseManager.sharedInstance.saveTemporaryManagedObjectContext(self.childrenManagedObjectContext) {[weak self] (response, error) -> (Void) in
           self?.delegate?.contentSaved()
        }
    }
}
