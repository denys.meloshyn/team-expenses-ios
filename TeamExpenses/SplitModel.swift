//
//  SplitModel.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 20/01/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit
import CoreData

enum SplitModelType: Int {
    case summary
    case avarage
    case userExpense
    case split
};

class SplitModel: NSObject, NSCopying {
    var value = 0.0
    var differenceFromAvarage = 0.0
    var personID: NSManagedObjectID?
    var type = SplitModelType.summary
    var overPaidPersonID: NSManagedObjectID?
    var underPaidPersonID: NSManagedObjectID?
    
    required override init() {
        super.init()
    }
    
    required init(_ model: SplitModel) {
        self.type = model.type
        self.value = model.value
        self.differenceFromAvarage = model.differenceFromAvarage
        self.personID = model.personID?.copy() as? NSManagedObjectID
        self.overPaidPersonID = model.overPaidPersonID?.copy() as? NSManagedObjectID
        self.underPaidPersonID = model.underPaidPersonID?.copy() as? NSManagedObjectID
    }
    
    override var description : String {
        return self.debugDescription
    }
    
    override var debugDescription: String {
        var person: Person?
            
        if let personID = self.personID {
            person = ExpenseManager.sharedInstance.managedObjectContext.object(with: personID) as? Person
        }
        
        return "SplitModel user: \(person?.userName() ?? "") value: \(self.value) differenceFromAvarage:\(self.differenceFromAvarage)"
    }
    
    override var hashValue: Int {
        get {
            var result = self.type.hashValue
            result ^= self.value.hashValue
            result ^= self.differenceFromAvarage.hashValue
            
            if let personID = self.personID {
                result ^= personID.hashValue
            }
            
            if let overPaidPersonID = self.overPaidPersonID {
                result ^= overPaidPersonID.hashValue
            }
            
            if let underPaidPersonID = self.underPaidPersonID {
                result ^= underPaidPersonID.hashValue
            }
            
            return result
        }
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let object = object as? SplitModel {
            if self === object {
                return true
            }
            
            if self.type != object.type {
                return false
            }
            
            if self.value != object.value {
                return false
            }
            
            if self.differenceFromAvarage != object.differenceFromAvarage {
                return false
            }
            
            if self.personID != object.personID {
                return false
            }
            
            if self.overPaidPersonID != object.overPaidPersonID {
                return false
            }
            
            if self.underPaidPersonID != object.underPaidPersonID {
                return false
            }
            
            return true
        }
        
        return false
    }
    
    // MARK: - NSCopying
    
    public func copy(with zone: NSZone? = nil) -> Any {
        return type(of: self).init(self)
    }
}
