//
//  NSError+SwiftCasting.swift
//  Muzarium
//
//  Created by Denys.Meloshyn on 26/06/15.
//  Copyright (c) 2015 Denys Meloshyn. All rights reserved.
//

import Foundation

let baseDoamin = "TeamExpensesDomainError"

extension NSError {
    class func swiftError(_ statusCode: Int = 0) -> NSError {
        let swiftError = NSError(domain: "", code: statusCode, userInfo: nil)
        
        return swiftError
    }
    
    class func errorLogin() -> NSError {
        let swiftError = NSError(domain: baseDoamin, code: -1, userInfo: nil)
        
        return swiftError
    }
}
