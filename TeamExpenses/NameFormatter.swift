//
//  NameFormatter.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 26/08/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit

class NameFormatter: NSObject {
    class func userName(_ person: Person?, replace name: String) -> String {
        if let person = person, person.isUserCurrent() {
            return name
        }
        
        if let name = person?.name {
            return name
        }
        
        return ""
    }
    
    class func allPeopleInDealNames(_ expense: Expense?, _ event: Event?, _ allTitle: String = LocalisationManager.splitExpenseBetweenAll(), splitWithMe: String = LocalisationManager.expenseSplittedWithMe()) -> String {
        var title = ""
        
        if let splitBetweenPersons = expense?.personsInDeal, splitBetweenPersons.count > 0 {
            if let numberOfTeamMembers = event?.persons?.count, numberOfTeamMembers == splitBetweenPersons.count {
                title = allTitle
            }
            else {
                var i = 0
                for teamMember in splitBetweenPersons {
                    title += NameFormatter.userName(teamMember, replace: splitWithMe)
                    if i < splitBetweenPersons.count - 1 {
                        title += ", "
                    }
                    
                    i += 1
                }
            }
        }
        
        return title
    }
}
