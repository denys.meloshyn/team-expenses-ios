//
//  ExpenseEditView.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 25/08/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit

class ExpenseEditView: NSObject, ExpenseEditPresenterDelegate {
    @IBOutlet private var titleBorderView: UIView?
    @IBOutlet private var priceBorderView: UIView?
    @IBOutlet private var errorPriceLabel: UILabel?
    @IBOutlet private var scrollView: UIScrollView?
    @IBOutlet private var titleTextField: UITextField?
    @IBOutlet private var priceTextField: UITextField?
    @IBOutlet private var creationDateBorderView: UIView?
    @IBOutlet private var changeSplitPeronsButton: UIButton?
    @IBOutlet private var creationDateTextField: UITextField?
    @IBOutlet private var changeExpenseOwnerButton: UIButton?
    @IBOutlet private var splitBetweenTeamMembersLabel: UILabel?
    @IBOutlet private weak var viewController: UIViewController?
    @IBOutlet private var constraintErrorPriceLabelTop: NSLayoutConstraint?
    
    let presenter = ExpenseEditPresenter()
    
    func initialConfiguration() {
        self.presenter.delegate = self
        self.presenter.viewController = self.viewController
        self.presenter.initialConfiguration()
        
        self.viewController?.view.backgroundColor = Constatnts.defaultBackgroundColor()
        self.errorPriceLabel?.textColor = Constatnts.defaultErrorColor()
        
        self.priceTextField?.addTarget(self.presenter, action: #selector(ExpenseEditPresenter.priceChangedAction(sender:)), for: UIControlEvents.editingChanged)
        self.titleTextField?.addTarget(self.presenter, action: #selector(ExpenseEditPresenter.titleValueEdited(_:)), for: UIControlEvents.editingChanged)
        self.changeExpenseOwnerButton?.addTarget(self.presenter, action: #selector(ExpenseEditPresenter.changeExpenseCreator), for: UIControlEvents.touchUpInside)
        self.changeSplitPeronsButton?.addTarget(self.presenter, action: #selector(ExpenseEditPresenter.splitExpenseBetweenTeamMembersAction), for: UIControlEvents.touchUpInside)
        
        self.configureTextFields()
        self.priceTextField?.becomeFirstResponder()
    }
    
    // MARK: - ExpenseEditPresenterDelegate methods
    
    func presentInterfaceForNewExpense() {
        let saveBarButtonItem = UIBarButtonItem(title: LocalisationManager.create(), style: UIBarButtonItemStyle.done, target: self.presenter.interaction, action: #selector(ExpenseEditInteraction.saveContent))
        saveBarButtonItem.tintColor = Constatnts.defaultActiveColor()
        self.viewController?.navigationItem.rightBarButtonItem = saveBarButtonItem
    }
    
    func presentInterfaceForExistedExpense() {
        let updateBarButtonItem = UIBarButtonItem(title: LocalisationManager.update(), style: UIBarButtonItemStyle.done, target: self.presenter.interaction, action: #selector(ExpenseEditInteraction.saveContent))
        updateBarButtonItem.tintColor = Constatnts.defaultActiveColor()
        self.viewController?.navigationItem.rightBarButtonItem = updateBarButtonItem
    }
    
    func showKeyboard(with height: CGFloat) {
        if var contentInsets = self.scrollView?.contentInset {
            contentInsets.bottom = height
            self.scrollView?.contentInset = contentInsets
            self.scrollView?.scrollIndicatorInsets = contentInsets
        }
    }
    
    func hideKeyboard() {
        if var contentInsets = self.scrollView?.contentInset {
            if let length = self.viewController?.bottomLayoutGuide.length {
                contentInsets.bottom = length
            }
            
            self.scrollView?.contentInset = contentInsets
            self.scrollView?.scrollIndicatorInsets = contentInsets
        }
    }
    
    func updateTextBorder(for textField: ExpenseEditField) {
        self.resetBorderColors()
        
        let color = Constatnts.defaultActiveColor()
        if textField == ExpenseEditField.Price {
            self.priceBorderView?.layer.borderColor = color.cgColor
        }
        else if textField == ExpenseEditField.Title {
            self.titleBorderView?.layer.borderColor = color.cgColor
        }
        else if textField == ExpenseEditField.CreationDate {
            self.creationDateBorderView?.layer.borderColor = color.cgColor
        }
    }
    
    func pageSaveState(is enabled: Bool) {
        self.viewController?.navigationItem.rightBarButtonItem?.isEnabled = enabled
    }
    
    func priceIsValid() {
        self.updateTextBorder(for: ExpenseEditField.Price)
        
        // Value is correct
        self.errorPriceLabel?.text = ""
        self.constraintErrorPriceLabelTop?.constant = 0.0
    }
    
    func showPriceErrorMessage(_ message: String) {
        self.constraintErrorPriceLabelTop?.constant = 8.0
        self.errorPriceLabel?.text = message
        self.priceBorderView?.layer.borderColor = Constatnts.defaultErrorColor().cgColor
    }
    
    func presentCreationDateField(_ date: String?) {
        self.creationDateTextField?.text = date
    }
    
    func showPrice(_ price: String?) {
        self.priceTextField?.text = price
    }
    
    func showTitle(_ title: String?) {
        self.titleTextField?.text = title
    }
    
    func showExpenseCreatorButton(with title: String?) {
        self.changeExpenseOwnerButton?.setTitle(title, for: UIControlState.normal)
        self.changeExpenseOwnerButton?.setTitle(title, for: UIControlState.highlighted)
    }
    
    func showPersons(inDeal value: String?) {
        self.splitBetweenTeamMembersLabel?.text = value
    }
    
    func activateTextField(_ textField: ExpenseEditField) {
        if textField == ExpenseEditField.CreationDate {
            self.creationDateTextField?.becomeFirstResponder()
        }
        else if textField == ExpenseEditField.Title {
            self.titleTextField?.becomeFirstResponder()
        }
    }
    
    func activeTextField() -> ExpenseEditField {
        if let isFirstResponder = self.priceTextField?.isFirstResponder , isFirstResponder {
            return ExpenseEditField.Price
        }
        else if let isFirstResponder = self.titleTextField?.isFirstResponder , isFirstResponder {
            return ExpenseEditField.Title
        }
        else if let isFirstResponder = self.creationDateTextField?.isFirstResponder , isFirstResponder {
            return ExpenseEditField.CreationDate
        }
        
        return ExpenseEditField.None
    }
    
    func dismissKeyboard(for textField: ExpenseEditField) {
        if textField == ExpenseEditField.Title {
            self.titleTextField?.resignFirstResponder()
        }
        else if textField == ExpenseEditField.Price {
            self.priceTextField?.resignFirstResponder()
        }
        else if textField == ExpenseEditField.CreationDate {
            self.creationDateTextField?.resignFirstResponder()
        }
    }
    
    // MARK: - Public methods
    
    func viewWillAppear() {
       self.presenter.viewWillAppear()
    }
    
    func viewWillDisappear() {
        self.presenter.viewWillDisappear()
    }
    
    // MARK: - Private methods
    
    private func resetBorderColors() {
        let defaultBorderColor = Constatnts.defaultTextBorderColor()
        self.titleBorderView?.layer.borderColor = defaultBorderColor.cgColor
        self.priceBorderView?.layer.borderColor = defaultBorderColor.cgColor
        self.creationDateBorderView?.layer.borderColor = defaultBorderColor.cgColor
    }
    
    private func configureTextFields() {
        self.priceTextField?.delegate = self.presenter
        self.titleTextField?.delegate = self.presenter
        self.creationDateTextField?.delegate = self.presenter
        
        self.titleBorderView?.layer.borderWidth = 1.0
        self.titleBorderView?.layer.cornerRadius = 5.0
        self.priceBorderView?.layer.borderWidth = 1.0
        self.priceBorderView?.layer.cornerRadius = 5.0
        self.creationDateBorderView?.layer.borderWidth = 1.0
        self.creationDateBorderView?.layer.cornerRadius = 5.0
        
        self.changeSplitPeronsButton?.layer.cornerRadius = 5.0
        self.changeSplitPeronsButton?.backgroundColor = Constatnts.defaultActiveColor()
        
        self.changeExpenseOwnerButton?.layer.cornerRadius = 5.0
        self.changeExpenseOwnerButton?.backgroundColor = Constatnts.defaultActiveColor()
        
        var viewWidth: CGFloat = 0.0
        if let width = self.viewController?.view.frame.width {
            viewWidth = width
        }
        
        let toolBar = UIToolbar(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: viewWidth, height: 44.0)))
        toolBar.isTranslucent = false
        toolBar.tintColor = UIColor.white
        toolBar.barTintColor = Constatnts.defaultApperanceColor()
        toolBar.barStyle = UIBarStyle.default
        
        let doneToolBarItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self.presenter, action: #selector(ExpenseEditPresenter.doneKeyboard))
        let flexibleSpaceToolBarItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let nextToolBarItem = UIBarButtonItem(title: LocalisationManager.next(), style: UIBarButtonItemStyle.done, target: self.presenter, action: #selector(ExpenseEditPresenter.nextKeyboard))
        toolBar.items = [doneToolBarItem, flexibleSpaceToolBarItem, nextToolBarItem]
        self.priceTextField?.inputAccessoryView = toolBar
        
        self.titleTextField?.inputAccessoryView = toolBar
        
        let dateToolBar = UIToolbar(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: viewWidth, height: 44.0)))
        dateToolBar.tintColor = UIColor.white
        dateToolBar.barTintColor = Constatnts.defaultApperanceColor()
        dateToolBar.barStyle = UIBarStyle.default
        
        let doneDateToolBarItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self.presenter, action: #selector(ExpenseEditPresenter.doneKeyboard))
        dateToolBar.items = [doneDateToolBarItem]
        let datePicker = UIDatePicker()
        datePicker.addTarget(self.presenter, action: #selector(ExpenseEditPresenter.updateCreationDate(sender:)), for: UIControlEvents.valueChanged)
        if let creationDate = self.presenter.expenseCreationDate() {
            datePicker.date = creationDate
        }
        self.creationDateTextField?.inputView = datePicker
        self.creationDateTextField?.inputAccessoryView = dateToolBar
    }
}
