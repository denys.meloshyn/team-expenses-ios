//
//  PriceFormatter.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 21/01/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit

class PriceFormatter: NSObject {
    class func roundDecimalForTwoPlacesFormatter() -> NumberFormatter {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        formatter.usesGroupingSeparator = false
        formatter.minimumIntegerDigits = 1
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        formatter.roundingMode = NumberFormatter.RoundingMode.halfUp
        
        return formatter
    }
    
    class func roundStringDecimalForTwoPlacesToNumber(_ value: String?) -> NSNumber? {
        if let value = value {
            let formatter = PriceFormatter.roundDecimalForTwoPlacesFormatter()
            
            return formatter.number(from: value)
        }
        
        return nil
    }
    
    class func roundDecimalForTwoPlacesToString(_ value: Double) -> String? {
        let formatter = PriceFormatter.roundDecimalForTwoPlacesFormatter()
        
        return formatter.string(from: NSNumber(value: value))
    }
    
    class func roundDecimalForTwoPlacesToDouble(_ value: Double) -> Double {
        let formatter = PriceFormatter.roundDecimalForTwoPlacesFormatter()
        
        if let stringValue = PriceFormatter.roundDecimalForTwoPlacesToString(value) {
            if let result = formatter.number(from: stringValue) {
                return result.doubleValue
            }
        }
        
        return 0.0
    }
}
