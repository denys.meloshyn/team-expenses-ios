//
//  AboutViewController.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 02/05/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit
import MessageUI

enum AboutItemType {
    case license
    case writeEmail
    case appVersion
};

class AboutViewController: UITableViewController, MFMailComposeViewControllerDelegate {
    private lazy var emailViewcontroller = MFMailComposeViewController()
    private let items = [AboutItemType.license, AboutItemType.writeEmail, AboutItemType.appVersion]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.title = LocalisationManager.about()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.createMailController()
        
        self.tableView?.estimatedRowHeight = 44.0
        self.tableView?.rowHeight = UITableViewAutomaticDimension
        self.tableView.backgroundColor = Constatnts.defaultBackgroundColor()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        AnalyticsManager.trackScreenVisible(name: kAboutScreen)
    }
    
    // MARK: - UITableViewDataSource methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let type = self.items[(indexPath as NSIndexPath).row]
        var cell: UITableViewCell?
        
        switch type {
            case AboutItemType.license:
                cell = tableView.dequeueReusableCell(withIdentifier: "LicenseCell")
                
                break
            
            case AboutItemType.writeEmail:
                cell = tableView.dequeueReusableCell(withIdentifier: "WriteEmailCell")
                
                break
            
            case AboutItemType.appVersion:
                cell = tableView.dequeueReusableCell(withIdentifier: "AppVersionCell")
                
                if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
                    cell?.detailTextLabel?.text = version
                }
                
                break
        }
        
        let selectedView = UIView()
        selectedView.backgroundColor = Constatnts.defaultActiveColor()
        cell?.selectedBackgroundView = selectedView
        
        if let cell = cell {
            return cell
        }
        
        return UITableViewCell()
    }
    
    // MARK: - UITableViewDelegate methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let type = self.items[(indexPath as NSIndexPath).row]
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        if type == AboutItemType.license {
            AnalyticsManager.track(kAboutScreen, action: kOpenLicenseAction, label: "")
            
            if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "LicenseTableViewController") {
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
        else if type == AboutItemType.writeEmail {
            AnalyticsManager.track(kAboutScreen, action: kSendEmailAction, label: "")
            
            if MFMailComposeViewController.canSendMail() {
                self.present(self.emailViewcontroller, animated: true, completion: { () -> Void in
                    UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
                })
            }
        }
    }
    
    // MARK: - MFMailComposeViewControllerDelegate methods
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        
        self.createMailController()
    }
    
    private func createMailController() {
        if MFMailComposeViewController.canSendMail() {
            self.emailViewcontroller = MFMailComposeViewController()
            self.emailViewcontroller.mailComposeDelegate = self
            self.emailViewcontroller.modalTransitionStyle = UIModalTransitionStyle.coverVertical
            
            self.emailViewcontroller.navigationBar.tintColor = UIColor.white
            
            self.emailViewcontroller.setToRecipients([supportEmail])
            self.emailViewcontroller.setSubject(LocalisationManager.emailSubject())
        }
    }
}
