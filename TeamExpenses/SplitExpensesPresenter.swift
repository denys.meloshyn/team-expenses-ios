//
//  SplitExpensesPresenter.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 07/09/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit
import MessageUI

protocol SplitExpensesPresenterDelegate: class {
    func showShareButton()
    func showPageTitle(_ title: String)
    func applyTableBackgroundColor(_ color: UIColor)
    func showHeader(_ view: UIView?, with tintColor: UIColor, and textColor: UIColor);
    func createCell(with title: String?, and detailText: NSAttributedString?) -> UITableViewCell?
}

class SplitExpensesPresenter: NSObject, UITableViewDelegate, UITableViewDataSource, MFMailComposeViewControllerDelegate {
    weak var viewController: UIViewController?
    weak var delegate: SplitExpensesPresenterDelegate?
    private var emailViewcontroller = MFMailComposeViewController()
    
    let router = SplitExpensesRouter()
    let interaction = SplitExpensesInteraction()
    
    // MARK: - Public methods
    
    func initialConfiguration() {
        self.router.viewController = self.viewController
        self.interaction.initialConfiguration()
        
        self.delegate?.showShareButton()
        self.delegate?.showPageTitle(LocalisationManager.splitExpense())
        self.delegate?.applyTableBackgroundColor(Constatnts.defaultBackgroundColor())
        
        self.createMailController()
    }
    
    func exportReport() {
        if MFMailComposeViewController.canSendMail() {
            let html = ReportGenerator.sharedInstance.generateReport(self.interaction.splitModels(), currentEventID: self.interaction.currentEventID)
            
            self.router.presentMailPage(self.emailViewcontroller, with: html)
        }
        else {
            // Need to handle error
        }
    }
    
    // MARK: - UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let type = self.interaction.sectionSplitModelType(in: section) {
            if type == SplitModelType.summary {
                return LocalisationManager.summary()
            }
            else if type == SplitModelType.split {
                return LocalisationManager.split()
            }
        }
        
        return nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.interaction.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.interaction.numberOfRowsInSection(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let content: (text: String?, detailText: NSAttributedString?) = self.configureCellContent(with: self.interaction.splitModel(at: indexPath))
        
        if let cell = self.delegate?.createCell(with: content.text, and: content.detailText) {
            return cell
        }
        
        return UITableViewCell()
    }
    
    // MARK: - UITableViewDelegate methods
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        self.delegate?.showHeader(view, with: Constatnts.defaultApperanceColor(), and: UIColor.white)
    }
    
    // MARK: - MFMailComposeViewControllerDelegate methods
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
        
        self.createMailController()
    }
    
    // MARK: - Private methods
    
    private func configureCellContent(with model: SplitModel) -> (text: String?, detailText: NSAttributedString?) {
        var text: String?
        var detailText: NSAttributedString?
        
        if model.type == SplitModelType.summary {
            let person = self.interaction.person(with: model)
            
            let formatter = PriceFormatter.roundDecimalForTwoPlacesFormatter()
            formatter.numberStyle = NumberFormatter.Style.currency
            
            text = person?.userName()
            
            let resultAttributedString = NSMutableAttributedString()
            var attributedSting: NSAttributedString?
            if var splitValue = PriceFormatter.roundDecimalForTwoPlacesToString(model.differenceFromAvarage) {
                var attributes: [String: AnyObject]?
                
                if model.differenceFromAvarage > 0.0 {
                    attributes = [NSForegroundColorAttributeName: UIColor(rgba: "#43A047")]
                    splitValue = "+\(splitValue)"
                }
                else if model.differenceFromAvarage < 0.0 {
                    attributes = [NSForegroundColorAttributeName: UIColor(rgba: "#E53935")]
                }
                else {
                    attributes = [NSForegroundColorAttributeName: Constatnts.defaultPriceColor()]
                }
                
                attributedSting = NSAttributedString(string: splitValue, attributes: attributes)
                if let attributedSting = attributedSting {
                    resultAttributedString.append(attributedSting)
                }
            }
            
            let totalExpense = model.value - model.differenceFromAvarage
            if let totalValue = PriceFormatter.roundDecimalForTwoPlacesToString(totalExpense) {
                let attributes = [NSForegroundColorAttributeName: Constatnts.defaultPriceColor()]
                
                attributedSting = NSAttributedString(string: " (" + totalValue + ")", attributes: attributes)
                if let attributedSting = attributedSting {
                    resultAttributedString.append(attributedSting)
                }
            }
            
            detailText = resultAttributedString
        }
        else if model.type == SplitModelType.split {
            var overPaidPerson = ""
            var underPaidPerson = ""
            
            if let overPaidModel = self.interaction.overPaidPerson(with: model) {
                overPaidPerson = NameFormatter.userName(overPaidModel, replace: LocalisationManager.splitPersonOwnToMe())
            }
            
            if let underPaidModel = interaction.underPaidPersonID(with: model) {
                underPaidPerson = underPaidModel.userName()
            }
            
            text = underPaidPerson + " » " + overPaidPerson
            if let price = PriceFormatter.roundDecimalForTwoPlacesToString(model.value) {
                detailText = NSAttributedString(string: price)
            }
        }
        
        return (text, detailText)
    }
    
    private func createMailController() {
        self.emailViewcontroller = MFMailComposeViewController()
        self.emailViewcontroller.mailComposeDelegate = self
        self.emailViewcontroller.navigationBar.tintColor = UIColor.white
        
        //        let html = BillManager.sharedInstance.generateReport(self.items, currentEventID: self.currentEventID)
        //
        //        if let data = html.data(using: String.Encoding.utf8) {
        //            self.emailViewcontroller.addAttachmentData(data, mimeType: "text/plain", fileName: "report.html")
        //        }
        
        if let title = self.interaction.currentEvent?.title {
            let subject = "Pitch in report: \(title)"
            self.emailViewcontroller.setSubject(subject)
        }
    }
}
