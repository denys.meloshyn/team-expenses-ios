//
//  UserInfo.swift
//  Muzarium
//
//  Created by Ned on 26.06.15.
//  Copyright (c) 2015 Denys Meloshyn. All rights reserved.
//

import UIKit

class UserInfo: NSObject, NSCoding
{
    private let kUserToken = "kUserToken"
    private let kUserPassword = "kUserCredentials"
    
    var userID = -1 {
        didSet {
            self.save()
        }
    }
    
    var lastName: String {
//        let temproraryManagedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(ExpenseManager.sharedInstance.managedObjectContext)
//        if let currentUser = ExpenseManager.sharedInstance.currentUser(temproraryManagedObjectContext) {
//            if let lastName = currentUser.lastName {
//                return lastName
//            }
//        }
        
        return ""
    }
    
    var firstName: String {
//        let temproraryManagedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(ExpenseManager.sharedInstance.managedObjectContext)
//        if let currentUser = ExpenseManager.sharedInstance.currentUser(temproraryManagedObjectContext) {
//            if let firstName = currentUser.firstName {
//                return firstName
//            }
//        }
        
        return ""
    }
    
    var userImage = "" {
        didSet {
            self.save()
        }
    }
    
    var isUserSkippedLogin = false {
        didSet {
            self.save()
        }
    }
    
    var timeStampEvent = "" {
        didSet {
            self.save()
        }
    }
    
    var timeStampExpense = "" {
        didSet {
            self.save()
        }
    }
    
    var timeStampTeamMember = "" {
        didSet {
            self.save()
        }
    }
    
    var timeStampPerson = "" {
        didSet {
            self.save()
        }
    }
    
    var timeStampExpensePerson = "" {
        didSet {
            self.save()
        }
    }
    
    var token = ""
    var email = ""
    var password = ""
    
    class var sharedInstance: UserInfo
    {
        struct Static {
            static let instance: UserInfo = UserInfo()
        }
        
        return Static.instance
    }
    
    override init() {
        super.init()
        
        self.load()
    }
    
    required init?(coder aDecoder: NSCoder) {
        if let userID = aDecoder.decodeObject(forKey: "userID") as? Int {
            self.userID = userID
        }
        
        if let userImage = aDecoder.decodeObject(forKey: "userImage") as? String {
            self.userImage = userImage
        }
        
        if let timeStampEvent = aDecoder.decodeObject(forKey: "timeStampEvent") as? String {
            self.timeStampEvent = timeStampEvent
        }
        
        if let timeStampExpense = aDecoder.decodeObject(forKey: "timeStampExpense") as? String {
            self.timeStampExpense = timeStampExpense
        }
        
        if let timeStampTeamMember = aDecoder.decodeObject(forKey: "timeStampTeamMember") as? String {
            self.timeStampTeamMember = timeStampTeamMember
        }
        
        if let timeStampPerson = aDecoder.decodeObject(forKey: "timeStampPerson") as? String {
            self.timeStampPerson = timeStampPerson
        }
        
        if let timeStampExpensePerson = aDecoder.decodeObject(forKey: "timeStampExpensePerson") as? String {
            self.timeStampExpensePerson = timeStampExpensePerson
        }
        
        if let isUserSkippedLogin = aDecoder.decodeObject(forKey: "isUserSkippedLogin") as? Bool {
            self.isUserSkippedLogin = isUserSkippedLogin
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.userID, forKey: "userID")
        aCoder.encode(self.userImage, forKey: "userImage")
        aCoder.encode(self.timeStampEvent, forKey: "timeStampEvent")
        aCoder.encode(self.timeStampPerson, forKey: "timeStampPerson")
        aCoder.encode(self.timeStampExpense, forKey: "timeStampExpense")
        aCoder.encode(self.isUserSkippedLogin, forKey: "isUserSkippedLogin")
        aCoder.encode(self.timeStampTeamMember, forKey: "timeStampTeamMember")
        aCoder.encode(self.timeStampExpensePerson, forKey: "timeStampExpensePerson")
    }
    
    // MARK: - Private methods
    
    private func load() {
        if let data = UserDefaults.standard.object(forKey: "UserInfo") as? Data {
            if let loadedUser = NSKeyedUnarchiver.unarchiveObject(with: data) as? UserInfo {
                self.userID = loadedUser.userID
                self.userImage = loadedUser.userImage
                self.timeStampEvent = loadedUser.timeStampEvent
                self.timeStampPerson = loadedUser.timeStampPerson
                self.timeStampExpense = loadedUser.timeStampExpense
                self.isUserSkippedLogin = loadedUser.isUserSkippedLogin
                self.timeStampTeamMember = loadedUser.timeStampTeamMember
                self.timeStampExpensePerson = loadedUser.timeStampExpensePerson
            }
        }
    }
    
    // MARK: - Public methods
    
    func configureWithDictionary(_ dictionary: Dictionary <String, AnyObject>, completionBlock: AsynchronousURLConnection.AsynchronousCompletionBlock?) {
        let temproraryManagedObjectContext = ExpenseManager.sharedInstance.createChildrenManagedObjectContextFromParentContext(ExpenseManager.sharedInstance.managedObjectContext)
        var currentPerson = ExpenseManager.sharedInstance.currentUser(temproraryManagedObjectContext)
        
        if currentPerson == nil {
            currentPerson = ExpenseManager.sharedInstance.createNewPerson(temproraryManagedObjectContext)
            currentPerson?.isCurrentUser = true
        }
        
        if let value = dictionary["personID"] as? Int {
            self.userID = value
//            currentPerson?.userID = value
        }
        
        if (dictionary["firstName"] as? String) != nil {
//            currentPerson?.firstName = value
        }
        
        if (dictionary["lastName"] as? String) != nil {
//            currentPerson?.lastName = value
        }
        
        if (dictionary["email"] as? String) != nil {
//            currentPerson?.email = value
        }
                
        if let value = dictionary["userToken"] as? String {
            self.token = value
        }
        
        ExpenseManager.sharedInstance.saveTemporaryManagedObjectContext(temproraryManagedObjectContext) { (response, error) -> (Void) in
            completionBlock?(response, error)
        }
    }
    
    func isUserLoggedIn () -> (Bool) {
        return self.userID != -1
    }
    
    func save() {
        let data = NSKeyedArchiver.archivedData(withRootObject: self)
        UserDefaults.standard.set(data, forKey: "UserInfo")
    }
    
    func logOut () {
        self.token = ""
        self.userID = -1
        self.password = ""
        self.userImage = ""
        self.timeStampEvent = ""
        self.timeStampPerson = ""
        self.timeStampExpense = ""
        self.timeStampTeamMember = ""
        self.timeStampExpensePerson = ""
    }
}
