//
//  TeamTableViewCell.swift
//  TeamExpenses
//
//  Created by Denys.Meloshyn on 12/09/16.
//  Copyright © 2016 Denys Meloshyn. All rights reserved.
//

import UIKit

class TeamTableViewCell: UITableViewCell {
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.tintColor = Constatnts.defaultApperanceColor()
        self.textLabel?.highlightedTextColor = UIColor.white
        self.backgroundColor = UIColor.clear
        self.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        
        let selectedView = UIView()
        selectedView.backgroundColor = Constatnts.defaultActiveColor()
        self.selectedBackgroundView = selectedView
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.textLabel?.text = ""
        self.accessoryType = UITableViewCellAccessoryType.disclosureIndicator
        self.selectionStyle = UITableViewCellSelectionStyle.default
    }
    
    func configureWithModel(_ model: Person?) {
        if let model = model {
            if model.isUserCurrent() {
                self.selectionStyle = UITableViewCellSelectionStyle.none
                self.accessoryType = UITableViewCellAccessoryType.none
            }
            
            self.textLabel?.text = NameFormatter.userName(model, replace: LocalisationManager.me())
        }
    }
}
